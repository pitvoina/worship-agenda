import * as Font from 'expo-font';

export const useFonts = async () =>
    await Font.loadAsync({
        'Poppins thin': require('../assets/fonts/Poppins-Thin.ttf'),
        'Poppins regular': require('../assets/fonts/Poppins-Regular.ttf'),
        'Poppins medium': require('../assets/fonts/Poppins-Medium.ttf'),
        'Poppins semiBold': require('../assets/fonts/Poppins-SemiBold.ttf'),
        'Poppins bold': require('../assets/fonts/Poppins-Bold.ttf'),
        'Poppins extraBold': require('../assets/fonts/Poppins-ExtraBold.ttf'),
        'Poppins light': require('../assets/fonts/Poppins-Light.ttf'),
        'Poppins extraLight': require('../assets/fonts/Poppins-ExtraLight.ttf'),
    });
