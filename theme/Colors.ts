export const Colors = {
    white: '#fff',
    black: '#000',
    o3_dark: '#03090D',
    ob_dark: '#0B1E26',
    od_dark: '#0D0D0D',
    dark_teal: '#254A59',
    teal: '#3E768C',
    light_teal: '#0CECDA',
    red: '#F21905',
    light_red: '#FF4D3A',
    green: '#228B22',
    gray: '#8A7F75',
    highlightTeal: 'rgba(12, 236, 218, .3)',
    backdrop: 'rgba(11, 30, 38, .95)',
}
