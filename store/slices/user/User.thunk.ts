import {auth, db, storage} from '../../../config';
import {createAsyncThunk} from '@reduxjs/toolkit';
import {doc, getDoc, setDoc, getDocs, query, collection} from 'firebase/firestore';
import {ref, uploadBytes, getDownloadURL} from 'firebase/storage';
import {signInWithEmailAndPassword, signOut, updateEmail} from 'firebase/auth';
import {v4 as uuidv4} from 'uuid';
import {ImagePickerResult} from 'expo-image-picker';
import {UserType} from './User.slice';

export interface ErrorType {
    code: string,
    message: string
}

export const signIn = createAsyncThunk(
    'signIn',
    async ({email, password}: { email: string, password: string }, {rejectWithValue}) => {
        return signInWithEmailAndPassword(auth, email, password)
            .then((userCredential) => userCredential)
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
);

export const getUserData = createAsyncThunk(
    'getUserData',
    async (userId: string, {rejectWithValue}) => {
        const docRef = doc(db, 'users', userId);
        return await getDoc(docRef)
            .then(doc => doc.data())
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}))
    }
);

export const logOut = createAsyncThunk(
    'logOut',
    async (user: void, {rejectWithValue}) => {
        return signOut(auth).then(() => true)
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
);

export const setUserData = createAsyncThunk(
    'setUserData',
    async ({userId, name, phoneNumber, role}: { userId: string | null, name: string, phoneNumber: string | null, role: string | null }, {rejectWithValue}) => {
        if (userId == null) return;
        const docRef = doc(db, 'users', userId);
        return await setDoc(docRef, {
            displayName: `${ name }`,
            phoneNumber: phoneNumber ? phoneNumber : '',
            role: role ? role : '',
        }).then(() => {
            return {displayName: `${ name }`, phoneNumber: phoneNumber ? phoneNumber : '', role: role ? role : ''}
        })
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
);

export const editUser = createAsyncThunk(
    'editUser',
    async ({email, name, phoneNumber, role}: { email: string, name: string, phoneNumber: string, role: string }, {rejectWithValue}) => {
        if (auth.currentUser) {
            return updateEmail(auth.currentUser, email)
                .catch((error: ErrorType) => rejectWithValue({error: error.code, message: error.message}));
        } else {
            return;
        }
    }
);

export const uploadUserImg = createAsyncThunk(
    'uploadUserImg',
    async ({image, userId}: { image: ImagePickerResult, userId: string }, {rejectWithValue}) => {
        if (image !== null) {
            if (!image.cancelled && image.uri) {
                const newFileName = `${ uuidv4() }.${ image.uri.split('.').pop() }`;

                const imgUri = image.uri;

                const blob: any = await new Promise((resolve, reject) => {
                    const xhr = new XMLHttpRequest();
                    xhr.onload = function () {
                        resolve(xhr.response);
                    };
                    xhr.onerror = function (e) {
                        reject(new TypeError('Network request failed'));
                    };
                    xhr.responseType = 'blob';
                    xhr.open('GET', imgUri, true);
                    xhr.send(null);
                });

                const imgRef = ref(storage, `users/images/${ newFileName }`);
                const fullPath = await uploadBytes(imgRef, blob).then(snapshot => snapshot.ref.fullPath);
                blob.close();

                let url = await getDownloadURL(ref(storage, fullPath)).then(url => url);

                const docRef = doc(db, 'users', userId);
                await setDoc(docRef, {'photo': url}, {merge: true});

                return url;
            } else {
                return rejectWithValue({code: '', message: 'Cancelled!'})
            }
        } else {
            return rejectWithValue({code: '', message: 'Something went wrong. Please try again!'});
        }
    }
);

export const getUsers = createAsyncThunk(
    'getUsers',
    async (_users, {rejectWithValue}) => {
        return await getDocs(collection(db, `users`))
            .then(docs => {
                const users: UserType[] = [];
                docs.forEach(doc => {
                    const userData = doc.data();
                    users.push({
                        name: userData.displayName,
                        phoneNumber: userData.phoneNumber ? userData.phoneNumber : '',
                        photo: userData.photo ? userData.photo : '',
                        role: userData.role ? userData.role : '',
                        userId: doc.id,
                    })
                });
                return users;
            })
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
);
