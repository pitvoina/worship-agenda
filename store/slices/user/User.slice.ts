import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {getUserData, getUsers, logOut, setUserData, signIn, uploadUserImg} from './User.thunk';

export interface UserType {
    userId: string,
    name: string,
    phoneNumber: string,
    photo: string,
    role: string,
}

interface UserState {
    authUser: boolean,
    isEditData: boolean,
    isLoggedIn: boolean,
    name: string,
    noUserData: string | null,
    phoneNumber: string,
    photo: any,
    role: string,
    userEmail: string,
    userError: string | null,
    userId: string | null,
    userLoading: boolean,
    userList: UserType[],
}

const initialState: UserState = {
    authUser: false,
    isEditData: false,
    isLoggedIn: false,
    name: '',
    noUserData: null,
    phoneNumber: '',
    photo: null,
    role: '',
    userEmail: '',
    userError: null,
    userId: null,
    userLoading: false,
    userList: [],
}

const userSlice = createSlice({
        name: 'User',
        initialState,
        reducers: {
            setActiveUser: (state: UserState, {payload}: PayloadAction<any>) => {
                state.userId = payload.uid;
                state.name = payload.displayName ? payload.displayName : state.name;
                state.userEmail = payload.email;
                state.phoneNumber = payload.phoneNumber;
                state.role = payload.role;
                state.authUser = true;
            },
            clearUserError: (state: UserState) => {
                state.userError = null;
            },
            setId: (state: UserState, {payload}: PayloadAction<string>) => {
                state.userId = payload;
            },
            setName: (state: UserState, {payload}: PayloadAction<string>) => {
                state.name = payload;
            },
            setEmail: (state: UserState, {payload}: PayloadAction<string>) => {
                state.userEmail = payload;
            },
            setPhone: (state: UserState, {payload}: PayloadAction<string>) => {
                state.phoneNumber = payload;
            },
            setRole: (state: UserState, {payload}: PayloadAction<string>) => {
                state.role = payload;
            },
            setAuth: (state: UserState, {payload}: PayloadAction<boolean>) => {
                state.authUser = payload;
            },
            setIsEdit: (state: UserState, {payload}: PayloadAction<boolean>) => {
                state.isEditData = payload;
            },
        },
        extraReducers: builder => {
            builder
                .addCase(signIn.fulfilled, (state: UserState) => {
                    state.authUser = true;
                    state.userLoading = false;
                })
                .addCase(signIn.pending, (state: UserState) => {
                    state.userLoading = true;
                })
                .addCase(signIn.rejected, (state: UserState, {payload}: PayloadAction<any>) => {
                    state.userLoading = false;
                    state.userError = payload.message;
                })
                .addCase(getUserData.fulfilled, (state: UserState, {payload}: PayloadAction<any>) => {
                    state.userLoading = false;
                    state.phoneNumber = payload.phoneNumber;
                    state.name = payload.displayName;
                    state.role = payload.role;
                    state.photo = payload.photo;
                })
                .addCase(getUserData.pending, (state: UserState) => {
                    state.userLoading = true;
                })
                .addCase(getUserData.rejected, (state: UserState, {payload}: PayloadAction<any>) => {
                    state.userLoading = false;
                    state.userError = payload.message;
                })
                .addCase(logOut.fulfilled, (state: UserState) => {
                    state.userLoading = false;
                    state.userId = null;
                    state.name = '';
                    state.role = '';
                    state.phoneNumber = '';
                    state.userEmail = '';
                    state.authUser = false;
                })
                .addCase(logOut.rejected, (state: UserState, {payload}: PayloadAction<any>) => {
                    state.userError = payload.message;
                    state.userLoading = false;
                })
                .addCase(setUserData.fulfilled, (state: UserState, {payload}: PayloadAction<any>) => {
                    state.isEditData = false;
                    state.name = payload.displayName;
                    state.phoneNumber = payload.phoneNumber;
                    state.role = payload.role;
                    state.userLoading = false;
                })
                .addCase(setUserData.pending, (state: UserState) => {
                    state.userLoading = true;
                })
                .addCase(setUserData.rejected, (state: UserState, {payload}: PayloadAction<any>) => {
                    state.userLoading = false;
                    state.userError = payload.message;
                })
                .addCase(uploadUserImg.fulfilled, (state: UserState, {payload}: PayloadAction<string>) => {
                    state.userLoading = false;
                    state.photo = payload;
                })
                .addCase(uploadUserImg.pending, (state: UserState) => {
                    state.userLoading = true;
                })
                .addCase(uploadUserImg.rejected, (state: UserState, {payload}: PayloadAction<any>) => {
                    state.userLoading = false;
                    state.userError = payload.message;
                })
                .addCase(getUsers.fulfilled, (state: UserState, {payload}: PayloadAction<UserType[]>) => {
                    state.userLoading = false;
                    state.userList = payload;
                })
                .addCase(getUsers.pending, (state: UserState) => {
                    state.userLoading = true;
                })
                .addCase(getUsers.rejected, (state: UserState, {payload}: PayloadAction<any>) => {
                    state.userLoading = false;
                    state.userError = payload.message;
                })
        }
    })
;

export const UserSlice = userSlice.actions;
export const UserReducer = userSlice.reducer;
