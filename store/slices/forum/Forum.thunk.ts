import {ErrorType} from '../user/User.thunk';
import {createAsyncThunk} from '@reduxjs/toolkit';
import {db} from '../../../config';
import {doc, getDoc, setDoc, getDocs, collection, arrayUnion, arrayRemove, orderBy, query} from 'firebase/firestore';
import {ThreadType} from '../thread/Thread.slice';

export const createNewThread = createAsyncThunk(
    'createNewThread',
    async (
        {userId, displayName, userPhoto, userRole, title, content}: { userId: string, displayName: string, userPhoto: string, userRole: string, title: string, content: string },
        {rejectWithValue}
    ) => {
        const currentDate = String(new Date().getTime()).slice(0, -3);
        const docRef = doc(db, 'forum', currentDate);
        return await setDoc(docRef, {
            content: content,
            createdAt: currentDate,
            createdBy: userId,
            favorite: [],
            likes: [],
            threadId: `thread_${ currentDate }`,
            title: title,
            userName: displayName,
            userPhoto: userPhoto,
            userRole: userRole,
            views: []
        }).then(() => {
            const userDocRef = doc(db, 'users', userId);
            return getDoc(userDocRef)
                .then((doc) => {
                    const userData = doc.data();
                    return {
                        userId,
                        content,
                        title,
                        createdAt: currentDate,
                        threadId: `thread_${ currentDate }`,
                        userName: userData?.displayName,
                        userPhoto: userData?.photo,
                        userRole: userData?.role,
                    }
                })
                .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
        })
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
);

export const getForumThreads = createAsyncThunk(
    'getForumThreads',
    async (_param, {rejectWithValue}) => {
        const q = query(collection(db, 'forum'), orderBy('createdAt', 'desc'));
        return await getDocs(q)
            .then((snaps) => {
                const docs: ThreadType[] = [];
                snaps.forEach((docData) => {
                    const data = docData.data();
                    docs.push({
                        comments: data.comments,
                        content: data.content,
                        createdAt: data.createdAt,
                        createdBy: data.createdBy,
                        favorite: data.favorite,
                        likes: data.likes,
                        threadId: data.threadId,
                        title: data.title,
                        userName: data.userName,
                        userPhoto: data.userPhoto,
                        userRole: data.userRole,
                        views: data.views,
                    });

                });
                return docs;
            })
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
);

export const setFavoriteThread = createAsyncThunk(
    'setFavoriteThread',
    async ({threadId, userId, isFav}: { threadId: string, userId: string, isFav: boolean }, {rejectWithValue}) => {
        const q = doc(db, `forum/${ threadId }`);

        return await setDoc(q, {favorite: (isFav ? arrayUnion(userId) : arrayRemove(userId))}, {merge: true})
            .then(() => 'done')
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
)
