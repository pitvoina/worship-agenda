import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {createNewThread, getForumThreads, setFavoriteThread} from './Forum.thunk';
import {ThreadType} from '../thread/Thread.slice';


export interface ForumType {
    forumError: string | null,
    forumLoading: boolean,
    threads: ThreadType[],
}

const initialState: ForumType = {
    forumError: null,
    forumLoading: false,
    threads: [],
}


const forumSlice = createSlice({
    name: 'Forum',
    initialState,
    reducers: {
        clearForumError: (state: ForumType) => {
            state.forumError = null;
        },
        setFavorite: (state: ForumType, {payload}: PayloadAction<{ threadId: string, userId: string, isFav: boolean }>) => {
            state.threads.map((thread) => {
                if (thread.threadId === payload.threadId) {
                    if (payload.isFav) {
                        thread.favorite = thread.favorite.filter(f => f !== payload.userId);
                    } else {
                        thread.favorite.push(payload.userId);
                    }
                }
            })
        },
    },
    extraReducers: builder => {
        builder
            .addCase(createNewThread.fulfilled, (state: ForumType, {payload}: PayloadAction<any>) => {
                state.forumLoading = false;
                state.threads.unshift({
                    comments: [],
                    content: payload.content,
                    createdAt: payload.createdAt,
                    createdBy: payload.userId,
                    favorite: [],
                    likes: [],
                    threadId: payload.threadId,
                    title: payload.title,
                    userName: payload.userName,
                    userPhoto: payload.userPhoto,
                    userRole: payload.userRole,
                    views: [],
                });
            })
            .addCase(createNewThread.pending, (state: ForumType) => {
                state.forumLoading = true;
            })
            .addCase(createNewThread.rejected, (state: ForumType, {payload}: PayloadAction<any>) => {
                state.forumLoading = false;
                state.forumError = payload.message;
            })
            .addCase(getForumThreads.fulfilled, (state: ForumType, {payload}: PayloadAction<any>) => {
                state.forumLoading = false;
                state.threads = payload;
            })
            .addCase(getForumThreads.pending, (state: ForumType) => {
                state.forumLoading = true;
            })
            .addCase(getForumThreads.rejected, (state: ForumType, {payload}: PayloadAction<any>) => {
                state.forumLoading = false;
                state.forumError = payload.message;
            })
            .addCase(setFavoriteThread.fulfilled, (state: ForumType, {payload}: PayloadAction<any>) => {
                state.forumLoading = false;
            })
            .addCase(setFavoriteThread.pending, (state: ForumType) => {
                state.forumLoading = true;
            })
            .addCase(setFavoriteThread.rejected, (state: ForumType, {payload}: PayloadAction<any>) => {
                state.forumLoading = false;
                state.forumError = payload.message;
            })
    }
});

export const ForumSlice = forumSlice.actions;
export const ForumReducer = forumSlice.reducer;
