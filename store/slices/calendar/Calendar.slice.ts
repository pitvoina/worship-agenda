import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {createNewAppointment, getAppointments, setArchive} from './Calendar.thunk';

export interface AppointmentType {
    appointmentId: string,
    archive: string[],
    createdBy: string,
    date: Date,
    hour: string,
    info: string,
    title: string,
    users: string[],
}

export interface CalendarType {
    appointments: AppointmentType[],
    calendarError: null | string,
    isLoading: boolean,
    newAppointment: AppointmentType,
    showUserList: boolean,
}

const initialState: CalendarType = {
    newAppointment: {
        appointmentId: '',
        archive: [],
        createdBy: '',
        date: new Date(),
        hour: '',
        info: '',
        title: '',
        users: [],
    },
    isLoading: false,
    calendarError: null,
    appointments: [],
    showUserList: false,
}


const calendarSlice = createSlice({
    name: 'Calendar',
    initialState,
    reducers: {
        clearCalendarError: (state: CalendarType) => {
            state.calendarError = null;
        },
        setNewAppointment: (state: CalendarType, {payload}: PayloadAction<AppointmentType>) => {
            state.newAppointment = payload;
        },
        setNewAppointmentTitle: (state: CalendarType, {payload}: PayloadAction<any>) => {
            state.newAppointment.title = payload;
        },
        setNewAppointmentInfo: (state: CalendarType, {payload}: PayloadAction<any>) => {
            state.newAppointment.info = payload;
        },
        setShowUserList: (state: CalendarType, {payload}: PayloadAction<boolean>) => {
            state.showUserList = payload
        },
        clearNewAppointment: (state: CalendarType) => {
            state.newAppointment = {
                appointmentId: '',
                archive: [],
                createdBy: '',
                date: new Date(),
                hour: '',
                info: '',
                title: '',
                users: [],
            };
        }
    },
    extraReducers: builder => {
        builder
            .addCase(getAppointments.fulfilled, (state: CalendarType, {payload}: PayloadAction<any>) => {
                state.isLoading = false;
                state.appointments = payload;
            })
            .addCase(getAppointments.pending, (state: CalendarType) => {
                state.isLoading = true;
            })
            .addCase(getAppointments.rejected, (state: CalendarType, {payload}: PayloadAction<any>) => {
                state.isLoading = false;
                state.calendarError = payload.message;
            })
            .addCase(setArchive.fulfilled, (state: CalendarType, {payload}: PayloadAction<any>) => {
                state.isLoading = false;
                state.appointments = payload;
            })
            .addCase(setArchive.pending, (state: CalendarType) => {
                state.isLoading = true;
            })
            .addCase(setArchive.rejected, (state: CalendarType, {payload}: PayloadAction<any>) => {
                state.isLoading = false;
                state.calendarError = payload.message;
            })
            .addCase(createNewAppointment.fulfilled, (state: CalendarType, {payload}: PayloadAction<any>) => {
                state.isLoading = false;
                state.appointments = payload;
            })
            .addCase(createNewAppointment.pending, (state: CalendarType) => {
                state.isLoading = true;
            })
            .addCase(createNewAppointment.rejected, (state: CalendarType, {payload}: PayloadAction<any>) => {
                state.isLoading = false;
                state.calendarError = payload.message;
            })
    }
});

export const CalendarSlice = calendarSlice.actions;
export const CalendarReducer = calendarSlice.reducer;
