import {createAsyncThunk} from '@reduxjs/toolkit';
import {arrayUnion, collection, doc, getDocs, query, setDoc, where} from 'firebase/firestore';
import {db} from '../../../config';
import {ErrorType} from '../user/User.thunk';
import {AppointmentType} from './Calendar.slice';
import {v4 as uuidv4} from 'uuid';

const setAppointmentData = (docs: any) => {
    let appointments: AppointmentType[] = [];
    docs.forEach((doc: any) => {
        appointments.push(creteDataObject(doc));
    });
    return appointments;
}

const creteDataObject = (doc: any) => {
    const appointData = doc.data();
    return {
        appointmentId: doc.id,
        archive: appointData.archive,
        createdBy: appointData.createdBy,
        date: new Date(appointData.date),
        hour: appointData.hour,
        info: appointData.info,
        title: appointData.title,
        users: appointData.users,
    }
}

export const getAppointments = createAsyncThunk(
    'getAppointments',
    async ({userId}: { userId: string }, {rejectWithValue}) => {
        const q = query(collection(db, `appointments`), where('users', 'array-contains', userId));

        return await getDocs(q)
            .then(docs => setAppointmentData(docs))
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
);

export const setArchive = createAsyncThunk(
    'setArchive',
    async ({userId, appointmentId}: { userId: string, appointmentId: string }, {rejectWithValue}) => {
        const q = doc(db, `appointments/${ appointmentId }`);

        return await setDoc(q, {archive: arrayUnion(userId)}, {merge: true})
            .then(() => {
                const appointmentsQuery = query(collection(db, `appointments`), where('users', 'array-contains', userId));
                return getDocs(appointmentsQuery)
                    .then(docs => setAppointmentData(docs))
                    .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
            })
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
);

export const createNewAppointment = createAsyncThunk(
    'createNewAppointment',
    async ({appointment, userId}: { appointment: AppointmentType, userId: string }, {rejectWithValue}) => {
        const q = doc(db, `appointments/${ uuidv4() }`);

        return await setDoc(q, {
            archive: [],
            createdBy: userId,
            date: appointment.date.getTime(),
            info: appointment.info,
            title: appointment.title,
            hour: appointment.hour,
            users: appointment.users
        })
            .then(() => {
                const getAppQ = query(collection(db, `appointments`), where('users', 'array-contains', userId));

                return getDocs(getAppQ)
                    .then(docs => setAppointmentData(docs))
                    .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
            })
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
)
