import {createAsyncThunk} from '@reduxjs/toolkit';
import {arrayUnion, doc, setDoc} from 'firebase/firestore';
import {db} from '../../../config';
import {v4 as uuidv4} from 'uuid';

export const startNewChat = createAsyncThunk(
    'startNewChat',
    async ({data}: { data: string[] }, {rejectWithValue}) => {
        const ref = doc(db, `chats/${ uuidv4() }`);

        return await setDoc(ref, {
            users: data,
            date: new Date().getTime(),
            messagesId: uuidv4(),
        }).then(() => true).catch(error => rejectWithValue({code: error.code, message: error.message}));
    }
);

export const postMessage = createAsyncThunk(
    'postMessage',
    async ({messagesId, data}: {messagesId: string, data: any}, {rejectWithValue}) => {
        const ref = doc(db, `messages/${messagesId}`);

        return await setDoc(ref, {messages: arrayUnion(data)}, {merge: true})
    }
);
