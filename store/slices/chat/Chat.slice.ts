import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {postMessage, startNewChat} from './Chat.thunk';
import {UserType} from '../user/User.slice';

export interface ChatType {
    users: [],
    messagesId: string,
    date: number,
}

export interface MessageType {
    userId: string,
    name: string,
    text: string,
    date: string,
}

export interface ChatData {
    toUser: UserType | null,
    messagesId: string,
    chatId: string,
}

export interface Chats {
    chats: ChatType[],
    chatLoading: boolean,
    chatError: string,
    messages: ChatData,
}

const initialState: Chats = {
    chats: [],
    chatLoading: false,
    chatError: '',
    messages: {toUser: null, messagesId: '', chatId: ''},
}


const chatSlice = createSlice({
    name: 'Chat',
    initialState,
    reducers: {
        setChatsData: (state: Chats, {payload}: PayloadAction<any>) => {
            state.chats = payload;
        },
        setChatData: (state: Chats, {payload}: PayloadAction<any>) => {
            state.messages = payload;
        }
    },
    extraReducers: builder => {
        builder
            .addCase(startNewChat.fulfilled, (state: Chats) => {
                state.chatLoading = false;
            })
            .addCase(startNewChat.pending, (state: Chats) => {
                state.chatLoading = true;
            })
            .addCase(startNewChat.rejected, (state: Chats, {payload}: PayloadAction<any>) => {
                state.chatLoading = false;
                state.chatError = payload.message;
            })
            .addCase(postMessage.fulfilled, (state: Chats, {payload}: PayloadAction<any>) => {
            })
            .addCase(postMessage.rejected, (state: Chats, {payload}: PayloadAction<any>) => {
            })
    }
});

export const ChatSlice = chatSlice.actions;
export const ChatReducer = chatSlice.reducer;
