import {createAsyncThunk} from '@reduxjs/toolkit';
import {doc, query, collection, where, getDocs, setDoc, arrayUnion, arrayRemove, getDoc, orderBy, deleteDoc} from 'firebase/firestore';
import {db} from '../../../config';
import {ErrorType} from '../user/User.thunk';
import firebase from 'firebase/compat';
import {v4 as uuidv4} from 'uuid';
import {CommentType, ReplyType} from './Thread.slice';

const setCommentData = (snaps: any) => {
    const data: Array<{ id: string, data: CommentType }> = [];
    snaps.forEach((snap: any) => {
        const snapData = snap.data();
        data.push({
            id: snap.id,
            data: {
                commentId: snap.id,
                content: snapData.content,
                createdAt: snapData.createdAt,
                likes: snapData.likes,
                name: snapData.name,
                photo: snapData.photo,
                replies: [],
                role: snapData.role,
                threadId: snapData.threadId,
                userId: snapData.userId
            }
        });
    });
    return data;
}

const setReplyData = (docs: any) => {
    const replies: ReplyType[] = [];
    docs.forEach((reply: any) => {
        const replyData = reply.data();

        replies.push({
            commentId: replyData.commentId,
            content: replyData.content,
            createdAt: replyData.createdAt,
            likes: replyData.likes,
            name: replyData.name,
            photo: replyData.photo,
            replyId: reply.id,
            role: replyData.role,
            userId: replyData.userId,
        })
    })
    return replies;
}


export const getThread = createAsyncThunk(
    'getThread',
    async ({threadId}: { threadId: string }, {rejectWithValue}) => {
        const q = doc(db, `forum/${ threadId }`);
        return await getDoc(q)
            .then(snap => snap.data())
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
);

export const getComments = createAsyncThunk(
    'getComments',
    async ({threadId}: { threadId: string }, {rejectWithValue}) => {
        const q = query(collection(db, `forum/${ threadId }/comments`), orderBy('createdAt', 'asc'));
        return await getDocs(q)
            .then((snaps) => setCommentData(snaps))
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
);

export const setFavoriteThead = createAsyncThunk(
    'setFavoriteThead',
    async ({threadId, userId, isFav}: { threadId: string, userId: string, isFav: boolean }, {rejectWithValue}) => {
        const q = doc(db, `forum/${ threadId }`);

        return await setDoc(q, {favorite: (isFav ? arrayUnion(userId) : arrayRemove(userId))}, {merge: true})
            .then(() => 'done')
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
);

export const setLikeThread = createAsyncThunk(
    'setLikeThread',
    async ({threadId, userId, isLike}: { threadId: string, userId: string, isLike: boolean }, {rejectWithValue}) => {
        const q = doc(db, `forum/${ threadId }`);

        return await setDoc(q, {likes: (isLike ? arrayUnion(userId) : arrayRemove(userId))}, {merge: true})
            .then(() => 'done')
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
);

export const setViews = createAsyncThunk(
    'setViews',
    async ({threadId, userId}: { threadId: string, userId: string }, {rejectWithValue}) => {
        const q = doc(db, `forum/${ threadId }`);

        return await setDoc(q, {views: arrayUnion(userId)}, {merge: true})
            .then(() => 'done')
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
);

export const getThreadUsers = createAsyncThunk(
    'getThreadUsers',
    async ({userIds, type}: { userIds: string[], type: string }, {rejectWithValue}) => {
        const q = query(collection(db, 'users'), where(firebase.firestore.FieldPath.documentId(), 'in', userIds));

        return await getDocs(q)
            .then((snaps) => {
                const data: any = [];
                snaps.forEach((doc) => data.push({...doc.data(), userId: doc.id}));
                return {users: data, type};
            })
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
);

export const setComment = createAsyncThunk(
    'setComment',
    async (
        {parentId, commentText, userId, photo, role, name}: { parentId: string, commentText: string, userId: string, photo: string, role: string, name: string },
        {rejectWithValue}) => {
        const q = doc(db, `forum/${ parentId }/comments/${ uuidv4() }`);

        return await setDoc(q, {
                content: commentText,
                createdAt: String(new Date().getTime()).slice(0, -3),
                likes: [],
                name: name,
                photo: photo,
                role: role,
                threadId: parentId,
                userId: userId
            },
            {merge: true})
            .then(
                () => getDocs(query(collection(db, `forum/${ parentId }/comments`), orderBy('createdAt', 'asc')))
                    .then((snaps) => {
                        const comments = setCommentData(snaps);
                        return getDocs(query(collection(db, `commentReplies/${ parentId }/replies`), orderBy('createdAt', 'asc')))
                            .then((docs) => {
                                const replies = setReplyData(docs);
                                return {comments: comments, replies: replies};
                            })
                            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
                    })
                    .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}))
            )
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
);

export const likeComment = createAsyncThunk(
    'likeComment',
    async ({commentId, threadId, userId, set}: { commentId: string, threadId: string, userId: string, set: boolean }, {rejectWithValue}) => {
        const q = doc(db, `forum/${ threadId }/comments/${ commentId }`);
        return await setDoc(q, {likes: (set ? arrayUnion(userId) : arrayRemove(userId))}, {merge: true})
            .then(
                () => getDocs(query(collection(db, `forum/${ threadId }/comments`), orderBy('createdAt', 'asc')))
                    .then(snaps => {
                        const comments = setCommentData(snaps);
                        return getDocs(query(collection(db, `commentReplies/${ threadId }/replies`), orderBy('createdAt', 'asc')))
                            .then((docs) => {
                                const replies = setReplyData(docs);
                                return {comments: comments, replies: replies};
                            })
                            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
                    })
                    .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}))
            )
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
);

export const deleteComment = createAsyncThunk(
    'deleteComment',
    async ({commentId, threadId}: { commentId: string, threadId: string }, {rejectWithValue}) => {
        const q = doc(db, `forum/${ threadId }/comments/${ commentId }`);

        return await deleteDoc(q)
            .then(
                () => getDocs(query(collection(db, `forum/${ threadId }/comments`), orderBy('createdAt', 'asc')))
                    .then(snaps => {
                        const comments = setCommentData(snaps);
                        return getDocs(query(collection(db, `commentReplies/${ threadId }/replies`), orderBy('createdAt', 'asc')))
                            .then((docs) => {
                                const replies = setReplyData(docs);
                                return {comments: comments, replies: replies};
                            })
                            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));

                    })
                    .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}))
            )
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
);

export const deleteReplyWithComment = createAsyncThunk(
    'deleteReplyWithComment',
    async ({threadId, commentId}: { commentId: string, threadId: string }, {rejectWithValue}) => {
        const q = query(collection(db, `commentReplies/${ threadId }/replies`), where('commentId', '==', commentId));

        return await getDocs(q)
            .then(snaps =>
                snaps.forEach(snap =>
                    deleteDoc(doc(db, `commentReplies/${ threadId }/replies/${ snap.id }`))
                )
            )
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
);


export const getReplies = createAsyncThunk(
    'getReplies',
    async ({threadId}: { threadId: string }, {rejectWithValue}) => {
        const q = query(collection(db, `commentReplies/${ threadId }/replies`), orderBy('createdAt', 'asc'));

        return await getDocs(q)
            .then((docs) => setReplyData(docs))
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
)

export const setReply = createAsyncThunk(
    'setReply',
    async ({threadId, parentId, commentText, userId, photo, role, name}: { threadId: string, parentId: string, commentText: string, userId: string, photo: string, role: string, name: string }, {rejectWithValue}) => {
        const q = doc(db, `commentReplies/${ threadId }/replies/${ uuidv4() }`);

        return await setDoc(q, {
                content: commentText,
                createdAt: String(new Date().getTime()).slice(0, -3),
                likes: [],
                name: name,
                photo: photo,
                role: role,
                userId: userId,
                commentId: parentId,
            },
            {merge: true})
            .then(() => {
                const q = query(collection(db, `commentReplies/${ threadId }/replies`), orderBy('createdAt', 'asc'));
                return getDocs(q)
                    .then((docs) => setReplyData(docs))
                    .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
            })
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
);

export const deleteReply = createAsyncThunk(
    'deleteReply',
    async ({threadId, replyId}: { threadId: string, replyId: string }, {rejectWithValue}) => {
        const q = doc(db, `commentReplies/${ threadId }/replies/${ replyId }`);

        return await deleteDoc(q)
            .then(() => {
                const q = query(collection(db, `commentReplies/${ threadId }/replies`), orderBy('createdAt', 'asc'));
                return getDocs(q)
                    .then((docs) => setReplyData(docs))
                    .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
            })
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
);

export const likeReply = createAsyncThunk(
    'likeReply',
    async ({userId, threadId, replyId, set}: { userId: string, threadId: string, replyId: string, set: boolean }, {rejectWithValue}) => {
        const q = doc(db, `commentReplies/${ threadId }/replies/${ replyId }`);

        return await setDoc(q, {likes: (set ? arrayUnion(userId) : arrayRemove(userId))}, {merge: true})
            .then(() => {
                const q = query(collection(db, `commentReplies/${ threadId }/replies`), orderBy('createdAt', 'asc'));
                return getDocs(q)
                    .then((docs) => setReplyData(docs))
                    .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
            })
            .catch((error: ErrorType) => rejectWithValue({code: error.code, message: error.message}));
    }
)
