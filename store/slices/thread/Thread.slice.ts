import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {deleteComment, deleteReply, deleteReplyWithComment, getComments, getReplies, getThread, getThreadUsers, likeComment, likeReply, setComment, setFavoriteThead, setLikeThread, setReply, setViews} from './Thread.thunk';

const combineCommentsWithReplies = (payload: { replies: ReplyType[], comments: { id: string, data: CommentType }[] }) => {
    payload.replies.forEach(
        (reply: ReplyType) => {
            payload.comments.forEach((comment: { id: string; data: CommentType }) => comment.id === reply.commentId && comment.data.replies.push(reply));
        }
    );

    return payload.comments;
}

const repliesToComments = (comments: { id: string, data: CommentType }[], payload: ReplyType[]) => {
    comments.forEach(comment => comment.data.replies = []);
    payload.forEach(
        (reply: ReplyType) => {
            comments.forEach((comment: { id: string; data: CommentType }) => comment.id === reply.commentId && comment.data.replies.push(reply));
        }
    );
    return comments;
}

export interface ReplyType {
    commentId: string,
    content: string,
    createdAt: string,
    likes: string[],
    name: string,
    photo: string,
    replyId: string,
    role: string,
    userId: string,
}

export interface StatsType {
    count: number,
    type: string,
    users: { name: string, role: string, photo: string, userId: string }[]
}

export interface CommentType {
    content: string,
    commentId: string,
    createdAt: string,
    likes: string[],
    name: string,
    photo: string,
    replies: ReplyType[],
    role: string,
    threadId: string,
    userId: string,
}

export interface ThreadType {
    comments: { id: string, data: CommentType }[],
    content: string,
    createdAt: string,
    createdBy: string,
    favorite: string[],
    likes: string[],
    showWrite?: boolean,
    stats?: StatsType,
    threadError?: string,
    threadId: string,
    threadLoading?: boolean,
    title: string,
    userName: string,
    userPhoto: string,
    userRole: string,
    views: string[],
    writeParentId?: string,
    writeType?: 'comment' | 'reply' | null,
}

const initialState: ThreadType = {
    comments: [],
    content: '',
    createdAt: '',
    createdBy: '',
    favorite: [],
    likes: [],
    showWrite: false,
    stats: {users: [], type: '', count: 0},
    threadError: '',
    threadId: '',
    threadLoading: false,
    title: '',
    userName: '',
    userPhoto: '',
    userRole: '',
    views: [],
    writeParentId: '',
    writeType: null,
}
const threadSlice = createSlice({
    name: 'Thread',
    initialState,
    reducers: {
        setThread: (state: ThreadType, {payload}: PayloadAction<any>) => {
            state.comments = [];
            state.content = '';
            state.createdAt = '';
            state.createdBy = '';
            state.favorite = [];
            state.likes = [];
            state.threadId = '';
            state.title = '';
            state.userName = '';
            state.userPhoto = '';
            state.userRole = '';
            state.views = [];
        },
        clearThreadError: (state: ThreadType) => {
            state.threadError = '';
        },
        setWriteType: (state: ThreadType, {payload}: PayloadAction<'comment' | 'reply' | null>) => {
            state.writeType = payload;
        },
        setWriteParentId: (state: ThreadType, {payload}: PayloadAction<string>) => {
            state.writeParentId = payload;
        },
        setShowWrite: (state: ThreadType, {payload}: PayloadAction<boolean>) => {
            state.showWrite = payload;
        },
        setLike: (state: ThreadType, {payload}: PayloadAction<{ userId: string, add: boolean }>) => {
            if (payload.add) {
                state.likes.push(payload.userId);
            } else {
                state.likes = state.likes.filter(like => like !== payload.userId && like);
            }
        },
        setFavorite: (state: ThreadType, {payload}: PayloadAction<{ userId: string, add: boolean }>) => {
            if (payload.add) {
                state.favorite.push(payload.userId);
            } else {
                state.favorite = state.likes.filter(like => like !== payload.userId && like);
            }
        },
    },
    extraReducers: builder => {
        builder
            .addCase(getThread.fulfilled, (state: ThreadType, {payload}: PayloadAction<any>) => {
                const readableDate = new Date(parseInt(payload.createdAt) * 1000);

                state.threadLoading = false;
                state.comments = [];
                state.content = payload.content;
                state.createdAt = `${ ('0' + readableDate.getDay()).slice(-2) }.${ ('0' + readableDate.getMonth()).slice(-2) }.${ readableDate.getFullYear() }`;
                state.createdBy = payload.createdBy;
                state.favorite = payload.favorite;
                state.likes = payload.likes;
                state.threadId = payload.threadId;
                state.title = payload.title;
                state.userName = payload.userName;
                state.userPhoto = payload.userPhoto;
                state.userRole = payload.userRole;
                state.views = payload.views;
            })
            .addCase(getThread.pending, (state: ThreadType) => {
                state.threadLoading = true;
            })
            .addCase(getThread.rejected, (state: ThreadType, {payload}: PayloadAction<any>) => {
                state.threadLoading = false;
                state.threadError = payload.message;
            })

            .addCase(setFavoriteThead.fulfilled, (state: ThreadType) => {
                state.threadLoading = false;
            })
            .addCase(setFavoriteThead.pending, (state: ThreadType) => {
                state.threadLoading = true;
            })
            .addCase(setFavoriteThead.rejected, (state: ThreadType, {payload}: PayloadAction<any>) => {
                state.threadLoading = false;
                state.threadError = payload.message;
            })
            .addCase(setLikeThread.fulfilled, (state: ThreadType) => {
                state.threadLoading = false;
            })
            .addCase(setLikeThread.pending, (state: ThreadType) => {
                state.threadLoading = true;
            })
            .addCase(setLikeThread.rejected, (state: ThreadType, {payload}: PayloadAction<any>) => {
                state.threadLoading = false;
                state.threadError = payload.message;
            })
            .addCase(setViews.fulfilled, (state: ThreadType) => {
                state.threadLoading = false;
            })
            .addCase(setViews.rejected, (state: ThreadType, {payload}: PayloadAction<any>) => {
                state.threadLoading = false;
                state.threadError = payload.message;
            })
            .addCase(getThreadUsers.fulfilled, (state: ThreadType, {payload}: PayloadAction<any>) => {
                const data: StatsType = {type: payload.type, count: payload.users.length, users: []};
                payload.users.forEach((user: any) => data.users.push({name: user.displayName, role: user.role, photo: user.photo, userId: user.userId}));

                state.stats = data;
                state.threadLoading = false;
            })
            .addCase(getThreadUsers.pending, (state: ThreadType) => {
                state.threadLoading = true;
            })
            .addCase(getThreadUsers.rejected, (state: ThreadType, {payload}: PayloadAction<any>) => {
                state.threadLoading = false;
                state.threadError = payload.message;
            })
            .addCase(setComment.fulfilled, (state: ThreadType, {payload}: PayloadAction<any>) => {
                state.comments = combineCommentsWithReplies(payload);
                state.showWrite = false;
                state.threadLoading = false;
                state.writeParentId = '';
                state.writeType = null;
            })
            .addCase(setComment.pending, (state: ThreadType) => {
                state.threadLoading = true;
            })
            .addCase(setComment.rejected, (state: ThreadType, {payload}: PayloadAction<any>) => {
                state.threadLoading = false;
                state.threadError = payload.message;
            })
            .addCase(likeComment.fulfilled, (state: ThreadType, {payload}: PayloadAction<any>) => {
                state.threadLoading = false;
                state.comments = combineCommentsWithReplies(payload);
            })
            .addCase(likeComment.pending, (state: ThreadType) => {
                state.threadLoading = true;
            })
            .addCase(likeComment.rejected, (state: ThreadType, {payload}: PayloadAction<any>) => {
                state.threadLoading = false;
                state.threadError = payload.message;
            })
            .addCase(getComments.fulfilled, (state: ThreadType, {payload}: PayloadAction<any>) => {
                state.threadLoading = false;
                state.comments = payload;
            })
            .addCase(getComments.pending, (state: ThreadType) => {
                state.threadLoading = true;
            })
            .addCase(getComments.rejected, (state: ThreadType, {payload}: PayloadAction<any>) => {
                state.threadLoading = false;
                state.threadError = payload.message;
            })
            .addCase(deleteComment.fulfilled, (state: ThreadType, {payload}: PayloadAction<any>) => {
                state.threadLoading = false;

                state.comments = state.comments = combineCommentsWithReplies(payload);
            })
            .addCase(deleteComment.pending, (state: ThreadType) => {
                state.threadLoading = true;
            })
            .addCase(deleteComment.rejected, (state: ThreadType, {payload}: PayloadAction<any>) => {
                state.threadLoading = false;
                state.threadError = payload.message;
            })
            .addCase(setReply.fulfilled, (state: ThreadType, {payload}: PayloadAction<any>) => {
                state.comments = repliesToComments(state.comments, payload);
                state.showWrite = false;
                state.threadLoading = false;
                state.writeParentId = '';
                state.writeType = null;
            })
            .addCase(setReply.pending, (state: ThreadType) => {
                state.threadLoading = true;
            })
            .addCase(setReply.rejected, (state: ThreadType, {payload}: PayloadAction<any>) => {
                state.threadLoading = false;
                state.threadError = payload.message;
            })
            .addCase(getReplies.fulfilled, (state: ThreadType, {payload}: PayloadAction<any>) => {
                state.threadLoading = false;
                payload.forEach(
                    (reply: ReplyType) => {
                        state.comments.forEach(comment => comment.id === reply.commentId && comment.data.replies.push(reply));
                    }
                );
            })
            .addCase(getReplies.pending, (state: ThreadType) => {
                state.threadLoading = true;
            })
            .addCase(getReplies.rejected, (state: ThreadType, {payload}: PayloadAction<any>) => {
                state.threadLoading = false;
                state.threadError = payload.message;
            })
            .addCase(deleteReply.fulfilled, (state: ThreadType, {payload}: PayloadAction<any>) => {
                state.threadLoading = false;
                state.comments = repliesToComments(state.comments, payload);
            })
            .addCase(deleteReply.pending, (state: ThreadType) => {
                state.threadLoading = true;
            })
            .addCase(deleteReply.rejected, (state: ThreadType, {payload}: PayloadAction<any>) => {
                state.threadLoading = false;
                state.threadError = payload.message;
            })
            .addCase(likeReply.fulfilled, (state: ThreadType, {payload}: PayloadAction<any>) => {
                state.threadLoading = false;
                state.comments = repliesToComments(state.comments, payload);
            })
            .addCase(likeReply.pending, (state: ThreadType) => {
                state.threadLoading = true;
            })
            .addCase(likeReply.rejected, (state: ThreadType, {payload}: PayloadAction<any>) => {
                state.threadLoading = false;
                state.threadError = payload.message;
            })
            .addCase(deleteReplyWithComment.fulfilled, (state: ThreadType) => {
                state.threadLoading = false;
            })
            .addCase(deleteReplyWithComment.pending, (state: ThreadType) => {
                state.threadLoading = true;
            })
            .addCase(deleteReplyWithComment.rejected, (state: ThreadType, {payload}: PayloadAction<any>) => {
                state.threadLoading = false;
                state.threadError = payload.message;
            })
    }
});

export const ThreadSlice = threadSlice.actions;
export const ThreadReducer = threadSlice.reducer;
