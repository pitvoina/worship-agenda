import {configureStore} from '@reduxjs/toolkit';
import thunk from 'redux-thunk';

import {UserReducer} from './slices/user/User.slice';
import {ForumReducer} from './slices/forum/Forum.slice';
import {ThreadReducer} from './slices/thread/Thread.slice';
import {CalendarReducer} from './slices/calendar/Calendar.slice';
import {ChatReducer} from './slices/chat/Chat.slice';

export const store = configureStore({
    reducer: {
        user: UserReducer,
        forum: ForumReducer,
        thread: ThreadReducer,
        calendar: CalendarReducer,
        chat: ChatReducer,
    },
    middleware: [thunk]
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
