import * as React from 'react';
import {StyleSheet, ScrollView, View} from 'react-native';
import {Colors} from '../theme/Colors';
import {ProfileImage} from '../components/profile/ProfileImage';
import {ProfileData} from '../components/profile/ProfileData';
import Constants from 'expo-constants';
import {Menu} from '../components/menu/Menu';
import * as NavigationBar from 'expo-navigation-bar';
import {EditProfile} from '../components/profile/EditProfile';
import {useSelector} from 'react-redux';
import {RootState} from '../store/store';
import {StatusBar} from 'expo-status-bar';
import {Loader} from '../components/loader/Loader';
import {Error} from '../components/error/Error';
import {UserSlice} from '../store/slices/user/User.slice';

export const ProfileScreen = () => {
    NavigationBar.setBackgroundColorAsync(Colors.o3_dark).then();
    const {clearUserError} = UserSlice;
    const {userLoading, isEditData, userError} = useSelector((state: RootState) => state.user);
    return (
        <View style={ styles.container }>
            <StatusBar style={ 'light' }/>
            <ScrollView style={ {flexGrow: 1} }>
                <ProfileImage/>
                { isEditData ?
                    <EditProfile/>
                    :
                    <ProfileData/>
                }
            </ScrollView>
            <Menu/>
            <Loader isLoading={ userLoading }/>
            <Error error={ userError } clearError={ clearUserError }/>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.od_dark,
        flexGrow: 1,
        paddingTop: Constants.statusBarHeight,
    },
});
