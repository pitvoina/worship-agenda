import * as NavigationBar from 'expo-navigation-bar';
import * as React from 'react';
import Constants from 'expo-constants';
import {Colors} from '../theme/Colors';
import {RegisterForm} from '../components/register/RegisterForm';
import {RegisterHeader} from '../components/register/RegisterHeader';
import {ScrollView, StyleSheet, View} from 'react-native';
import {StatusBar} from 'expo-status-bar';
import {Error} from '../components/error/Error';
import {useSelector} from 'react-redux';
import {RootState} from '../store/store';
import {UserSlice} from '../store/slices/user/User.slice';

export const RegisterScreen = () => {
    NavigationBar.setBackgroundColorAsync(Colors.white).then();
    const {clearUserError} = UserSlice;
    const {userError} = useSelector((state: RootState) => state.user);

    return (
        <View style={ styles.container }>
            <ScrollView>
                <StatusBar style={ 'dark' }/>
                <RegisterHeader/>
                <RegisterForm/>
            </ScrollView>
            <Error error={ userError } clearError={ clearUserError }/>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        flex: 1,
        flexGrow: 1,
        marginTop: Constants.statusBarHeight
    },
});
