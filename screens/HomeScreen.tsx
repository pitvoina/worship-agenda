import * as React from 'react';
import {View, StyleSheet} from 'react-native';
import {HomeHeader} from '../components/home/HomeHeader';
import {Colors} from '../theme/Colors';
import {Menu} from '../components/menu/Menu';
import {NewsSideScroll} from '../components/news/NewsSideScroll';
import {HomeActivity} from '../components/home/HomeActivity';
import * as NavigationBar from 'expo-navigation-bar';
import {StatusBar} from 'expo-status-bar';
import {CalendarViewAppointment} from '../components/calendar/CalendarViewAppointment';
import {useEffect, useState} from 'react';
import {AppointmentType, CalendarSlice} from '../store/slices/calendar/Calendar.slice';
import {Error} from '../components/error/Error';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../store/store';
import {getAppointments} from '../store/slices/calendar/Calendar.thunk';
import {getUsers} from '../store/slices/user/User.thunk';

export const HomeScreen = () => {
    NavigationBar.setBackgroundColorAsync(Colors.o3_dark).then();
    const dispatch = useDispatch();
    const {userId} = useSelector((state: RootState) => state.user);
    const {clearCalendarError} = CalendarSlice;
    const {calendarError} = useSelector((state: RootState) => state.calendar);
    const [activityData, setActivityData] = useState<any>(null);

    const getActivityData = (appointment: AppointmentType) => {
        setActivityData(appointment);
    }

    useEffect(() => {
        dispatch(getAppointments({userId: String(userId)}));
        dispatch(getUsers());
    }, [userId]);

    return (
        <View style={ styles.container }>
            <StatusBar style={ 'dark' }/>
            <HomeHeader/>
            <NewsSideScroll/>
            <HomeActivity getActivityData={ getActivityData }/>
            <Menu/>
            { activityData && <CalendarViewAppointment
                activityData={ activityData }
                close={ () => setActivityData(null) }
            /> }
            <Error error={ calendarError } clearError={ clearCalendarError }/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.ob_dark,
        flexGrow: 1,
    },
});
