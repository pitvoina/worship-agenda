import * as React from 'react';
import {StyleSheet, View} from 'react-native';
import {Colors} from '../theme/Colors';
import {StatusBar} from 'expo-status-bar';
import * as NavigationBar from 'expo-navigation-bar';
import Constants from 'expo-constants';
import {ForumNewThreadHeader} from '../components/forum/thread/short/ForumNewThreadHeader';
import {ForumNewThreadBody} from '../components/forum/thread/short/ForumNewThreadBody';

export const NewThreadScreen = () => {
    NavigationBar.setBackgroundColorAsync(Colors.o3_dark).then();

    return (
        <View style={ styles.container }>
            <StatusBar style={ 'light' }/>
            <ForumNewThreadHeader/>
                <ForumNewThreadBody/>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.o3_dark,
        flex: 1,
        paddingTop: Constants.statusBarHeight,
    },
})
