import * as React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Colors} from '../theme/Colors';
import {StatusBar} from 'expo-status-bar';
import Constants from 'expo-constants';
import {CalendarHeader} from '../components/calendar/CalendarHeader';
import {CalendarBody} from '../components/calendar/CalendarBody';
import {Loader} from '../components/loader/Loader';
import {useSelector} from 'react-redux';
import {RootState} from '../store/store';
import {CalendarUserList} from '../components/calendar/CalendarUserList';

export const CalendarScreen = () => {
    const {isLoading, showUserList} = useSelector((state: RootState) => state.calendar);

    return (
        <View style={ styles.container }>
            <StatusBar style={ 'dark' } backgroundColor={ Colors.white }/>
            <CalendarHeader/>
            <CalendarBody/>
            { showUserList && <CalendarUserList/> }
            <Loader isLoading={ isLoading }/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        paddingTop: Constants.statusBarHeight,
        position: 'relative',
    }
});
