import * as React from 'react';
import {Colors} from '../theme/Colors';
import {ProfileChatButtonGroup} from '../components/buttons/ProfileChatButtonGroup';
import {ScrollView, StyleSheet, View} from 'react-native';
import {StatusBar} from 'expo-status-bar';
import {Text} from '../components/text/Text';
import * as NavigationBar from 'expo-navigation-bar';
import {Menu} from '../components/menu/Menu';
import Constants from 'expo-constants';
import {heightPx, widthPx} from '../helpers/Dimensions';
import {useState} from 'react';
import {SearchComponent} from '../components/chat/SearchComponent';
import {UserChats} from '../components/chat/UserChats';
import {Chat} from '../components/chat/Chat';
import {UserList} from '../components/chat/UserList';

export const ChatScreen = () => {
    NavigationBar.setBackgroundColorAsync(Colors.o3_dark).then();
    const [showUsers, setShowUsers] = useState(false);
    const [openChat, setOpenChat] = useState(false);

    return (
        <View style={ styles.container }>
            {/*<StatusBar style={ 'dark' } backgroundColor={ Colors.white }/>*/}
            { openChat ?
                <Chat close={ () => setOpenChat(false) }/>
                :
                <>
                    <View style={ styles.headerContainer }>
                        <Text
                            color={ Colors.ob_dark }
                            content={ 'Chat' }
                            fontFamily={ 'Poppins semiBold' }
                            size={ 35 }
                            style={ styles.headerText }
                        />
                        <ProfileChatButtonGroup
                            profileIconColor={ Colors.ob_dark }
                            chatIconColor={ Colors.ob_dark }
                        />
                    </View>
                    <ScrollView>
                        <>
                            <SearchComponent setShowUsers={ setShowUsers }/>
                            <UserChats openChat={ setOpenChat }/>
                        </>
                    </ScrollView>
                    <Menu/>
                </>
            }
            { showUsers && <UserList close={ () => setShowUsers(false) }/> }
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: Constants.statusBarHeight,
        position: 'relative',
    },
    headerContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: widthPx(25),
        paddingVertical: heightPx(15),
    },
    headerText: {
        marginRight: 'auto'
    }
});
