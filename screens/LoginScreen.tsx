import * as React from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import {Colors} from '../theme/Colors'
import {LoginHeader} from '../components/login/LoginHeader';
import {SignInBackground} from '../svg/SignInBackground';
import {LoginForms} from '../components/login/LoginForms';
import {StatusBar} from 'expo-status-bar';
import * as NavigationBar from 'expo-navigation-bar';
import Constants from 'expo-constants';
import {Loader} from '../components/loader/Loader';
import {useSelector} from 'react-redux';
import {RootState} from '../store/store';
import {Error} from '../components/error/Error';
import {UserSlice} from '../store/slices/user/User.slice';

export const LoginScreen = () => {
    NavigationBar.setBackgroundColorAsync(Colors.black).then();
    const {clearUserError} = UserSlice;
    const {userLoading, userError} = useSelector((state: RootState) => state.user);

    return (
        <View style={ styles.container }>
            <ScrollView>
                <SignInBackground/>
                <StatusBar style={ 'dark' }/>
                <LoginHeader/>
                <LoginForms/>
            </ScrollView>
            <Loader isLoading={ userLoading }/>
            <Error error={ userError } clearError={ clearUserError }/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.black,
        flex: 1,
        flexGrow: 1,
        marginTop: Constants.statusBarHeight
    },
});
