import * as NavigationBar from 'expo-navigation-bar';
import * as React from 'react';
import Constants from 'expo-constants';
import {BackButton} from '../components/buttons/BackButton';
import {Colors} from '../theme/Colors';
import {FullThreadBody} from '../components/forum/thread/full/FullThreadBody';
import {FullThreadHeader} from '../components/forum/thread/full/FullThreadHeader';
import {FullThreadStats} from '../components/forum/thread/full/FullThreadStats';
import {ProfileChatButtonGroup} from '../components/buttons/ProfileChatButtonGroup';
import {StatusBar} from 'expo-status-bar';
import {Keyboard, ScrollView, StyleSheet, View} from 'react-native';
import {heightPx, widthPercent, widthPx} from '../helpers/Dimensions';
import {FontAwesome, AntDesign, MaterialCommunityIcons} from '@expo/vector-icons';
import {AppInput} from '../components/form/AppInput';
import {useEffect, useState} from 'react';
import {FullThreadComments} from '../components/forum/thread/full/FullThreadComments';
import {Stats} from '../components/forum/thread/full/Stats';
import {Loader} from '../components/loader/Loader';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../store/store';
import {Error} from '../components/error/Error';
import {ThreadSlice} from '../store/slices/thread/Thread.slice';
import {getThread, getThreadUsers, setFavoriteThead, setLikeThread, setViews, setComment, getComments, setReply, getReplies} from '../store/slices/thread/Thread.thunk';
import Animated, {Easing, useAnimatedStyle, withTiming} from 'react-native-reanimated';

export const ForumThreadScreen = ({route}: any) => {
    NavigationBar.setBackgroundColorAsync(Colors.o3_dark).then();
    const dispatch = useDispatch();
    const {setLike, setFavorite, setShowWrite, setWriteParentId, setWriteType} = ThreadSlice;
    const {threadId} = route.params;
    const {clearThreadError} = ThreadSlice;
    const {userId, role, photo, name} = useSelector((state: RootState) => state.user);
    const {
        comments,
        content,
        createdAt,
        createdBy,
        favorite,
        likes,
        showWrite,
        stats,
        threadError,
        threadLoading,
        title,
        userName,
        userPhoto,
        userRole,
        writeParentId,
        writeType,
        views,
    } = useSelector((state: RootState) => state.thread);
    const [commentText, setCommentText] = useState('');
    const [showStats, setShowStats] = useState(false);

    useEffect(() => {
        const id = threadId.split('_')[1];

        dispatch(getThread({threadId: id}));
        dispatch(getComments({threadId: id}));
        dispatch(getReplies({threadId: id}));
        dispatch(setViews({threadId: threadId.split('_')[1], userId: String(userId)}))
    }, []);

    const onSetFavorite = () => {
        dispatch(setFavoriteThead({threadId: threadId.split('_')[1], userId: String(userId), isFav: !favorite.includes(`${ userId }`)}));
        dispatch(setFavorite({userId: `${ userId }`, add: !favorite.includes(`${ userId }`)}));
    }

    const onSetLike = () => {
        dispatch(setLikeThread({threadId: threadId.split('_')[1], userId: String(userId), isLike: !likes.includes(`${ userId }`)}));
        dispatch(setLike({userId: `${ userId }`, add: !likes.includes(`${ userId }`)}));
    }

    const showLikes = () => {
        if (likes.length === 0) return;
        setShowStats(true);
        dispatch(getThreadUsers({userIds: likes, type: 'likes'}));
    }

    const showViews = () => {
        if (views.length === 0) return;
        setShowStats(true);
        dispatch(getThreadUsers({userIds: views, type: 'views'}));
    }

    const writeComment = () => {
        let id = threadId.split('_')[1];
        switch (true) {
            case (showWrite && writeParentId !== id):
                dispatch(setWriteParentId(id));
                dispatch(setWriteType('comment'))
                break;
            case (!showWrite):
                dispatch(setWriteParentId(id));
                dispatch(setShowWrite(true));
                dispatch(setWriteType('comment'))
                break;
            default:
                dispatch(setShowWrite(false));
                dispatch(setWriteType(null));
                break;
        }
    }

    const submitComment = () => {
        if (!writeParentId || !commentText || !userId) return;
        Keyboard.dismiss();
        setCommentText('');
        if (writeType === 'comment') {
            dispatch(setComment({parentId: writeParentId, commentText, userId, photo, role, name}));
        } else if (writeType === 'reply') {
            dispatch(setReply({threadId: threadId.split('_')[1], parentId: writeParentId, commentText, userId, photo, role, name}));
        }
    }

    const config = {duration: 500, easing: Easing.bezier(0.5, 0.01, 0, 1)};
    const animatedStyle = useAnimatedStyle(() => {
        return {
            opacity: withTiming(showWrite ? 1 : 0, config),
            left: withTiming(showWrite ? '0%' : '-100%', config)
        };
    });

    return (
        <View style={ [styles.container, {paddingBottom: showWrite ? heightPx(85) : 0}] }>
            <ScrollView>
                <StatusBar style={ 'light' } backgroundColor={ Colors.od_dark }/>
                <View style={ styles.headerContainer }>
                    <BackButton
                        style={ {top: heightPx(5)} }
                        iconColor={ Colors.white }
                    />
                    <ProfileChatButtonGroup
                        profileIconColor={ Colors.white }
                        chatIconColor={ Colors.white }
                    />
                </View>
                <View style={ styles.threadHead }>
                    <FullThreadHeader
                        authorId={ createdBy }
                        date={ createdAt }
                        img={ userPhoto }
                        name={ userName }
                        role={ userRole }
                    />
                    <FullThreadBody title={ title } text={ content }/>
                    <View style={ styles.actions }>
                        <FontAwesome
                            onPress={ () => onSetFavorite() }
                            color={ (favorite && favorite.includes(`${ userId }`) ? Colors.red : Colors.white) }
                            name={ (favorite && favorite.includes(`${ userId }`) ? 'star' : 'star-o') }
                            size={ 30 }
                            style={ styles.iconStyle }
                        />
                        <AntDesign
                            onPress={ () => onSetLike() }
                            color={ (likes && likes.includes(`${ userId }`) ? Colors.red : Colors.white) }
                            name={ (likes && likes.includes(`${ userId }`) ? 'like1' : 'like2') }
                            size={ 30 }
                            style={ styles.iconStyle }
                        />
                        <MaterialCommunityIcons
                            onPress={ () => writeComment() }
                            color={ Colors.white }
                            name={ "message-outline" }
                            size={ 30 }
                            style={ styles.iconStyle }
                        />
                    </View>
                    <FullThreadStats
                        comments={ `${ comments ? comments.length : 0 }` }
                        likes={ `${ likes.length }` }
                        views={ `${ views.length }` }
                        likesClick={ showLikes }
                        viewsClick={ showViews }
                    />
                </View>
                <FullThreadComments/>
            </ScrollView>
            <Animated.View style={ [styles.writeComment, animatedStyle] }>
                <AppInput
                    autoCorrect={ false }
                    inputContainerStyle={ styles.inputContainer }
                    inputStyle={ styles.input }
                    multiline={ true }
                    setValueFn={ setCommentText }
                    value={ commentText }
                    inputPlaceholder={ 'Write a comment' }
                    actionButton={ {icon: 'send-o', action: () => submitComment()} }
                />
            </Animated.View>
            {
                (showStats && stats) &&
                <Stats
                    stats={ stats }
                    close={ () => setShowStats(false) }
                />
            }
            <Loader isLoading={ !!threadLoading }/>
            <Error error={ String(threadError) } clearError={ clearThreadError }/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.od_dark,
        flexGrow: 1,
        paddingTop: Constants.statusBarHeight,
        position: 'relative',
    },
    actions: {
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    iconStyle: {
        marginVertical: heightPx(20),
    },
    headerContainer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    threadHead: {
        paddingHorizontal: widthPx(15),
        marginTop: heightPx(10),
    },
    input: {
        alignItems: 'center',
        height: 'auto',
        justifyContent: 'center',
        maxHeight: heightPx(300),
        paddingBottom: heightPx(10),
        paddingLeft: widthPx(10),
        paddingTop: heightPx(10),
        width: widthPercent(100) - 80,
    },
    inputContainer: {
        backgroundColor: Colors.white,
        borderColor: Colors.od_dark,
        borderRadius: 20,
        borderWidth: .5,
        justifyContent: 'center',
        marginHorizontal: widthPx(15),
        paddingLeft: widthPx(15),
        width: widthPercent(100) - 30,
    },
    writeComment: {
        backgroundColor: Colors.black,
        paddingVertical: heightPx(20),
        position: 'absolute',
        bottom: -25,
    }
});
