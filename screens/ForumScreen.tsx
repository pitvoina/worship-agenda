import * as React from 'react';
import {StyleSheet, View} from 'react-native';
import Animated, {Easing, useAnimatedStyle, withTiming} from 'react-native-reanimated';
import {Menu} from '../components/menu/Menu';
import * as NavigationBar from 'expo-navigation-bar';
import {Colors} from '../theme/Colors';
import {ForumHeader} from '../components/forum/ForumHeader';
import {ForumBody} from '../components/forum/ForumBody';
import {IconButton} from '../components/buttons/IconButton';
import {heightPx, widthPercent, widthPx} from '../helpers/Dimensions';
import {useEffect, useState} from 'react';
import {useIsFocused, useNavigation} from '@react-navigation/native';
import {StatusBar} from 'expo-status-bar';
import {Loader} from '../components/loader/Loader';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../store/store';
import {Error} from '../components/error/Error';
import {ForumSlice} from '../store/slices/forum/Forum.slice';
import {getForumThreads} from '../store/slices/forum/Forum.thunk';

export const ForumScreen = () => {
    NavigationBar.setBackgroundColorAsync(Colors.o3_dark).then();
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const isFocused = useIsFocused();
    const {clearForumError} = ForumSlice;
    const {forumLoading, forumError} = useSelector((state: RootState) => state.forum);
    const [showNewThread, setShowNewThread] = useState(true);


    useEffect(() => {
        dispatch(getForumThreads());
    }, [isFocused]);

    const onScrollStart = () => {
        setShowNewThread(false);
    }

    const onScrollEnd = () => {
        setTimeout(() => setShowNewThread(true), 1500);
    }

    const config = {duration: 500, easing: Easing.bezier(0.5, 0.01, 0, 1)};

    const style = useAnimatedStyle(() => {
        return {
            opacity: withTiming(showNewThread ? 1 : 0, config),
            bottom: withTiming(showNewThread ? 80 : 0, config)
        };
    });

    return (
        <View
            style={ styles.container }
        >
            <StatusBar style={ 'dark' }/>
            <Animated.View
                style={ [styles.animatedView, style] }
            >
                <IconButton
                    iconName={ 'plus' }
                    onPress={ () => navigation.navigate('NewThread') }
                    style={ [
                        styles.newThreadButton,
                    ] }
                    size={ 35 }
                    iconColor={ Colors.white }/>
            </Animated.View>
            <ForumHeader/>
            <ForumBody
                onScrollStart={ () => onScrollStart() }
                onScrollEnd={ () => onScrollEnd() }
            />
            <Menu/>
            <Loader isLoading={ forumLoading }/>
            <Error error={ forumError } clearError={ clearForumError }/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        flexGrow: 1,
    },
    newThreadButton: {
        alignItems: 'center',
        backgroundColor: Colors.light_teal,
        borderRadius: 40,
        height: widthPx(60),
        justifyContent: 'center',
        padding: 0,
        width: widthPx(60),

    },
    animatedView: {
        display: 'flex',
        width: widthPx(60),
        height: widthPx(60),
        position: 'absolute',
        right: widthPercent(5),
        bottom: heightPx(80),
        zIndex: 1
    }
});
