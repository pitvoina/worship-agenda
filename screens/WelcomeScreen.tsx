import * as React from 'react';
import {useEffect} from 'react';
import {StyleSheet, View} from 'react-native';
import {WelcomeGradient} from '../components/welcome/WelcomeGradient';
import * as NavigationBar from 'expo-navigation-bar';
import {Colors} from '../theme/Colors';
import {auth} from '../config';
import {useDispatch, useSelector} from 'react-redux';
import {getUserData} from '../store/slices/user/User.thunk';
import {UserSlice} from '../store/slices/user/User.slice';

export const WelcomeScreen = () => {
    NavigationBar.setBackgroundColorAsync(Colors.white).then();
    const dispatch = useDispatch();
    const {setId, setEmail, setAuth} = UserSlice;

    useEffect(() => {
        auth.onAuthStateChanged((userData) => {
            if (userData !== null) {
                dispatch(getUserData(userData.uid));
                dispatch(setAuth(true));
                dispatch(setEmail(userData.email ? userData.email : ''));
                dispatch(setId(userData.uid));
            }
        });
    }, []);

    return (
        <View style={ styles.container }>
            <WelcomeGradient/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
