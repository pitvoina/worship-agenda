import * as React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {Button} from 'react-native-paper';
import {Colors} from '../../theme/Colors';
import {fontScale, heightPercent, heightPx, widthPx} from '../../helpers/Dimensions';

export const WelcomeFooter = () => {
    const navigation = useNavigation();

    return (
        <View style={ styles.container }>
            <Text style={ styles.header }>Welcome</Text>
            <Text style={ styles.text }>to WorshipAgenda</Text>
            <Button
                uppercase={ false }
                mode={ 'contained' }
                onPress={ () => navigation.navigate('Login') }
                labelStyle={ styles.labelStyle }
                style={ styles.loginButton }
            >{ 'Login' }</Button>
            <Button
                uppercase={ false }
                mode={ 'contained' }
                onPress={ () => navigation.navigate('Register') }
                labelStyle={ styles.labelStyle }
                style={ styles.signupButton }
            >{ 'Sign Up' }</Button>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: heightPercent(60),
        alignItems: 'center',
    },
    header: {
        color: Colors.teal,
        fontFamily: 'Poppins bold',
        fontSize: fontScale(45),
        letterSpacing: .6,
        lineHeight: fontScale(50),
        marginTop: heightPercent(14),
    },
    text: {
        color: Colors.teal,
        fontFamily: 'Poppins semiBold',
        fontSize: fontScale(25),
        letterSpacing: .5,
    },
    loginButton: {
        borderColor: Colors.black,
        backgroundColor: Colors.white,
        borderRadius: 0,
        borderWidth: 2,
        marginBottom: heightPx(35),
        marginTop: heightPx(50),
        paddingVertical: 3,
        width: widthPx(220),
    },
    signupButton: {
        backgroundColor: 'transparent',
        shadowColor: Colors.white,
        width: widthPx(220),
    },
    labelStyle: {
        color: Colors.black,
        fontSize: fontScale(20),
        fontFamily: 'Poppins semiBold'
    }
})
