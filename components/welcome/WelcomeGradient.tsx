import * as React from 'react';
import {StyleSheet, View} from 'react-native';
import {WelcomeLogo} from '../../svg/WelcomeLogo';
import {WelcomeBackground} from '../../svg/WelcomeBackground';
import {WelcomeFooter} from './WelcomeFooter';
import {heightPercent} from '../../helpers/Dimensions';
import {StatusBar} from 'expo-status-bar';

export const WelcomeGradient = () => {
    return (
        <View style={ styles.gradient }>
            <StatusBar style={ 'light' } />
            <WelcomeBackground/>
            <View style={ styles.logoContainer }>
                <WelcomeLogo/>
            </View>
            <WelcomeFooter/>
        </View>
    )
}

const styles = StyleSheet.create({
    gradient: {
        flex: 1,
        padding: 0,
        // paddingTop: Platform.OS === 'android' ? 50 : 0,
        position: 'relative',
    },
    logoContainer: {
        display: 'flex',
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
        height: heightPercent(40),
    },
})
