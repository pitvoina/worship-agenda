import * as React from 'react';
import {StyleSheet, Text, TextProps} from 'react-native';
import {Colors} from '../../theme/Colors';
import {fontScale, heightPx} from '../../helpers/Dimensions';

type MessageType = 'error' | 'info' | 'success';

interface Props extends TextProps {
    text: string,
    type: MessageType,
}

export const Message = (props: Props) => {
    const {type, text, style} = props;

    return (
        <Text style={ [styles.message, styles[type], style] }>{ text }</Text>
    );
}

const styles = StyleSheet.create({
    message: {
        paddingVertical: heightPx(2),
        fontSize: fontScale(14),
        height: 2 * fontScale(18),
    },
    ['error']: {
        color: Colors.red,
    },
    ['info']: {
        color: Colors.teal,
    },
    ['success']: {
        color: Colors.green,
    }
});
