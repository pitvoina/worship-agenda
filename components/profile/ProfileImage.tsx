import * as React from 'react';
import {useEffect} from 'react';
import {Image, Platform, StyleSheet, TouchableWithoutFeedback, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {fontScale, heightPx, widthPercent, widthPx} from '../../helpers/Dimensions';
import {BackButton} from '../buttons/BackButton';
import {IconButton} from '../buttons/IconButton';
import {Colors} from '../../theme/Colors';
import {FontAwesome5} from '@expo/vector-icons';
import {RootState} from '../../store/store';
import {useNavigation} from '@react-navigation/native';
import {UserSlice} from '../../store/slices/user/User.slice';
import * as ImagePicker from 'expo-image-picker';
import {uploadUserImg} from '../../store/slices/user/User.thunk';

export const ProfileImage = () => {
    const dispatch = useDispatch();
    const navigation = useNavigation();
    const {userId, photo, isEditData} = useSelector((state: RootState) => state.user);
    const {setIsEdit} = UserSlice;

    useEffect(() => {
        (async () => {
            if (Platform.OS !== 'web') {
                const {status} = await ImagePicker.requestMediaLibraryPermissionsAsync();
                if (status !== 'granted') {
                    alert('Sorry, we need camera roll permissions to make this work!');
                }
            }
        })();
    }, []);

    const pickImage = async () => {
        await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [1, 1],
            quality: 1,
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
        }).then(data => {
            if (!userId) return;
            dispatch(uploadUserImg({image: data, userId}));
        });
    }

    return (
        <View style={ styles.container }>
            { isEditData &&
            <BackButton
                iconColor={ Colors.white }
                onPressFn={ () => dispatch(setIsEdit(false)) }
                style={ styles.backButton }
                text={ 'Edit Profile' }
                textFontFamily={ 'Poppins medium' }
                textSize={ fontScale(25) }
            />
            }
            <IconButton
                iconColor={ Colors.white }
                onPress={ () => navigation.navigate('Chat') }
                style={ styles.chatIcon }
                iconName={ 'comment-dots' }
            />
            <TouchableWithoutFeedback
                onPress={ pickImage }
            ><View
                style={ styles.uploadImgBtn }
            >
                { photo ?
                    <Image source={ {uri: photo} } style={ styles.img }/>
                    :
                    (<>
                        <FontAwesome5
                            color={ Colors.white }
                            name={ 'user-circle' }
                            size={ fontScale(160) }
                            solid
                        />
                        <FontAwesome5
                            name={ 'plus' }
                            size={ fontScale(20) }
                            solid
                            style={ styles.plusIcon }
                        />
                    </>)
                }
            </View>
            </TouchableWithoutFeedback>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        paddingTop: heightPx(60),
        alignItems: 'center',
        position: 'relative',
        width: widthPercent(100),
    },
    img: {
        backgroundColor: 'red',
        height: widthPx(136),
        width: widthPx(136),
        borderRadius: widthPx(136),
        borderColor: 'gray',
        borderWidth: 1,
    },
    chatIcon: {
        right: 0,
        position: 'absolute'
    },
    plusIcon: {
        backgroundColor: Colors.light_teal,
        paddingBottom: heightPx(4),
        paddingTop: heightPx(5),
        paddingLeft: widthPx(7),
        paddingRight: widthPx(6),
        borderRadius: widthPx(50),
        bottom: heightPx(10),
        right: widthPx(10),
        position: 'absolute',
    },
    uploadImgBtn: {
        position: 'relative'
    },
    backButton: {
        top: heightPx(-2),
    },
})
