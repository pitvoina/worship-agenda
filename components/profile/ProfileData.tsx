import * as React from 'react';
import {Colors} from '../../theme/Colors';
import {FontAwesome5} from '@expo/vector-icons';
import {RootState} from '../../store/store';
import {Spacing, Text} from '../text/Text';
import {StyleSheet, TouchableHighlight, TouchableWithoutFeedback, View} from 'react-native';
import {heightPx, widthPx} from '../../helpers/Dimensions';
import {logOut} from '../../store/slices/user/User.thunk';
import {useDispatch, useSelector} from 'react-redux';
import {useState} from 'react';
import {UserSlice} from '../../store/slices/user/User.slice';

export const ProfileData = () => {
    const dispatch = useDispatch();
    const {userEmail, name, phoneNumber, noUserData, role} = useSelector((state: RootState) => state.user);
    const {setIsEdit} = UserSlice;
    const [editButtonTextColor, setEditButtonTextColor] = useState(Colors.light_teal);

    const valueSpacing: Spacing = {position: 'bottom', size: 30};
    const labelSpacing: Spacing = {position: 'bottom', size: 5};
    const valueSize = 22;
    const labelSize = 18;

    const labelFontFamily = 'Poppins medium';
    const valueFontFamily = 'Poppins semiBold';

    return (
        <View style={ styles.dataContainer }>
            { noUserData !== null && <Text content={ noUserData }/> }
            <View style={ styles.headTxt }>
                <Text content={ name } size={ 30 } fontFamily={ labelFontFamily }/>
                <Text content={ role } size={ 22 } fontFamily={ valueFontFamily }/>
            </View>

            <View style={ styles.details }>
                <Text content={ 'E-mail Address' } spacing={ labelSpacing } size={ labelSize } fontFamily={ labelFontFamily }/>
                <Text spacing={ valueSpacing } content={ userEmail } size={ valueSize } fontFamily={ valueFontFamily }/>

                <Text content={ 'Phone number' } spacing={ labelSpacing } size={ labelSize } fontFamily={ labelFontFamily }/>
                <Text spacing={ valueSpacing } content={ phoneNumber } size={ valueSize } fontFamily={ valueFontFamily }/>

                <Text content={ 'Ministry' } spacing={ labelSpacing } size={ labelSize } fontFamily={ labelFontFamily }/>
                <Text spacing={ valueSpacing } content={ role } size={ valueSize } fontFamily={ valueFontFamily }/>
            </View>
            <View style={ styles.editButton }>
                <TouchableHighlight
                    activeOpacity={ 1 }
                    onHideUnderlay={ () => setEditButtonTextColor(Colors.light_teal) }
                    onPress={ () => dispatch(setIsEdit(true)) }
                    onShowUnderlay={ () => setEditButtonTextColor(Colors.ob_dark) }
                    style={ {borderRadius: 4} }
                    underlayColor={ Colors.highlightTeal }
                >
                    <View style={ [
                        styles.buttonContent,
                        {borderColor: editButtonTextColor},
                    ] }>
                        <FontAwesome5
                            name={ 'edit' }
                            size={ 20 }
                            color={ editButtonTextColor }
                            solid
                            style={ {marginRight: widthPx(15), paddingTop: heightPx(2)} }
                        />
                        <Text color={ editButtonTextColor } content={ 'Edit profile' } size={ 25 } fontFamily={ 'Poppins semiBold' }/>
                    </View>
                </TouchableHighlight>
                <TouchableWithoutFeedback
                    onPress={ () => dispatch(logOut()) }
                >
                    <View style={ styles.logout }>
                        <FontAwesome5
                            name={ 'sign-out-alt' }
                            size={ 20 }
                            color={ Colors.red }
                            solid
                            style={ {marginRight: widthPx(15), paddingTop: heightPx(2)} }
                        />
                        <Text content={ 'Log out' } color={ Colors.red } size={ 25 } fontFamily={ 'Poppins semiBold' }/>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    dataContainer: {
        paddingVertical: heightPx(20),
    },
    headTxt: {
        alignItems: 'center',
        marginBottom: heightPx(40),
        paddingHorizontal: widthPx(25),
    },
    details: {
        paddingTop: heightPx(15),
        paddingHorizontal: widthPx(25),
    },
    editButton: {
        borderTopWidth: .5,
        borderColor: Colors.white,
        borderStyle: 'solid',
        paddingHorizontal: widthPx(25),
        paddingTop: heightPx(40),
        alignItems: 'center',
    },
    buttonContent: {
        borderStyle: 'solid',
        borderWidth: .5,
        flexDirection: 'row',
        paddingHorizontal: widthPx(25),
        paddingVertical: heightPx(10),
    },
    logout: {
        alignItems: 'center',
        borderColor: Colors.red,
        borderStyle: 'solid',
        borderWidth: .5,
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: heightPx(35),
        paddingHorizontal: widthPx(25),
        paddingVertical: heightPx(10),
    }
});
