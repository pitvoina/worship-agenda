import * as React from 'react';
import {StyleSheet, View} from 'react-native';
import {AppInput, Error} from '../form/AppInput';
import {useState} from 'react';
import {Colors} from '../../theme/Colors';
import {fontScale, heightPx, widthPx} from '../../helpers/Dimensions';
import {Button} from 'react-native-paper';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../../store/store';
import {setUserData} from '../../store/slices/user/User.thunk';
import {Validation} from '../../helpers/Validation';

export const EditProfile = () => {
    const dispatch = useDispatch();
    const {userId, name, role, phoneNumber} = useSelector((state: RootState) => state.user);

    const [fullName, setFullName] = useState(name);
    const [phone, setPhone] = useState(phoneNumber);
    const [ministry, setMinistry] = useState(role);
    const [submit, setSubmit] = useState(false);
    const [fullNameError, setFullNameError] = useState<Error>({message: '', hasError: false});
    const [phoneError, setPhoneError] = useState<Error>({message: '', hasError: false});

    const validateSubmit = () => {
        setSubmit(true);
        let nameValidation = Validation({value: fullName, type: 'name', isDirty: true});
        let phoneValidation = Validation({value: phone, type: 'phone', isDirty: true});
        setFullNameError(nameValidation);
        setPhoneError(phoneValidation);
        saveData(!!nameValidation?.hasError || !!phoneValidation?.hasError);

    }

    const saveData = (errors: boolean) => {
        if (errors) return;
        dispatch(setUserData({userId: userId, name: fullName, phoneNumber: phone, role: ministry}));
    }

    return (
        <View style={ styles.container }>
            <AppInput
                autoCorrect={ false }
                borderColor={ Colors.white }
                inputContainerStyle={ styles.inputContainerStyle }
                inputLabel={ 'Name' }
                inputDirty={ false }
                inputPlaceholder={ 'ex: Jhonny' }
                inputStyle={ styles.input }
                isPassword={ false }
                error={ fullNameError }
                labelStyle={ {color: Colors.white, fontFamily: 'Poppins medium', marginBottom: 5} }
                setValueFn={ setFullName }
                submit={ submit }
                setInputError={ setFullNameError }
                type={ 'name' }
                validate={ true }
                value={ fullName }
            />
            <AppInput
                autoCorrect={ false }
                borderColor={ Colors.white }
                inputContainerStyle={ styles.inputContainerStyle }
                inputLabel={ 'Phone number' }
                setInputError={ setPhoneError }
                inputDirty={ false }
                inputPlaceholder={ 'ex: +40 755 000 000' }
                inputStyle={ styles.input }
                isPassword={ false }
                error={ phoneError }
                labelStyle={ {color: Colors.white, fontFamily: 'Poppins medium', marginBottom: 5} }
                setValueFn={ setPhone }
                submit={ submit }
                type={ 'phone' }
                validate={ true }
                value={ phone }
            />
            <AppInput
                autoCorrect={ false }
                borderColor={ Colors.white }
                inputContainerStyle={ styles.inputContainerStyle }
                inputLabel={ 'Ministry' }
                inputDirty={ false }
                inputPlaceholder={ 'ex: Lead guitar' }
                inputStyle={ styles.input }
                isPassword={ false }
                labelStyle={ {color: Colors.white, fontFamily: 'Poppins medium', marginBottom: 5} }
                setValueFn={ setMinistry }
                submit={ submit }
                type={ 'ministry' }
                value={ ministry }
            />
            <Button
                mode={ 'contained' }
                onPress={ () => validateSubmit() }
                uppercase={ false }
                labelStyle={ styles.buttonLabel }
                style={ styles.save }
            >{ 'Save changes' }</Button>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        marginTop: heightPx(25),
        marginBottom: 15
    },
    input: {
        color: Colors.o3_dark,
    },
    inputContainerStyle: {
        backgroundColor: Colors.white,
        borderWidth: 2,
        paddingBottom: heightPx(10),
        paddingHorizontal: widthPx(10),
        paddingTop: heightPx(10),
    },
    buttonLabel: {
        color: Colors.o3_dark,
        fontSize: fontScale(22),
        fontFamily: 'Poppins semiBold',
    },
    save: {
        backgroundColor: Colors.light_teal,
        width: widthPx(240),
        marginVertical: heightPx(20),
        borderRadius: 0,

    }
});



