import * as React from 'react';
import {StyleSheet, View} from 'react-native';
import {SearchComponent} from '../search-component/SearchComponent';
import {useEffect, useState} from 'react';
import {ForumThreadShort} from './thread/short/ForumThreadShort';
import {useSelector} from 'react-redux';
import {RootState} from '../../store/store';
import {ThreadType} from '../../store/slices/thread/Thread.slice';

interface Props {
    onScrollStart?: () => void,
    onScrollEnd?: () => void,
}

export const ForumBody = (props: Props) => {
    const {onScrollStart, onScrollEnd} = props;
    const {threads} = useSelector((state: RootState) => state.forum);
    const [results, setResults] = useState<ThreadType[]>([]);

    const searchFn = (value: string) => {
        let res: ThreadType[] = [];
        const regex = new RegExp(value, 'g');

        threads.map(item => item.title.match(regex) && res.push(item));
        setResults(res);
    }

    useEffect(() => {
        setResults(threads);
    }, [threads]);


    return (
        <View style={ styles.container }>
            <SearchComponent
                onScrollStart={ onScrollStart }
                onScrollEnd={ onScrollEnd }
                searchFn={ searchFn }
                searchResults={
                    results && results.map((result) =>
                        <ForumThreadShort
                            authorId={ result.createdBy }
                            comments={result.comments}
                            date={ result.createdAt }
                            favorite={result.favorite}
                            img={ result.userPhoto }
                            key={ result.threadId }
                            likes={result.likes}
                            name={ result.userName }
                            role={ result.userRole }
                            text={ result.content }
                            threadId={ result.threadId }
                            title={ result.title }
                            views={result.views}
                        />
                    )
                }
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexGrow: 1
    }
});
