import * as React from 'react';
import {StyleSheet, View} from 'react-native';
import {Text} from '../text/Text';
import {Colors} from '../../theme/Colors';
import Constants from 'expo-constants';
import {ProfileChatButtonGroup} from '../buttons/ProfileChatButtonGroup';
import {heightPx, widthPercent, widthPx} from '../../helpers/Dimensions';

export const ForumHeader = () => {
    return (
        <View style={ styles.container }>
            <Text content={ 'Forum' } color={ Colors.black } size={ 35 } fontFamily={ 'Poppins bold' } style={ styles.text }/>
            <ProfileChatButtonGroup/>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: widthPercent(5),
        paddingTop: Constants.statusBarHeight + heightPx(5),
    },
    text: {
        marginLeft: widthPx(35),
    }
})
