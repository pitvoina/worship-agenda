import * as React from 'react';
import {StyleSheet, View} from 'react-native';
import {Text} from '../../text/Text';
import {heightPx} from '../../../helpers/Dimensions';

interface Props {
    title: string,
    text: string,
    threadId: string | number,
}

export const ThreadBody = (props: Props) => {
    const {title, text, threadId} = props;

    return (
        <View style={ styles.container }>
            <Text
                content={ title }
                fontFamily={ 'Poppins semiBold' }
                size={ 20 }
                style={ styles.spacing }
            />
            <Text
                content={ text }
                numberOfLines={ 3 }
                size={ 14 }
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        marginBottom: heightPx(5)
    },
    spacing: {
        marginBottom: heightPx(10),
    }
})
