import * as React from 'react';
import {StyleSheet, TouchableWithoutFeedback, View} from 'react-native';
import {Entypo, FontAwesome, MaterialIcons} from '@expo/vector-icons';
import {Colors} from '../../../theme/Colors';
import {Text} from '../../text/Text';
import {widthPx} from '../../../helpers/Dimensions';

interface Props {
    favorite: boolean,
    threadId: string | number,
    views: string[],
}

export const ThreadFooter = (props: Props) => {
    const {favorite, threadId, views} = props;
    const iconSize: number = 25;

    return (
        <View style={ styles.container }>
            <FontAwesome
                color={ favorite ? Colors.red : Colors.white }
                name={ favorite ? 'star' : 'star-o' }
                size={ iconSize }
                style={ styles.iconStyle }
            />
            <MaterialIcons
                onPress={ () => console.log(`Comment to thread => ${ threadId }`) }
                color={ Colors.white }
                name={ 'chat-bubble-outline' }
                size={ iconSize }
                style={ styles.iconStyle }
            />
            <TouchableWithoutFeedback onPress={ () => console.log(`Show viewers => ${ threadId }`) }>
                <View style={ styles.views }>
                    <Entypo
                        color={ Colors.white }
                        name={ 'eye' }
                        size={ iconSize }
                    />
                    <Text content={ `${ views.length } ${ views.length === 1 ? 'view' : 'views' }` } size={ 12 }/>
                </View>
            </TouchableWithoutFeedback>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    iconStyle: {
        padding: 5,
        marginRight: widthPx(10)
    },
    views: {
        padding: 5,
        alignItems: 'center',
        marginLeft: 'auto'
    }
});
