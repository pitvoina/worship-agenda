import * as React from 'react';
import {StyleProp, StyleSheet, View, ViewStyle, Image} from 'react-native';
import {IconButton} from '../../buttons/IconButton';
import {Colors} from '../../../theme/Colors';
import {heightPx, widthPx} from '../../../helpers/Dimensions';
import {Text} from '../../text/Text';

interface Props {
    date: string,
    img: string | null,
    name: string,
    role: string,
    style?: StyleProp<ViewStyle> | undefined,
}

export const ThreadHeader = (props: Props) => {
    const {date, img, name, role, style} = props;
    const newDate = new Date(parseInt(date) * 1000);
    const formattedDate = `${ ('0' + newDate.getDay()).slice(-2) }.${ ('0' + newDate.getMonth()).slice(-2) }.${ newDate.getFullYear() }`;
    return (
        <View style={ [styles.container, style] }>
            <Text style={ styles.date } content={ formattedDate } size={ 12 }/>
            { img ?
                <Image source={ {uri: img} } style={ styles.userImage }/>
                :
                <IconButton iconName={ 'user-circle' } style={ styles.iconButton } iconColor={ Colors.white } size={ 50 }/> }
            <View>
                <Text content={ name } fontFamily={ 'Poppins bold' } size={ 18 }/>
                <Text content={ role } fontFamily={ 'Poppins regular' } size={ 12 }/>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flexDirection: 'row',
        marginBottom: heightPx(15),
        paddingTop: heightPx(5),
        position: 'relative',
    },
    iconButton: {
        marginRight: widthPx(15),
        padding: 0,
    },
    userImage: {
        borderColor: Colors.gray,
        borderRadius: 50,
        borderWidth: 1,
        height: widthPx(50),
        marginRight: widthPx(15),
        width: widthPx(50),
    },
    date: {
        position: 'absolute',
        right: 0,
        top: 0,
    }
})
