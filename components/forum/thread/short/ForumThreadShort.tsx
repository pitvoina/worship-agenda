import * as React from 'react';
import {Colors} from '../../../../theme/Colors';
import {CommentType} from '../../../../store/slices/thread/Thread.slice';
import {ForumSlice} from '../../../../store/slices/forum/Forum.slice';
import {Pressable, StyleSheet} from 'react-native';
import {RootState} from '../../../../store/store';
import {ThreadBody} from '../ThreadBody';
import {ThreadFooter} from '../ThreadFooter';
import {ThreadHeader} from '../ThreadHeader';
import {heightPx, widthPx} from '../../../../helpers/Dimensions';
import {setFavoriteThread} from '../../../../store/slices/forum/Forum.thunk';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

interface Props {
    authorId: string,
    comments: { id: string, data: CommentType }[],
    date: string,
    favorite: string[],
    img: string | null,
    likes: string[],
    name: string,
    role: string,
    text: string,
    threadId: string,
    title: string,
    views: string[],
}

export const ForumThreadShort = (props: Props) => {
    const {
        date,
        favorite,
        img,
        name,
        role,
        text,
        threadId,
        title,
        views
    } = props;
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const {userId} = useSelector((state: RootState) => state.user);
    const {setFavorite} = ForumSlice;

    const handLongPress = (isFav: boolean) => {
        dispatch(setFavorite({threadId: threadId, userId: String(userId), isFav: !isFav}));
        dispatch(setFavoriteThread({threadId: date, userId: String(userId), isFav}));
    }

    return (
        <Pressable
            style={ styles.container }
            onLongPress={ () => handLongPress(!favorite.includes(String(userId))) }
            onPress={ () => navigation.navigate('Thread', {threadId: threadId}) }
        >
            <ThreadHeader
                date={ date }
                img={ img }
                name={ name }
                role={ role }
            />
            <ThreadBody
                title={ title }
                text={ text }
                threadId={ threadId }
            />
            <ThreadFooter
                favorite={ favorite.includes(String(userId)) }
                threadId={ threadId }
                views={ views }
            />
        </Pressable>
    );
}

const styles = StyleSheet.create({
    container: {
        elevation: 5,
        backgroundColor: Colors.ob_dark,
        borderRadius: 20,
        paddingHorizontal: widthPx(15),
        paddingVertical: heightPx(15),
        marginBottom: heightPx(10)
    }
})
