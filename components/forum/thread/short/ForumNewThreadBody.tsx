import * as React from 'react';
import {AppInput} from '../../../form/AppInput';
import {Colors} from '../../../../theme/Colors';
import {RootState} from '../../../../store/store';
import {ScrollView, StyleSheet, TouchableWithoutFeedback, View} from 'react-native';
import {Text} from '../../../text/Text';
import {createNewThread} from '../../../../store/slices/forum/Forum.thunk';
import {fontScale, heightPx, widthPercent} from '../../../../helpers/Dimensions';
import {useDispatch, useSelector} from 'react-redux';
import {useState} from 'react';
import {useNavigation} from '@react-navigation/native';

export const ForumNewThreadBody = () => {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const {userId, name, photo, role} = useSelector((state: RootState) => state.user);
    const [titleVal, setTitleVal] = useState('Let`s talk about something');
    const [contentVal, setContentVal] = useState('Talking about something with no specific subject will get us nowhere fast and sure. But let`s see where we`ll end up!');

    const [textAreaBg, setTextAreaBg] = useState('transparent');
    const [inputBg, setInputBg] = useState('transparent');

    const handleCreateThread = () => {
        if (!userId) return;
        dispatch(createNewThread({
            content: contentVal,
            displayName: name,
            title: titleVal,
            userId,
            userPhoto: photo,
            userRole: role
        }));
        navigation.navigate('Forum');
    }

    return (
        <ScrollView
            style={ styles.scrollView }
            contentContainerStyle={ styles.container }
        >
            <View>
                <Text
                    content={ 'Start a forum discussion' }
                    size={ 30 }
                    fontFamily={ 'Poppins bold' }
                    style={ styles.header }
                />
                <AppInput
                    autoCorrect={ false }
                    inputContainerStyle={ styles.inputContainerStyle }
                    inputPlaceholder={ 'Thread title...' }
                    inputStyle={ {...styles.inputStyle, backgroundColor: inputBg} }
                    onBlur={ () => setInputBg('transparent') }
                    onFocus={ () => setInputBg('rgba(50,50,50,.1)') }
                    placeholderColor={ 'rgba(255,255,255, .3)' }
                    selectionColor={ 'rgba(255,255,255,.8)' }
                    setValueFn={ setTitleVal }
                    value={ titleVal }
                />
                <AppInput
                    autoCorrect={ false }
                    inputContainerStyle={ styles.textAreaContainer }
                    inputPlaceholder={ 'Thread content...' }
                    inputStyle={ {...styles.textArea, backgroundColor: textAreaBg} }
                    multiline={ true }
                    onBlur={ () => setTextAreaBg('transparent') }
                    onFocus={ () => setTextAreaBg('rgba(50,50,50,.1)') }
                    placeholderColor={ 'rgba(255,255,255, .3)' }
                    selectionColor={ 'rgba(255,255,255,.8)' }
                    setValueFn={ setContentVal }
                    value={ contentVal }
                />
            </View>
            <TouchableWithoutFeedback
                onPress={ () => handleCreateThread() }
            >
                <View
                    style={ styles.buttonStyle }
                >
                    <Text content={ 'Create thread' } size={ 25 } fontFamily={ 'Poppins medium' } color={ Colors.light_teal }/>
                </View>
            </TouchableWithoutFeedback>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    scrollView: {
        flexGrow: 1,
        marginTop: heightPx(20),
    },
    container: {
        flexGrow: 1,
        justifyContent: 'space-between',
        paddingBottom: heightPx(40),
    },
    header: {
        paddingHorizontal: widthPercent(5),
    },
    inputContainerStyle: {
        borderBottomWidth: .5,
        borderColor: Colors.white,
        borderStyle: 'solid',
        borderTopWidth: .5,
        marginBottom: 0,
        marginTop: heightPx(35),
        paddingHorizontal: widthPercent(5),
        paddingVertical: heightPx(10),
        width: '100%',
    },
    inputStyle: {
        color: Colors.white,
        fontFamily: 'Poppins medium',
        fontSize: fontScale(20),
        lineHeight: fontScale(28),
        height: heightPx(45),
        overflow: 'hidden'
    },
    textAreaContainer: {
        justifyContent: 'flex-start',
        marginBottom: 0,
        padding: widthPercent(5),
        width: '100%',
    },
    textArea: {
        color: Colors.white,
        fontSize: fontScale(18),
        height: 17 * fontScale(22),
        lineHeight: fontScale(22),
        marginBottom: heightPx(10),
        overflow: 'scroll',
        textAlignVertical: 'top',
    },
    buttonStyle: {
        alignItems: 'center',
        alignSelf: 'center',
        borderRadius: 1,
        height: heightPx(60),
        justifyContent: 'center',
        marginHorizontal: 'auto',
        width: widthPercent(60),
        borderStyle: 'solid',
        borderColor: Colors.light_teal,
        borderWidth: 1,
    }
})
