import * as React from 'react';
import {BackButton} from '../../../buttons/BackButton';
import {Colors} from '../../../../theme/Colors';
import {ProfileChatButtonGroup} from '../../../buttons/ProfileChatButtonGroup';
import {StyleSheet, View} from 'react-native';
import {widthPercent, widthPx} from '../../../../helpers/Dimensions';

export const ForumNewThreadHeader = () => {

    return (
        <View style={ styles.container }>
            <BackButton
                iconColor={ Colors.white }
                style={ styles.backButton }
                iconSize={ 50 }
            />
            <ProfileChatButtonGroup
                containerStyle={ styles.buttonsGroup }
                profileIconColor={ Colors.white }
                chatIconColor={ Colors.white }
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flexDirection: 'row',
        position: 'relative',
    },
    buttonsGroup: {
        marginLeft: 'auto'
    },
    backButton: {
        left: widthPercent(5) - widthPx(10),
        position: 'relative',
        top: 0,
    }
})
