import * as React from 'react';
import {StyleSheet, TouchableHighlight, View} from 'react-native';
import {ThreadHeader} from '../ThreadHeader';
import {heightPx, widthPx} from '../../../../helpers/Dimensions';
import {AntDesign, Feather} from '@expo/vector-icons';
import {Colors} from '../../../../theme/Colors';
import {Text} from '../../../text/Text';
import {ReplyType} from '../../../../store/slices/thread/Thread.slice';
import {useDispatch, useSelector} from 'react-redux';
import {deleteReply, likeReply} from '../../../../store/slices/thread/Thread.thunk';
import {RootState} from '../../../../store/store';

interface Props {
    reply: ReplyType,
    threadId: string,
}

export const CommentReply = (props: Props) => {
    const {threadId, reply} = props;
    const {userId} = useSelector((state: RootState) => state.user);
    const dispatch = useDispatch();
    const isLike = reply.likes.includes(String(userId));

    return (
        <TouchableHighlight
            onLongPress={ () => dispatch(deleteReply({threadId, replyId: reply.replyId})) }
        >
            <View style={ styles.container }>
                <Feather name="corner-down-right" size={ 24 } color={ Colors.white } style={ styles.replyArrow }/>
                <ThreadHeader date={ reply.createdAt } img={ reply.photo } name={ reply.name } role={ reply.role }/>
                <Text content={ reply.content }/>
                <View style={ styles.footer }>
                    <Text content={ `${ reply.likes ? reply.likes.length : 0 } likes` } style={ styles.likes } size={ 12 }/>
                    <AntDesign name="heart" size={ 20 } color={ isLike ? Colors.red : Colors.white } onPress={ () => dispatch(likeReply({userId: String(userId), threadId: threadId, replyId: reply.replyId, set: !isLike})) }/>
                </View>
            </View>
        </TouchableHighlight>
    );
}

const styles = StyleSheet.create({
    container: {
        position: 'relative',
        borderBottomWidth: 1,
        borderColor: 'rgba(255, 255, 255, .3)',
        paddingLeft: widthPx(50),
        paddingVertical: heightPx(10),
    },
    replyArrow: {
        position: 'absolute',
        top: heightPx(25),
        left: widthPx(20),
    },
    footer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    },
    likes: {
        marginLeft: 'auto',
        marginRight: widthPx(10)
    }
});
