import * as React from 'react';
import {Text} from '../../../text/Text';
import {StyleSheet, View} from 'react-native';
import {heightPx, widthPercent, widthPx} from '../../../../helpers/Dimensions';
import {Comment} from './Comment';
import {Colors} from '../../../../theme/Colors';
import LottieView from 'lottie-react-native';
import {useSelector} from 'react-redux';
import {RootState} from '../../../../store/store';

export const FullThreadComments = () => {
    const {comments} = useSelector((state: RootState) => state.thread);
    return (
        <View style={ styles.comments }>
            <Text content={ 'Comments' } size={ 20 } fontFamily={ 'Poppins semiBold' } style={ styles.commentsHeader }/>
            { (comments && comments.length !== 0) ?
                comments.map(({id, data}) => <Comment key={ id } comment={ data } commentId={ id } replies={ data.replies }/>)
                :
                <View style={ styles.noCommentsContainer }>
                    <Text
                        content={ 'Be the first one to write a comment' }
                        size={ 26 }
                        fontFamily={ 'Poppins extraLight' }
                        color={ Colors.white }
                        style={ styles.noComments }
                    />
                    <LottieView
                        style={ styles.animation }
                        source={ require('./../comment-animation/commentAnimation.json') }
                        loop
                        autoPlay
                    />
                </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    comments: {
        paddingVertical: heightPx(25),
    },
    commentsHeader: {
        borderBottomWidth: 1,
        paddingBottom: heightPx(10),
        paddingHorizontal: widthPx(15),
        borderColor: 'rgba(255, 255, 255, .2)',
    },
    noCommentsContainer: {
        flexGrow: 1,
        alignItems: 'center',
        paddingHorizontal: widthPercent(15)
    },
    noComments: {
        marginTop: heightPx(35),
        marginBottom: heightPx(150),
        textAlign: 'center',
    },
    animation: {
        marginTop: 45
    }
})
