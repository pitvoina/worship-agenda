import * as React from 'react';
import {Octicons} from '@expo/vector-icons';
import {Colors} from '../../../../theme/Colors';
import {StyleSheet, TouchableWithoutFeedback, View} from 'react-native';
import {Text} from '../../../text/Text';
import {heightPx, widthPercent} from '../../../../helpers/Dimensions';
import {useState} from 'react';

interface Props {
    likes: string,
    comments: string,
    views: string,
    likesClick: () => void,
    viewsClick: () => void
}

export const FullThreadStats = (props: Props) => {
    const {likes, comments, views, likesClick, viewsClick} = props;

    return (
        <View style={ styles.container }>
            <TouchableWithoutFeedback onPress={() => likesClick() }>
                <View style={ styles.stats }>
                    <Text size={ 12 } content={ likes } color={ Colors.black }/>
                    <Text size={ 12 } content={ 'Likes' } color={ Colors.black }/>
                </View>
            </TouchableWithoutFeedback>
            <View style={ [styles.stats, styles.border] }>
                <Text size={ 12 } content={ comments } color={ Colors.black }/>
                <Text size={ 12 } content={ 'Comments' } color={ Colors.black }/>
            </View>
            <TouchableWithoutFeedback onPress={() => viewsClick() }>
                <View style={ styles.stats }>
                    <Octicons name="eye" size={ 20 } color="black"/>
                    <Text size={ 12 } content={ `${ views } Views` } color={ Colors.black }/>
                </View>
            </TouchableWithoutFeedback>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        backgroundColor: Colors.white,
        borderRadius: 40,
        flexDirection: 'row',
        paddingVertical: heightPx(10),
        width: widthPercent(100) - 30,
    },
    stats: {
        alignItems: 'center',
        justifyContent: 'center',
        width: widthPercent(30)
    },
    border: {
        borderLeftWidth: 1,
        borderRightWidth: 1,
    },
});
