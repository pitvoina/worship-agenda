import * as React from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {Text} from '../../../text/Text';
import {IconButton} from '../../../buttons/IconButton';
import {Colors} from '../../../../theme/Colors';
import {heightPercent, heightPx, widthPercent, widthPx} from '../../../../helpers/Dimensions';
import {Ionicons} from '@expo/vector-icons';
import Constants from 'expo-constants';
import {StatsType} from '../../../../store/slices/thread/Thread.slice';

type Props = {
    stats: StatsType,
    close: () => void,
}

export const Stats = (props: Props) => {
    const {stats, close} = props;

    return (
        <View style={ styles.container }>
            <Ionicons
                name="close-sharp"
                size={ 40 }
                color={ Colors.white }
                onPress={ close }
                style={ styles.closeButton }
            />
            <Text
                content={ `${ stats.count } ${ stats.type }` }
                size={ 25 }
                fontFamily={ 'Poppins medium' }
                style={ styles.header }
            />
            { stats.users.map(
                (user: any, _i: number) =>
                    <View style={ styles.userContainer } key={ user.userId }>
                        { user.photo ?
                            <Image source={ {uri: user.photo} } style={styles.userImg}/>
                            :
                            <IconButton
                                iconName={ 'user-circle' }
                                style={ styles.iconButton }
                                iconColor={ Colors.white }
                                size={ 60 }
                            /> }
                        <View>
                            <Text content={ user.name } size={ 24 } color={ Colors.white } fontFamily={ 'Poppins semiBold' }/>
                            <Text content={ user.role } size={ 14 } color={ Colors.white }/>
                        </View>
                    </View>
            ) }
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.od_dark,
        height: heightPercent(100),
        paddingHorizontal: widthPx(15),
        paddingVertical: widthPx(15),
        position: 'absolute',
        top: Constants.statusBarHeight,
        width: widthPercent(100),
        zIndex: 5,
    },
    iconButton: {
        marginRight: widthPx(15),
        padding: 0,
    },
    userImg: {
        borderColor: Colors.gray,
        borderRadius: 60,
        borderWidth: 1,
        height: widthPx(60),
        marginRight: widthPx(15),
        width: widthPx(60),
    },
    closeButton: {
        marginLeft: 'auto',
    },
    header: {
        marginBottom: heightPx(15),
    },
    userContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        marginVertical: heightPx(15),
    }
})
