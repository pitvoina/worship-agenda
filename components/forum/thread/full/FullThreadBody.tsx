import * as React from 'react';
import {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {Text} from '../../../text/Text';
import {heightPx} from '../../../../helpers/Dimensions';

interface Props {
    title: string,
    text: string,
}

export const FullThreadBody = (props: Props) => {
    const {title, text} = props;
    const [showText, setShowText] = useState(text.length <= 140);

    return (
        <View style={ styles.container }>
            <Text
                content={ title }
                fontFamily={ 'Poppins semiBold' }
                size={ 25 }
                style={ styles.spacing }
            />
            <Text
                content={
                    showText ?
                        text :
                        `${ text.substring(0, 140) }...`}
                size={ 18 }
            />
            {text.length > 140 && <Text style={styles.hideText} size={ 18 } onPress={ () => setShowText(!showText) } fontFamily={ 'Poppins bold' } content={ showText ? 'Read less' : 'Read more' }/>}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        marginVertical: heightPx(10)
    },
    hideText: {
        marginLeft: 'auto'
    },
    spacing: {
        marginBottom: heightPx(10),
    }
})
