import * as React from 'react';
import {StyleSheet, View, Image} from 'react-native';
import {IconButton} from '../../../buttons/IconButton';
import {Colors} from '../../../../theme/Colors';
import {heightPx, widthPx} from '../../../../helpers/Dimensions';
import {Text} from '../../../text/Text';

interface Props {
    authorId: string,
    date: string,
    img: string | null,
    name: string,
    role: string,
}

export const FullThreadHeader = (props: Props) => {
    const {date, img, name, role} = props;
    return (
        <View style={ styles.container }>
            <Text style={ styles.date } content={ date } size={ 16 }/>
            { img ?
                <Image source={ {uri: img} } style={styles.profileImage}/>
                :
                <IconButton iconName={ 'user-circle' } style={ styles.iconButton } iconColor={ Colors.white } size={ 80 }/>
            }
            <View>
                <Text content={ name } fontFamily={ 'Poppins bold' } size={ 25 }/>
                <Text content={ role } fontFamily={ 'Poppins regular' } size={ 16 }/>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flexDirection: 'row',
        marginBottom: heightPx(15),
        paddingTop: 0,
        position: 'relative',
    },
    iconButton: {
        marginRight: widthPx(15),
        padding: 0,
    },
    profileImage: {
        borderColor: Colors.gray,
        borderRadius: 80,
        borderWidth: 1,
        marginRight: widthPx(15),
        height: widthPx(80),
        width: widthPx(80),
    },
    date: {
        position: 'absolute',
        right: 0,
        top: 0,
    }
})
