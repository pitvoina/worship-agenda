import * as React from 'react';
import {StyleSheet, TouchableWithoutFeedback, View} from 'react-native';
import {ThreadHeader} from '../ThreadHeader';
import {heightPx, widthPx} from '../../../../helpers/Dimensions';
import {Text} from '../../../text/Text';
import {AntDesign} from '@expo/vector-icons';
import {Colors} from '../../../../theme/Colors';
import {Entypo} from '@expo/vector-icons';
import {CommentReply} from './CommentReply';
import {CommentType, ReplyType, ThreadSlice} from '../../../../store/slices/thread/Thread.slice';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../../../../store/store';
import {deleteComment, deleteReplyWithComment, likeComment} from '../../../../store/slices/thread/Thread.thunk';

interface Props {
    comment: CommentType,
    commentId: string,
    replies: ReplyType[],
}

export const Comment = (props: Props) => {
    const {comment, commentId, replies} = props;
    const dispatch = useDispatch();
    const {setWriteParentId, setShowWrite, setWriteType} = ThreadSlice;
    const {showWrite, writeParentId, threadId} = useSelector((state: RootState) => state.thread);
    const {userId} = useSelector((state: RootState) => state.user);

    const setLikes = () => {
        if (userId) {
            dispatch(likeComment({
                threadId: threadId.split('_')[1],
                commentId: commentId,
                userId: userId ? userId : '',
                set: !comment.likes.includes(userId)
            }));
        }
    }


    const writeReply = () => {
        switch (true) {
            case (showWrite && writeParentId !== comment.commentId):
                dispatch(setWriteParentId(comment.commentId));
                dispatch(setWriteType('reply'));
                break;
            case !showWrite:
                dispatch(setWriteParentId(comment.commentId));
                dispatch(setShowWrite(true));
                dispatch(setWriteType('reply'));
                break;
            default:
                dispatch(setShowWrite(false));
                dispatch(setWriteType(null));
                break;
        }
    }

    return (
        <TouchableWithoutFeedback
            onLongPress={ () => {
                if (userId === comment.userId) {
                    dispatch(deleteComment({commentId, threadId: comment.threadId}));
                    dispatch(deleteReplyWithComment({commentId, threadId: comment.threadId}));
                }
            } }
        >
            <View style={ styles.container }>
                <ThreadHeader date={ comment.createdAt } img={ comment.photo } name={ comment.name } role={ comment.role }/>
                <Text content={ comment.content }/>
                <View style={ styles.footer }>
                    <Text content={ `${ comment.likes ? comment.likes.length : 0 } likes` } size={ 12 }/>
                    <Text content={ `|` } style={ styles.spacer } size={ 12 }/>
                    <Text content={ `${ comment.replies ? comment.replies.length : 0 } replies` } size={ 12 }/>
                    <AntDesign style={ styles.heart }
                               name="heart"
                               size={ 24 }
                               color={ comment.likes.includes(String(userId)) ? Colors.red : Colors.white }
                               onPress={ () => setLikes() }
                    />
                    <Entypo name="reply" size={ 24 } color={ Colors.white } onPress={ () => writeReply() }/>
                </View>
                { replies && replies.map((reply: ReplyType) => <CommentReply threadId={ comment.threadId } reply={ reply } key={ reply.replyId }/>) }
            </View>
        </TouchableWithoutFeedback>
    );
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: widthPx(15),
        paddingTop: heightPx(15),
    },
    footer: {
        alignItems: 'flex-end',
        borderBottomWidth: 1,
        borderColor: 'rgba(255, 255, 255, .3)',
        flexDirection: 'row',
        paddingBottom: heightPx(15),
        paddingTop: widthPx(15),
    },
    spacer: {
        marginHorizontal: widthPx(5),
    },
    heart: {
        marginLeft: 'auto',
        marginRight: widthPx(15),
    }
});
