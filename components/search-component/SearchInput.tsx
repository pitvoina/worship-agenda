import * as React from 'react';
import {StyleSheet, View} from 'react-native';
import {AppInput} from '../form/AppInput';
import {useEffect, useState} from 'react';
import {fontScale, heightPx, widthPercent, widthPx} from '../../helpers/Dimensions';
import {Colors} from '../../theme/Colors';

interface Props {
    searchFn: (val: string) => void
}

export const SearchInput = (props: Props) => {
    const {searchFn} = props;
    const [value, setValue] = useState('');

    useEffect(() => {
        searchFn(value);
    }, [value]);


    return (
        <View style={ styles.container }>
            <AppInput
                inputPlaceholder={ 'Search...' }
                inputStyle={ styles.inputStyle }
                autoCorrect={ false }
                value={ value }
                setValueFn={ setValue }
                inputContainerStyle={ styles.inputContainer }
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginHorizontal: widthPercent(5),
    },
    inputStyle: {
        fontFamily: 'Poppins medium',
        fontSize: fontScale(20),
        height: heightPx(50),
        lineHeight: fontScale(24),
    },
    inputContainer: {
        borderColor: Colors.o3_dark,
        borderRadius: 2,
        borderStyle: 'solid',
        borderWidth: 1,
        marginBottom: heightPx(20),
        paddingHorizontal: widthPx(10),
        width: widthPercent(100) - 40,
    }
})
