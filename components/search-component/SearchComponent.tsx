import * as React from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import {SearchInput} from './SearchInput';
import {Text} from '../text/Text';
import {Colors} from '../../theme/Colors';
import {widthPercent} from '../../helpers/Dimensions';

interface Props {
    searchResults?: JSX.Element[],
    searchFn: (value: string) => void,
    onScrollStart?: () => void,
    onScrollEnd?: () => void,
}

export const SearchComponent = (props: Props) => {
    const {searchResults, searchFn, onScrollEnd, onScrollStart} = props;

    return (
        <>
            <SearchInput
                searchFn={ searchFn }
            />
            <ScrollView
                onScrollBeginDrag={ onScrollStart }
                onScrollEndDrag={ onScrollEnd }
            >
                <View style={ styles.defaultPadding }>
                    {
                        (searchResults && searchResults.length > 0) ?
                            searchResults
                            :
                            <Text content={ 'Nothing found!' } color={ Colors.red } size={ 20 }/>
                    }
                </View>
            </ScrollView>
        </>
    )
}

const styles = StyleSheet.create({
    defaultPadding: {
        marginHorizontal: widthPercent(5),
    },
})
