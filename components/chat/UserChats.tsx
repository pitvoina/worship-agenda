import * as React from 'react';
import {StyleSheet, View} from 'react-native';
import {useEffect, useState} from 'react';
import {UserChat} from './UserChat';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../../store/store';
import {collection, onSnapshot, orderBy, query, where} from 'firebase/firestore';
import {db} from '../../config';

interface Props {
    openChat: (show: boolean) => void,
}

export const UserChats = (props: Props) => {
    const dispatch = useDispatch();
    const {openChat} = props;
    const {userId} = useSelector((state: RootState) => state.user);
    const [chats, setChats] = useState([]);

    useEffect(() => {
        const q = query(collection(db, 'chats'), where('users', 'array-contains', userId), orderBy('date', 'desc'));
        const data: any = [];
        const unSubs = onSnapshot(q, snp => {
            snp.docChanges().forEach(change => {
                if (['added', 'modified', 'removed'].includes(change.type)) {
                    let docData = change.doc.data();
                    data.push({...docData, chatId: change.doc.id });
                }
            });
            setChats(data);
        });
    }, []);

    return (
        <>
            { chats.map((chat, index: number) => <UserChat chat={ chat } key={ index } openChat={ () => openChat(true) }/>) }
        </>
    );
}

const styles = StyleSheet.create({});
