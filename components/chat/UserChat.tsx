import * as React from 'react';
import {StyleSheet, TouchableWithoutFeedback, View, Image} from 'react-native';
import {FontAwesome} from '@expo/vector-icons';
import {heightPx, widthPx} from '../../helpers/Dimensions';
import {Text} from '../text/Text';
import {Colors} from '../../theme/Colors';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../../store/store';
import {UserType} from '../../store/slices/user/User.slice';
import {ChatSlice, MessageType} from '../../store/slices/chat/Chat.slice';
import {useEffect, useState} from 'react';
import {collection, doc, onSnapshot, orderBy, query, where} from 'firebase/firestore';
import {db} from '../../config';

interface Props {
    openChat: (open: boolean) => void,
    chat: any,
}

export const UserChat = (props: Props) => {
    const dispatch = useDispatch();
    const {openChat, chat} = props;
    const {setChatData} = ChatSlice;
    const {userList, userId} = useSelector((state: RootState) => state.user);
    const [messages, setMessages] = useState<MessageType[]>([]);
    const toUserId = chat.users.filter((id: string) => String(userId) !== String(id));

    useEffect(() => {
        const q = doc(db, `messages/${ chat.messagesId }`);
        const data: any = [];
        const unSubs = onSnapshot(q, doc => {
            let docData = doc.data()?.messages ? doc.data()?.messages : [];
            setMessages(docData);
        });

        return () => unSubs();
    }, [chat]);

    const messagesCount = messages.length;
    const hasMessages = messagesCount > 0;
    const lastMessage = messages[messagesCount - 1];

    const toUser = userList.filter((user: UserType) => user.userId === toUserId[0])[0];
    let formattedData: string = 'Nothing yet!';

    if (hasMessages) {
        let currDate = new Date().getTime();
        let messageDate = new Date(parseInt(lastMessage.date));
        switch (true) {
            case currDate - parseInt(lastMessage.date) > 600000:
                formattedData = `${ ('0' + messageDate.getDate()).slice(-2) }.${ ('0' + (messageDate.getMonth() + 1)).slice(-2) }`;
                break;
            case currDate - parseInt(lastMessage.date) < 60000:
                formattedData = 'Just now';
                break;
            case currDate - parseInt(lastMessage.date) < 600000:
                formattedData = String(Math.floor((currDate - parseInt(lastMessage.date)) / 60 / 1000)) + ' mins ago';
                break;
            default:
        }
    }

    const onChatPress = () => {
        dispatch(setChatData({toUser: toUser, messagesId: chat.messagesId, chatId: chat.chatId}));
        openChat(true);
    }

    return (
        <TouchableWithoutFeedback onPress={ () => onChatPress() }>
            <View style={ styles.container }>
                <View style={ styles.userData }>
                    { toUser.photo ?
                        <Image source={ {uri: toUser.photo} } style={ styles.image }/>
                        :
                        <FontAwesome name={ 'user-circle' } size={ 60 } color={ 'black' }/>
                    }
                    <View style={ styles.userNMessage }>
                        <Text content={ toUser.name } size={ 25 } fontFamily={ 'Poppins semiBold' } color={ Colors.ob_dark }/>
                        { hasMessages &&
                        <Text content={ lastMessage.text } size={ 18 } fontFamily={ 'Poppins medium' } color={ Colors.ob_dark }/>
                        }
                    </View>
                </View>
                <View style={ styles.info }>
                    <Text content={ formattedData } color={ Colors.ob_dark }/>
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: 'rgba(0, 0, 0, .5)',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: widthPx(25),
        paddingVertical: heightPx(20),
    },
    userData: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    image: {
        borderColor: Colors.gray,
        borderRadius: 60,
        borderWidth: 1,
        height: widthPx(60),
        width: widthPx(60),
    },
    userNMessage: {
        marginLeft: widthPx(15),
    },
    info: {
        alignItems: 'center',
    },
    newMessages: {
        alignItems: 'center',
        backgroundColor: Colors.red,
        borderRadius: 30,
        display: 'flex',
        height: 35,
        justifyContent: 'center',
        marginTop: heightPx(5),
        width: 35,
    }
});
