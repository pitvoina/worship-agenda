import * as React from 'react';
import {StyleSheet, View} from 'react-native';
import {Text} from '../text/Text';
import {Colors} from '../../theme/Colors';
import {heightPx, widthPercent, widthPx} from '../../helpers/Dimensions';

export interface MessageItem {
    message: string,
    position: 'left' | 'right',
    time: string,
    name: string,
}

export const MessageItem = (props: MessageItem) => {
    const {message, position, time, name} = props;
    const date = new Date(time);
    const formattedDate = `${ ('0' + date.getDate()).slice(-2) }.${ ('0' + (date.getMonth() + 1)).slice(-2) } at ${date.getHours()}:${date.getMinutes()}`;

    return (
        <View style={ [
            styles.container,
            (position === 'left' ?
                styles.msgLeft
                :
                styles.msgRight)
        ] }>
            <Text content={ message } color={ Colors.black }/>
            <Text
                content={ name }
                color={ Colors.white }
                style={ [
                    styles.name,
                    (position === 'right' ?
                        {right: 0}
                        :
                        {left: 0})
                ] }

            />
            <Text
                content={ formattedDate }
                size={ 12 }
                style={ [
                    styles.time,
                    (position === 'left' ?
                            styles.timeLeft
                            :
                            styles.timeRight
                    )
                ] }
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        borderRadius: 20,
        marginBottom: heightPx(40),
        width: widthPercent(65),
        paddingHorizontal: widthPx(15),
        paddingVertical: heightPx(10),
        position: 'relative',
    },
    msgLeft: {
        backgroundColor: Colors.white,
        borderBottomLeftRadius: 0,
        marginRight: 'auto',
    },
    msgRight: {
        backgroundColor: Colors.light_teal,
        borderBottomRightRadius: 0,
        marginLeft: 'auto',
    },
    name: {
        bottom: heightPx(-25),
        position: 'absolute',
    },
    time: {
        bottom: heightPx(-25),
        position: 'absolute',
    },
    timeLeft: {
        right: widthPx(15)
    },
    timeRight: {
        left: widthPx(15),
    }
});
