import * as React from 'react';
import {KeyboardAvoidingView, ScrollView, StyleSheet, View} from 'react-native';
import {AppInput} from '../form/AppInput';
import {useEffect, useState} from 'react';
import {MessageItem} from './MessageItem';
import {FontAwesome} from '@expo/vector-icons';
import {Colors} from '../../theme/Colors';
import {heightPercent, heightPx, widthPercent, widthPx} from '../../helpers/Dimensions';
import {Text} from '../text/Text';
import {StatusBar} from 'expo-status-bar';
import Constants from 'expo-constants';
import {useDispatch, useSelector} from 'react-redux';
import {MessageType} from '../../store/slices/chat/Chat.slice';
import {RootState} from '../../store/store';
import {postMessage} from '../../store/slices/chat/Chat.thunk';
import {doc, onSnapshot} from 'firebase/firestore';
import {db} from '../../config';

interface Props {
    close: () => void
}

export const Chat = (props: Props) => {
    const dispatch = useDispatch();
    const {close} = props;
    const [message, setMessage] = useState('');
    const {userId, name} = useSelector((state: RootState) => state.user);
    const {messages} = useSelector((state: RootState) => state.chat);
    const [msgs, setMsgs] = useState([]);

    useEffect(() => {
        const q = doc(db, `messages/${ messages.messagesId }`);
        const data: any = [];
        const unSubs = onSnapshot(q, doc => {
            let docData = doc.data();
            setMsgs(docData?.messages);
        });
        return () => unSubs();
    }, []);

    const toUser = messages.toUser;

    const sendMessage = () => {
        if (message.length === 0) return;
        dispatch(postMessage({
            messagesId: messages.messagesId,
            data: {
                userId: userId,
                date: new Date().getTime(),
                name: name,
                text: message,
            }
        }));

        setMessage('');
    }

    return (
        <KeyboardAvoidingView style={ styles.container }>
            <StatusBar style={ 'dark' } backgroundColor={ Colors.white }/>
            <View style={ styles.header }>
                <FontAwesome name={ 'chevron-left' } size={ 40 } onPress={ () => close() } color={ Colors.ob_dark }/>
                <View style={ styles.headerUserInfo }>
                    <Text content={ String(toUser?.name) } size={ 30 } fontFamily={ 'Poppins bold' } color={ Colors.od_dark }/>
                    <Text content={ String(toUser?.role) } size={ 16 } fontFamily={ 'Poppins regular' } color={ Colors.od_dark }/>
                </View>
            </View>
            <ScrollView contentContainerStyle={ styles.scrollView }>
                <View>
                    { msgs && msgs.map(
                        (msg: MessageType, index: number) =>
                            <MessageItem
                                name={ userId === msg.userId ? 'Me' : msg.name }
                                key={ index }
                                message={ msg.text }
                                time={ msg.date }
                                position={ userId === msg.userId ? 'right' : 'left' }
                            />
                    ) }
                </View>
            </ScrollView>
            <View style={ styles.writeMessageContainer }>
                <AppInput
                    autoCorrect={ false }
                    inputContainerStyle={ styles.inputContainer }
                    inputPlaceholder={ 'Write message ...' }
                    multiline={ true }
                    setValueFn={ setMessage }
                    value={ message }
                />
                <FontAwesome name={ 'send' } size={ 24 } color={ Colors.light_teal } onPress={ () => sendMessage() }/>
            </View>
        </KeyboardAvoidingView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.white,
        height: heightPercent(100),
        width: widthPercent(100),
        elevation: 5,
        zIndex: 5,
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: widthPx(25),
        paddingVertical: heightPx(25),
    },
    headerUserInfo: {
        marginLeft: widthPx(20),
    },
    scrollView: {
        // flex: 1,
        flexGrow: 1,
        backgroundColor: Colors.black,
        borderTopLeftRadius: 40,
        borderTopRightRadius: 40,
        paddingHorizontal: widthPx(25),
        paddingTop: heightPx(35),
    },
    inputContainer: {
        backgroundColor: Colors.white,
        bottom: heightPx(-20),
        paddingHorizontal: widthPx(20),
        paddingVertical: widthPx(5),
        width: widthPercent(100) - 90,
        borderRadius: 50
    },
    writeMessageContainer: {
        bottom: 0,
        backgroundColor: Colors.black,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: widthPx(25),
        alignItems: 'center',
    }
});
