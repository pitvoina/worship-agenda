import * as React from 'react';
import {StyleSheet, View} from 'react-native';
import {AppInput} from '../form/AppInput';
import {useState} from 'react';
import {AntDesign} from '@expo/vector-icons';
import {Colors} from '../../theme/Colors';
import {heightPx, widthPx} from '../../helpers/Dimensions';

interface Props {
    setShowUsers: (show: boolean) => void
}

export const SearchComponent = (props: Props) => {
    const {setShowUsers} = props;
    const [search, setSearch] = useState('');

    return (
        <View style={ styles.container }>
            <AppInput
                inputPlaceholder={ 'Search ...' }
                autoCorrect={ false }
                value={ search }
                setValueFn={ setSearch }
                inputContainerStyle={ styles.inputContainer }
                inputStyle={ styles.input }
            />
            <AntDesign
                onPress={ () => setShowUsers(true) }
                name={ 'pluscircle' }
                size={ 40 }
                color={ Colors.light_teal }
                style={ styles.addButton }
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: widthPx(25),
    },
    input: {
        height: heightPx(45),
    },
    inputContainer: {
        paddingHorizontal: widthPx(15),
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'rgba(0, 0, 0, .3)'
    },
    addButton: {
        marginBottom: heightPx(35),
        marginLeft: 'auto'
    }
});
