import * as React from 'react';
import {Image, ScrollView, StyleSheet, TouchableWithoutFeedback, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../../store/store';
import {FontAwesome5} from '@expo/vector-icons';
import {Colors} from '../../theme/Colors';
import {fontScale, heightPercent, heightPx, widthPercent, widthPx} from '../../helpers/Dimensions';
import {Text} from '../text/Text';
import {startNewChat} from '../../store/slices/chat/Chat.thunk';

interface Props {
    close: () => void,
}

export const UserList = (props: Props) => {
    const dispatch = useDispatch();
    const {close} = props;
    const {userList, userId} = useSelector((state: RootState) => state.user);

    const startChat = (secondUserId: string) => {
        if (userId === secondUserId) return;
        dispatch(startNewChat({data: [String(userId), secondUserId]}));
        close();
    }

    return (
        <View style={ styles.listContainer }>
            <ScrollView contentContainerStyle={ styles.usersList }>
                { userList &&
                userList.map(user =>
                    <TouchableWithoutFeedback
                        onPress={ () => startChat(user.userId) }
                        key={ user.userId }
                    >
                        <View key={ user.userId } style={ styles.userContainer }>
                            { user.photo ?
                                <Image source={ {uri: user.photo} } style={ styles.image }/>
                                :
                                <FontAwesome5
                                    solid
                                    color={ Colors.white }
                                    name={ 'user-circle' }
                                    size={ fontScale(52) }
                                    style={ styles.image }
                                />
                            }
                            <View>
                                <Text content={ user.name } size={ 22 } fontFamily={ 'Poppins semiBold' }/>
                                <Text content={ user.role } size={ 16 } fontFamily={ 'Poppins light' }/>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>)
                }

                <TouchableWithoutFeedback onPress={ () => close() }>
                    <View style={ styles.button }>
                        <Text content={ 'Close' } color={ Colors.white } size={ 24 }/>
                    </View>
                </TouchableWithoutFeedback>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    image: {
        borderRadius: 50,
        height: widthPx(50),
        marginHorizontal: widthPx(15),
        width: widthPx(50),
    },
    userContainer: {
        backgroundColor: Colors.ob_dark,
        alignItems: 'center',
        flexDirection: 'row',
        marginBottom: heightPx(10),
        minHeight: heightPx(75),
        paddingVertical: heightPx(15),
        width: widthPercent(100) - 100,
    },
    listContainer: {
        backgroundColor: Colors.backdrop,
        bottom: 0,
        elevation: 1,
        height: heightPercent(100),
        padding: heightPx(25),
        position: 'absolute',
        width: widthPercent(100),
        zIndex: 1,
    },
    usersList: {
        backgroundColor: Colors.od_dark,
        borderRadius: 5,
        height: heightPercent(100) - 50,
        padding: heightPx(20),
        paddingBottom: heightPx(80),
        width: widthPercent(100) - 50,
        alignItems: 'center',
    },
    button: {
        backgroundColor: Colors.dark_teal,
        paddingVertical: heightPx(15),
        paddingHorizontal: widthPx(40),
        bottom: heightPx(20),
        position: 'absolute',
    }
});
