import * as React from 'react';
import {Platform, StyleSheet, View} from 'react-native';
import {Entypo, FontAwesome5} from '@expo/vector-icons';
import {Colors} from '../../theme/Colors';
import {heightPx, widthPx} from '../../helpers/Dimensions';
import {useNavigation, useNavigationState} from '@react-navigation/native';

export const Menu = () => {
    const navState = useNavigationState(state => state.routes[state.index].name);
    const navigation = useNavigation();

    return (
        <View style={ styles.container }>
            <FontAwesome5
                color={ navState === 'Home' ? Colors.light_teal : Colors.white }
                name={ 'home' }
                onPress={ () => navigation.navigate('Home') }
                size={ 20 }
                solid
                style={ styles.iconStyle }
            />
            <FontAwesome5
                color={ navState === 'Calendar' ? Colors.light_teal : Colors.white }
                name={ 'calendar-plus' }
                onPress={ () => navigation.navigate('Calendar') }
                size={ 35 }
                solid
                style={ styles.iconStyle }
            />
            <Entypo
                color={ navState === 'Forum' ? Colors.light_teal : Colors.white }
                name={ 'chat' }
                onPress={ () => navigation.navigate('Forum') }
                size={ 20 }
                solid
                style={ styles.iconStyle }
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'baseline',
        backgroundColor: Colors.o3_dark,
        borderBottomWidth: 0,
        borderColor: 'rgba(255, 255, 255, .2)',
        borderStyle: 'solid',
        borderTopLeftRadius: 50,
        borderTopRightRadius: 50,
        borderWidth: .5,
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginHorizontal: -5,
        marginTop: 'auto',
        paddingHorizontal: widthPx(25),
    },
    iconStyle: {
        paddingBottom: Platform.OS === 'ios' ? heightPx(40) : heightPx(15),
        paddingHorizontal: widthPx(20),
        paddingTop: heightPx(20),
    }

})
