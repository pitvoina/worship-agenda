import * as React from 'react';
import LottieView from 'lottie-react-native';
import {StyleSheet} from 'react-native';
import {Colors} from '../../theme/Colors';
import {heightPercent, widthPercent} from '../../helpers/Dimensions';
import Animated, {FadeIn, FadeOut} from 'react-native-reanimated';

interface Props {
    isLoading: boolean,
}

export const Loader = ({isLoading}: Props) => {
    return (
        isLoading ? <Animated.View
            style={ styles.container }
            entering={FadeIn.duration(150)}
            exiting={FadeOut.duration(150)}
        >
            <LottieView
                source={ require('./loader.json') }
                colorFilters={
                    [
                        {
                            keypath: 'Shape Layer 1',
                            color: Colors.light_teal,
                        },
                        {
                            keypath: 'Shape Layer 2',
                            color: Colors.light_teal,
                        },
                        {
                            keypath: 'Shape Layer 3',
                            color: Colors.light_teal,
                        },
                        {
                            keypath: 'Shape Layer 4',
                            color: Colors.light_teal,
                        },
                        {
                            keypath: 'Shape Layer 5',
                            color: Colors.light_teal,
                        },
                        {
                            keypath: 'Shape Layer 6',
                            color: Colors.light_teal,
                        },
                        {
                            keypath: 'Shape Layer 7',
                            color: Colors.light_teal,
                        },
                        {
                            keypath: 'Shape Layer 8',
                            color: Colors.light_teal,
                        },
                        {
                            keypath: 'Shape Layer 9',
                            color: Colors.light_teal,
                        },
                        {
                            keypath: 'Shape Layer 10',
                            color: Colors.light_teal,
                        }
                    ]
                }
                autoPlay
                loop
            />
        </Animated.View> : null
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0, 0, 0, .97)',
        position: 'absolute',
        height: heightPercent(100),
        width: widthPercent(100)
    }
});
