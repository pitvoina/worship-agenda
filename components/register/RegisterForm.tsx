import * as React from 'react';
import {KeyboardTypeOptions, ScrollView, StyleSheet, View} from 'react-native';
import {AppInput, Error, InputType} from '../form/AppInput';
import {Colors} from '../../theme/Colors';
import {Dispatch, SetStateAction, useState} from 'react';
import {Button} from 'react-native-paper';
import {fontScale, heightPx, widthPercent, widthPx} from '../../helpers/Dimensions';
import {ErrorType, setUserData} from '../../store/slices/user/User.thunk';
import {useDispatch} from 'react-redux';
import {Validation} from '../../helpers/Validation';
import {createUserWithEmailAndPassword} from 'firebase/auth';
import {auth} from '../../config';

export type TextInputData = {
    label: string,
    placeholder: string,
    secure?: boolean,
    disabled?: boolean,
    value: string,
    setValFn: Dispatch<SetStateAction<string>>,
    keyboardType?: KeyboardTypeOptions | undefined,
    type: InputType,
    validate?: boolean
}

export const RegisterForm = () => {
    const dispatch = useDispatch();
    const [email, setEmail] = useState('');
    const [emailError, setEmailError] = useState<Error>({message: '', hasError: false});
    const [name, setName] = useState('');
    const [nameError, setNameError] = useState<Error>({message: '', hasError: false});
    const [password, setPassword] = useState('');
    const [passwordError, setPasswordError] = useState<Error>({message: '', hasError: false});
    const [submit, setSubmit] = useState(false);

    const validateSubmit = () => {
        setSubmit(true);
        let emailValidation = Validation({value: email, type: 'email', isDirty: true});
        let nameValidation = Validation({value: name, type: 'name', isDirty: true});
        let passwordValidation = Validation({value: password, type: 'registerPassword', isDirty: true});

        setEmailError(emailValidation);
        setNameError(nameValidation);
        setPasswordError(passwordValidation);

        saveData((!!emailValidation?.hasError || !!nameValidation?.hasError || !!passwordValidation?.hasError));
    }

    const saveData = (hasError: boolean) => {
        if (hasError) return;

        createUserWithEmailAndPassword(auth, email, password)
            .then(newUser => dispatch(setUserData({userId: newUser.user.uid, name, phoneNumber: null, role: null})));
    }

    return (
        <ScrollView style={ styles.container }>
            <View style={ styles.formContainer }>
                <AppInput
                    autoCorrect={ false }
                    borderColor={ Colors.black }
                    error={ emailError }
                    inputContainerStyle={ styles.inputContainer }
                    inputDirty={ false }
                    inputLabel={ 'Email' }
                    inputPlaceholder={ 'my@email.com' }
                    inputStyle={ styles.input }
                    keyboardType={ 'email-address' }
                    labelStyle={ styles.labelInput }
                    setInputError={ setEmailError }
                    setValueFn={ setEmail }
                    submit={ submit }
                    type={ 'email' }
                    validate={ true }
                    value={ email }
                />
                <AppInput
                    autoCorrect={ false }
                    borderColor={ Colors.black }
                    error={ nameError }
                    inputContainerStyle={ styles.inputContainer }
                    inputDirty={ false }
                    inputLabel={ 'Name' }
                    inputPlaceholder={ 'ex: Jhonny Bravo' }
                    inputStyle={ styles.input }
                    isPassword={ false }
                    labelStyle={ styles.labelInput }
                    setInputError={ setNameError }
                    setValueFn={ setName }
                    submit={ submit }
                    type={ 'name' }
                    validate={ true }
                    value={ name }
                />
                <AppInput
                    autoCorrect={ false }
                    borderColor={ Colors.black }
                    error={ passwordError }
                    inputContainerStyle={ styles.inputContainer }
                    inputDirty={ false }
                    inputLabel={ 'Password' }
                    inputPlaceholder={ 'password' }
                    inputStyle={ styles.input }
                    isPassword={ true }
                    labelStyle={ styles.labelInput }
                    setInputError={ setPasswordError }
                    setValueFn={ setPassword }
                    submit={ submit }
                    type={ 'registerPassword' }
                    validate={ true }
                    value={ password }
                />

                <Button
                    mode={ 'contained' }
                    onPress={ () => validateSubmit() }
                    uppercase={ false }
                    labelStyle={ {color: Colors.white, fontSize: fontScale(20), fontFamily: 'Poppins regular'} }
                    style={ styles.signupButton }
                >{ 'Sign Up' }</Button>
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        marginTop: heightPx(20),
        width: widthPercent(100),
    },
    labelInput: {
        color: Colors.black,
    },
    formContainer: {
        alignItems: 'center',
        width: widthPercent(100),
    },
    inputContainer: {
        borderWidth: 2,
        paddingHorizontal: widthPx(10),
        paddingTop: heightPx(10),
        paddingBottom: heightPx(10),
    },
    input: {
        color: Colors.black,
    },
    signupButton: {
        backgroundColor: Colors.teal,
        borderRadius: 0,
        width: widthPx(220),
        paddingTop: heightPx(5),
        paddingBottom: heightPx(3),
        marginBottom: heightPx(30),
    },
});
