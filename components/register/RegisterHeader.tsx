import * as React from 'react';
import {SignUpLogo} from '../../svg/SignUpLogo';
import {BackButton} from '../buttons/BackButton';
import {StyleSheet, View} from 'react-native';
import {heightPercent, heightPx} from '../../helpers/Dimensions';

export const RegisterHeader = () => {
    return (
        <View style={ styles.container }>
            <BackButton/>
            <SignUpLogo/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        height: heightPercent(25),
        justifyContent: 'center',
        paddingTop: heightPx(20),
    }
})
