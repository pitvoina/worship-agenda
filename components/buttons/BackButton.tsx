import * as React from 'react';
import {OpaqueColorValue, StyleProp, StyleSheet, TextStyle, TouchableWithoutFeedback, View, ViewStyle} from 'react-native';
import {FontAwesome5} from '@expo/vector-icons';
import {Colors} from '../../theme/Colors';
import {useNavigation} from '@react-navigation/native';
import {fontScale, heightPx, widthPx} from '../../helpers/Dimensions';
import {Text} from '../text/Text';

interface Props {
    iconColor?: string | OpaqueColorValue | undefined,
    onPressFn?: any | null,
    iconSize?: number,
    text?: string,
    style?: StyleProp<ViewStyle>,
    textFontFamily?: string,
    textSize?: number,
    textStyle?: StyleProp<TextStyle>
}

export const BackButton = (props: Props) => {
    const {iconColor, onPressFn, text, textFontFamily, textSize, textStyle, style, iconSize} = props;
    const navigation = useNavigation();

    return (
        <View style={ [styles.backButton, style] }>
            <TouchableWithoutFeedback
                onPress={ () => onPressFn ? onPressFn() : navigation.goBack() }
            >
                <View style={ styles.container }>
                    <FontAwesome5
                        color={ iconColor ? iconColor : Colors.black }
                        name={ 'angle-left' }
                        size={ fontScale((iconSize ? iconSize : 40)) }
                    />
                    { text &&
                    <Text
                        color={ iconColor }
                        content={ text }
                        fontFamily={ textFontFamily }
                        size={ textSize }
                        style={ [styles.text, textStyle] }
                    />
                    }
                </View>
            </TouchableWithoutFeedback>
        </View>
    );
}

const styles = StyleSheet.create({
    backButton: {
        left: widthPx(4),
        padding: 15,
        position: 'absolute',
        top: heightPx(20),
    },
    container: {
        alignItems: 'center',
        flexDirection: 'row',
    },
    text: {
        marginLeft: widthPx(10)
    }
});
