import * as React from 'react';
import {FontAwesome5} from '@expo/vector-icons';
import {Colors} from '../../theme/Colors';
import {fontScale, heightPx} from '../../helpers/Dimensions';
import {OpaqueColorValue, StyleSheet, View, ViewStyle} from 'react-native';
import {IconProps} from '@expo/vector-icons/build/createIconSet';

type Props = {
    iconColor?: string | OpaqueColorValue | undefined,
    style?: ViewStyle,
    iconName: string,
    size?: number,
} & Omit<IconProps<any>, "name">

export const IconButton = (props: Props) => {
    const {iconColor, onPress, style, iconName, size} = props;

    return (
        <View style={ [styles.container, style] }>
            <FontAwesome5
                solid
                color={ iconColor ? iconColor : Colors.black }
                name={ iconName }
                onPress={ onPress }
                size={ fontScale(size ?? 40) }
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: heightPx(15),
    }
})
