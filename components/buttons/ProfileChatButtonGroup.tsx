import * as React from 'react';
import {Image, OpaqueColorValue, StyleSheet, View, ViewStyle} from 'react-native';
import {IconButton} from './IconButton';
import {heightPx, widthPx} from '../../helpers/Dimensions';
import {useNavigation} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import {RootState} from '../../store/store';
import {Colors} from '../../theme/Colors';

interface Props {
    chatIconColor?: string | OpaqueColorValue | undefined,
    chatStyle?: ViewStyle,
    containerStyle?: ViewStyle,
    profileIconColor?: string | OpaqueColorValue | undefined,
    profileStyle?: ViewStyle,

}

export const ProfileChatButtonGroup = (props: Props) => {
    const {containerStyle, profileIconColor, profileStyle, chatIconColor, chatStyle} = props;
    const {photo} = useSelector((state: RootState) => state.user);
    const navigation = useNavigation();

    return (
        <View style={ [styles.container, containerStyle] }>
            { photo ?
                <View style={ styles.imageContainer }>
                    <Image source={ {uri: photo} } style={ styles.image }/>
                </View>
                :
                <IconButton onPress={ () => navigation.navigate('Profile') } size={ 45 } iconName={ 'user-circle' } style={ [styles.gap, profileStyle] } iconColor={ profileIconColor }/>
            }
            <IconButton onPress={ () => navigation.navigate('Chat') } iconName={ 'comment-dots' } style={ chatStyle } iconColor={ chatIconColor }/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    image: {
        borderColor: Colors.gray,
        borderRadius: 50,
        borderWidth: 1,
        height: widthPx(45),
        width: widthPx(45),
    },
    imageContainer: {
        marginRight: widthPx(25),
        paddingVertical: heightPx(14),
    },
    gap: {
        marginRight: widthPx(15),
    }
});
