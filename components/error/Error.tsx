import * as React from 'react';
import {StyleSheet, View} from 'react-native';
import {Colors} from '../../theme/Colors';
import {Text} from '../text/Text';
import {heightPx, widthPercent, widthPx} from '../../helpers/Dimensions';
import {useDispatch} from 'react-redux';
import {useEffect} from 'react';
import Animated, {SlideInRight, SlideOutRight} from 'react-native-reanimated';

interface Props {
    error: string | null,
    clearError: () => any,
}

export const Error = (props: Props) => {
    const dispatch = useDispatch();
    const {clearError, error} = props;

    useEffect(() => {
        if (error) {
            setTimeout(() => {
                dispatch(clearError());
            }, 3000);
        }
    }, [error]);

    return (
        error ?
            <Animated.View
                entering={ SlideInRight.duration(300) }
                exiting={ SlideOutRight.duration(300) }
                style={ styles.container }
            >
                <Text content={ error } size={ 18 }/>
            </Animated.View>
            :
            null
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.red,
        borderBottomLeftRadius: 20,
        borderTopLeftRadius: 20,
        elevation: 50,
        maxWidth: widthPercent(75),
        paddingHorizontal: widthPx(15),
        paddingVertical: heightPx(5),
        position: 'absolute',
        right: 0,
        top: 60,
        zIndex: 50,
    }
});
