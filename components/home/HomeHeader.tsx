import * as React from 'react';
import {Image, StyleSheet, Text, TouchableWithoutFeedback, View} from 'react-native';
import {fontScale, heightPercent, heightPx, widthPx} from '../../helpers/Dimensions';
import {Colors} from '../../theme/Colors';
import {IconButton} from '../buttons/IconButton';
import Constants from 'expo-constants';
import {useNavigation} from '@react-navigation/native';
import {FontAwesome5} from '@expo/vector-icons';
import {useSelector} from 'react-redux';
import {RootState} from '../../store/store';

export const HomeHeader = () => {
    const navigation = useNavigation();
    const {name, role, photo} = useSelector((state: RootState) => state.user);

    return (
        <View style={ styles.container }>
            <IconButton
                onPress={ () => navigation.navigate('Chat') }
                style={ styles.chatButton }
                iconName={ 'comment-dots' }
            />
            <TouchableWithoutFeedback
                onPress={ () => navigation.navigate('Profile') }
            >
                <View
                    style={ styles.userInfo }
                >
                    { photo ?
                        <Image source={ {uri: photo} } style={ styles.image }/>
                        :
                        <>
                            <FontAwesome5
                                name={ 'user-circle' }
                                color={ Colors.black }
                                size={ 70 }
                                solid
                                style={ styles.userIcon }
                            />
                        </>
                    }
                    <View>
                        <Text style={ styles.name }>{ name }</Text>
                        <Text style={ styles.role }>{ role }</Text>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        borderBottomEndRadius: 50,
        borderBottomStartRadius: 50,
        height: heightPercent(15) + Constants.statusBarHeight,
        paddingTop: Constants.statusBarHeight,
        position: 'relative',
    },
    image: {
        borderColor: Colors.gray,
        borderRadius: 70,
        borderWidth: 1,
        height: widthPx(70),
        marginRight: widthPx(20),
        width: widthPx(70),
    },
    chatButton: {
        position: 'absolute',
        top: Constants.statusBarHeight,
        right: widthPx(5),
    },
    userIcon: {
        marginRight: widthPx(20),
    },
    userInfo: {
        alignItems: 'center',
        alignSelf: 'flex-start',
        backgroundColor: 'transparent',
        display: 'flex',
        flexDirection: 'row',
        flexGrow: 1,
        paddingHorizontal: widthPx(25),
        paddingTop: heightPx(5),
    },
    name: {
        fontSize: fontScale(24),
        fontFamily: 'Poppins semiBold',
        lineHeight: 29,
    },
    role: {
        fontFamily: 'Poppins medium',
        fontSize: fontScale(18),
        lineHeight: 24,
    }
});
