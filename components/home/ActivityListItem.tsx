import * as React from 'react';
import {View, StyleSheet, TouchableWithoutFeedback} from 'react-native';
import {FontAwesome5} from '@expo/vector-icons';
import {fontScale, heightPx, widthPercent, widthPx} from '../../helpers/Dimensions';
import {Colors} from '../../theme/Colors';
import {Text} from '../text/Text';
import {AppointmentType} from '../../store/slices/calendar/Calendar.slice';

type Props = {
    appointment: AppointmentType,
    theme: 'dark' | 'light',
    viewActivity: () => void,
}

export const ActivityListItem = (props: Props) => {
    const {appointment, theme, viewActivity} = props;

    const appDate = new Date(appointment.date);
    const formattedDate = `${ ('0' + appDate.getDate()).slice(-2) }.${ ('0' + (appDate.getMonth() + 1)).slice(-2) }.${ appDate.getFullYear() }`;
    let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    return (
        <TouchableWithoutFeedback onPress={ () => viewActivity() }>
            <View style={ [styles.container, {borderColor: (theme === 'dark' ? Colors.white : Colors.od_dark)}] }>

                <View style={ styles.dayDate }>
                    <Text
                        content={ days[appDate.getDay()] }
                        size={ 16 }
                        fontFamily={ 'Poppins regular' }
                        color={ theme === 'dark' ? Colors.white : Colors.od_dark }
                    />
                    <Text
                        content={ formattedDate }
                        size={ 16 }
                        fontFamily={ 'Poppins regular' }
                        color={ theme === 'dark' ? Colors.white : Colors.od_dark }
                    />
                </View>
                <Text
                    content={ appointment.hour }
                    size={ 16 }
                    fontFamily={ 'Poppins regular' }
                    color={ theme === 'dark' ? Colors.white : Colors.od_dark }
                    style={ styles.hour }
                />
                <Text
                    content={ appointment.title }
                    size={ 16 }
                    fontFamily={ 'Poppins regular' }
                    color={ theme === 'dark' ? Colors.white : Colors.od_dark }
                    style={ styles.role }
                />
                <FontAwesome5
                    color={ theme === 'dark' ? Colors.white : Colors.od_dark }
                    size={ 30 }
                    name={ 'angle-right' }
                />
            </View>
        </TouchableWithoutFeedback>
    );
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: widthPx(15),
        paddingVertical: heightPx(15),
        borderStyle: 'solid',
        borderBottomWidth: .5,
    },
    text: {
        color: Colors.white,
        fontSize: fontScale(16),
        fontFamily: 'Poppins regular',
        flexGrow: 1,
    },
    dayDate: {
        width: widthPercent(35),
    },
    hour: {
        width: widthPercent(20),
    },
    role: {
        width: widthPercent(35),
    }

});
