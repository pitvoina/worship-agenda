import * as React from 'react';
import {StyleSheet, View} from 'react-native';
import {ActivityListItem} from './ActivityListItem';
import {heightPx} from '../../helpers/Dimensions';
import {useSelector} from 'react-redux';
import {RootState} from '../../store/store';
import {AppointmentType} from '../../store/slices/calendar/Calendar.slice';

interface Props {
    onActivityPress: (appointment: AppointmentType) => void,
}

export const HomeActivityList = (props: Props) => {
    const {onActivityPress} = props;
    const {appointments} = useSelector((state: RootState) => state.calendar);
    const {userId} = useSelector((state: RootState) => state.user);

    return (
        <View style={ styles.container }>
            { appointments.length !== 0 && appointments.map((appointment: AppointmentType, key: number) => (key < 5 && !appointment.archive.includes(String(userId))) &&
                <ActivityListItem viewActivity={ () => onActivityPress(appointment) } appointment={ appointment } key={ appointment.appointmentId } theme={ 'dark' }/>) }
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        marginTop: heightPx(25),
        marginBottom: heightPx(25)
    }
})
