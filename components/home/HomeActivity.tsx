import * as React from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import {fontScale, heightPx, widthPercent} from '../../helpers/Dimensions';
import {useNavigation} from '@react-navigation/native';
import {Colors} from '../../theme/Colors';
import {HomeActivityList} from './HomeActivityList';
import {AppointmentType} from '../../store/slices/calendar/Calendar.slice';

interface Props {
    getActivityData: (appointment: AppointmentType) => void
}

export const HomeActivity = (props: Props) => {
    const {getActivityData} = props;
    const navigation = useNavigation();

    return (
        <ScrollView style={ styles.container }>
            <View style={ styles.header }>
                <Text style={ styles.headerText }>Upcoming activities</Text>
                <Text
                    style={ [styles.headerLink, styles.headerText] }
                    onPress={ () => navigation.navigate('Calendar') }
                >
                    { 'See all' }
                </Text>
            </View>
            <HomeActivityList onActivityPress={ getActivityData }/>
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        paddingTop: heightPx(10),
        marginBottom: heightPx(5)
    },
    header: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: widthPercent(5),
    },
    headerText: {
        color: Colors.white,
        fontSize: fontScale(18),
        fontFamily: 'Poppins semiBold'
    },
    headerLink: {
        textDecorationLine: 'underline',
    }
})
