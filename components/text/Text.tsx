import * as React from 'react';
import {OpaqueColorValue, Text as BaseText, TextProps} from 'react-native';
import {fontScale, heightPx} from '../../helpers/Dimensions';
import {Colors} from '../../theme/Colors';

export type Spacing = {
    position: 'bottom' | 'top',
    size: number
}

type Props = {
    color?: string | OpaqueColorValue | undefined,
    content: string | null,
    fontFamily?: string,
    size?: number,
    spacing?: Spacing | null
} & TextProps;

export const Text = (props: Props) => {
    const {content, style, size = 16, color = Colors.white, fontFamily = 'Poppins regular', spacing = null, numberOfLines, onPress} = props;
    return (
        <BaseText
            onPress={ onPress }
            numberOfLines={ numberOfLines }
            style={ [
                {
                    fontSize: fontScale(size),
                    color: color,
                    fontFamily: fontFamily,
                    lineHeight: heightPx(size + 8),
                    letterSpacing: .8
                },
                (
                    spacing && (
                        spacing.position === 'bottom' ?
                            {marginBottom: heightPx(spacing.size)}
                            :
                            {marginTop: heightPx(spacing.size)}
                    )
                ),
                style
            ] }
        >
            { content }
        </BaseText>
    );
}
