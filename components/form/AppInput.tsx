import * as React from 'react';
import {Colors} from '../../theme/Colors';
import {Dispatch, SetStateAction, useEffect, useState} from 'react';
import {FontAwesome5, FontAwesome} from '@expo/vector-icons';
import {KeyboardTypeOptions, OpaqueColorValue, StyleProp, StyleSheet, Text, TextInput, TextInputProps, TextStyle, View, ViewStyle} from 'react-native';
import {Message} from '../messages/Message';
import {fontScale, heightPx, widthPercent} from '../../helpers/Dimensions';
import {err} from 'react-native-svg/lib/typescript/xml';

export type Error = { message: string | undefined, hasError: boolean } | undefined;
export type InputType = 'email' | 'password' | 'name' | 'lastname' | 'registerPassword' | 'phone' | 'ministry';

type Props = {
    actionButton?: {
        icon: any,
        action: () => void,
    },
    borderColor?: string | OpaqueColorValue | undefined,
    inputLabel?: string,
    inputPlaceholder: string,
    inputStyle?: StyleProp<TextStyle> | undefined,
    labelStyle?: StyleProp<TextStyle> | undefined,
    placeholderColor?: string | OpaqueColorValue | undefined,
    autoCorrect: boolean,
    isPassword?: boolean | undefined,
    keyboardType?: KeyboardTypeOptions | undefined,
    hasIcon?: boolean,
    type?: InputType,
    preIcon?: {
        name: any,
        color?: string | OpaqueColorValue | undefined,
        size?: number | undefined,
        style?: StyleProp<TextStyle> | undefined
    },
    value: string,
    setValueFn: any,
    errorCode?: string,
    setInputError?: (error: Error) => void,
    submit?: boolean,
    inputContainerStyle?: ViewStyle,
    validate?: boolean,
    inputDirty?: boolean,
    error?: Error,
} & TextInputProps;


export const AppInput = (props: Props) => {
    const {
        actionButton,
        autoCorrect,
        borderColor,
        editable,
        error,
        errorCode,
        hasIcon,
        inputContainerStyle,
        inputDirty,
        setInputError,
        inputLabel,
        inputPlaceholder,
        inputStyle,
        isPassword,
        keyboardType,
        labelStyle,
        multiline,
        onBlur,
        onFocus,
        placeholderColor,
        preIcon,
        selectionColor,
        setValueFn,
        submit,
        type,
        value,
        validate
    } = props;
    const [showPassword, setShowPassword] = useState(isPassword);
    const [isDirty, setIsDirty] = useState(!!inputDirty);

    const setError = () => {
        if (!validate) return;
        if (setInputError) {
            setInputError(error);
        }
    }

    useEffect(() => {
        setError();
        setIsDirty(false);
    }, [errorCode]);

    useEffect(() => {
        if (!errorCode && isDirty) {
            setError();
        }
    }, [value]);

    useEffect(() => {
        if (submit) {
            setIsDirty(true);
            setError();
        }
    }, [submit, isDirty]);


    const onChangeFn = (text: string) => {
        if (error?.hasError && setInputError) setInputError({message: '', hasError: false});
        setValueFn(text);
    }

    return (
        <View style={ styles.input }>
            { inputLabel && <Text style={ [styles.label, labelStyle, (error?.hasError ? styles.labelError : undefined)] }>{ inputLabel }</Text> }
            <View style={ [styles.inputContainer, inputContainerStyle] }>
                { preIcon?.name && <FontAwesome name={ preIcon.name } size={ preIcon.size } color={ preIcon.color } style={ [styles.preIcon, preIcon.style] }/> }
                <TextInput
                    autoComplete={ 'off' }
                    autoCorrect={ autoCorrect }
                    editable={ editable }
                    keyboardType={ keyboardType }
                    multiline={ multiline }
                    onBlur={ onBlur }
                    onChangeText={ onChangeFn }
                    onFocus={ onFocus }
                    placeholder={ inputPlaceholder }
                    placeholderTextColor={ error?.hasError ? Colors.light_red : placeholderColor }
                    secureTextEntry={ showPassword }
                    selectionColor={ selectionColor ?? 'rgba(0,0,0,.2)' }
                    style={ [
                        styles.input,
                        inputStyle,
                        {borderColor: (error?.hasError ? Colors.light_red : borderColor)},
                        {paddingRight: (hasIcon ? 60 : 0)},
                        {paddingLeft: (preIcon?.name && preIcon.size ? preIcon.size : 0)}
                    ] }
                    value={ value }
                />
                { (isPassword && hasIcon) && <FontAwesome5 style={ styles.icon } name={ showPassword ? 'eye' : 'eye-slash' } color={ Colors.white } size={ 20 } onPress={ () => setShowPassword(!showPassword) }/> }
                { actionButton?.icon && <FontAwesome style={ styles.actionButton } name={ actionButton.icon } size={ 25 } color={ Colors.ob_dark } onPress={ () => actionButton?.action() }/> }
            </View>
            { (error?.hasError && error.message) && <Message style={ styles.errorStyle } type={ 'error' } text={ error.message }/> }
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        position: 'relative'
    },
    preIcon: {
        position: 'absolute',
    },
    inputContainer: {
        marginBottom: heightPx(40),
        position: 'relative',
        width: widthPercent(75),
    },
    errorStyle: {
        bottom: 5,
        maxWidth: widthPercent(75),
        position: 'absolute',
    },
    input: {
        fontFamily: 'Poppins light',
        fontSize: fontScale(16),
        paddingBottom: 0,
        paddingTop: heightPx(5),
    },
    label: {
        fontFamily: 'Poppins semiBold',
        fontSize: fontScale(22),
        lineHeight: fontScale(25),
    },
    labelError: {
        color: Colors.light_red,
    },
    errorMessage: {
        color: Colors.light_red,
        fontSize: fontScale(14),
        marginTop: heightPx(5),
        position: 'absolute',
        fontFamily: 'Poppins extraLight',
        bottom: heightPx(-23),
    },
    icon: {
        position: 'absolute',
        right: 0,
        bottom: heightPx(-10),
        padding: heightPx(15),
    },
    actionButton: {
        position: 'absolute',
        bottom: 7,
        right: 15,
    }
})
