import * as React from 'react';
import {StyleProp, StyleSheet, Text, View} from 'react-native';
import {FontAwesome5} from '@expo/vector-icons';
import {Colors} from '../../theme/Colors';
import {fontScale, heightPx, widthPx} from '../../helpers/Dimensions';

interface Props {
    customStyle?: StyleProp<any>
}

export const NewsBubble = (props: Props) => {
    const {customStyle} = props;

    return (
        <View style={[styles.container, customStyle ]}>
            <View style={ styles.nameDate }>
                <Text style={styles.dnText}>{ 'Sammy' }</Text>
                <Text style={styles.dnText}>{ '18/01/22' }</Text>
            </View>
            <Text style={ styles.hashtag }>{ '#Multimedia' }</Text>
            <Text
                numberOfLines={ 3 }
                style={ styles.comment }
            >
                { 'Long ass textLong ass textLong ass textLong ass textLong ass textLong ass textLong ass textLong ass textLong ass textLong ass textLong ass textLong ass textLong ass text' }
            </Text>
            <FontAwesome5
                size={22}
                style={ styles.button }
                name={ 'angle-right' }
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        borderRadius: 24,
        padding: 20,
        position: 'relative',
        width: widthPx(260),
    },
    nameDate: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    dnText: {
        fontFamily: 'Poppins medium',
        fontSize: 15,
        color: Colors.gray,
    },
    hashtag: {
        fontFamily: 'Poppins bold',
        fontSize: fontScale(16),
        marginBottom: heightPx(2),
    },
    comment: {
        lineHeight: 20,
        width: '95%',
    },
    button: {
        bottom: 3,
        padding: 15,
        position: 'absolute',
        right: 0,
    }
});
