import * as React from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import {NewsBubble} from './NewsBubble';
import {fontScale, heightPx, widthPercent} from '../../helpers/Dimensions';
import {Colors} from '../../theme/Colors';

export const NewsSideScroll = () => {
    return (
        <ScrollView horizontal={ true }>
            <View style={ styles.container }>
                <Text style={ styles.sectionHeader }>{ 'Latest forums' }</Text>
                <View style={ styles.sideWayScroll }>
                    <NewsBubble customStyle={ {marginRight: 10} }/>
                    <NewsBubble customStyle={ {marginRight: 10} }/>
                    <NewsBubble/>
                </View>
                <View style={ styles.indicatorContainer }>
                    <View style={ [styles.indicator, styles.activeIndicator] }/>
                    <View style={ [styles.indicator] }/>
                    <View style={ [styles.indicator] }/>
                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: widthPercent(5),
        paddingVertical: heightPx(20),
    },
    sectionHeader: {
        color: Colors.white,
        fontFamily: 'Poppins semiBold',
        fontSize: fontScale(18),
        marginBottom: heightPx(10),
    },
    sideWayScroll: {
        flexDirection: 'row',
    },
    indicatorContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: heightPx(10),
        width: widthPercent(90),
    },
    indicator: {
        backgroundColor: Colors.white,
        borderRadius: 12,
        height: heightPx(12),
        marginRight: 6,
        width: heightPx(12),
    },
    activeIndicator: {
        backgroundColor: Colors.red,
        width: 40,
    }
});
