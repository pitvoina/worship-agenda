import * as React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useState} from 'react';
import {AppInput} from '../form/AppInput';
import {Colors} from '../../theme/Colors';
import {Button} from 'react-native-paper';
import {fontScale, heightPercent, heightPx, widthPercent, widthPx} from '../../helpers/Dimensions';
import {signIn} from '../../store/slices/user/User.thunk';
import {useDispatch} from 'react-redux';

export const LoginForms = () => {
    const navigation = useNavigation();
    const dispatch = useDispatch();

    const [errorCode, setErrorCode] = useState();
    const [email, setEmail] = useState('pitvoina@gmail.com');
    const [password, setPassword] = useState('123456');

    return (
        <View style={ styles.container }>
            <AppInput
                value={ email }
                setValueFn={ setEmail }
                errorCode={ errorCode }
                type={ 'email' }
                autoCorrect={ false }
                inputLabel={ 'Username' }
                inputPlaceholder={ 'my@email.com' }
                inputStyle={ styles.inputStyle }
                labelStyle={ styles.labelStyle }
                placeholderColor={ Colors.white }
                keyboardType={ 'email-address' }
            />
            <AppInput
                value={ password }
                setValueFn={ setPassword }
                errorCode={ errorCode }
                type={ 'password' }
                autoCorrect={ false }
                inputLabel={ 'Password' }
                inputPlaceholder={ 'password' }
                inputStyle={ styles.inputStyle }
                isPassword={ true }
                labelStyle={ styles.labelStyle }
                placeholderColor={ Colors.white }
                hasIcon={ true }
            />
            <View style={ styles.signUpTexts }>
                <Text style={ styles.registerText }>{ 'Don`t have an account?' }</Text>
                <Text style={ styles.registerLink } onPress={ () => navigation.navigate('Register') }>{ 'Sign Up.' }</Text>
            </View>
            <Button
                mode={ 'contained' }
                style={ styles.loginButton }
                labelStyle={ {color: Colors.black, fontSize: fontScale(20)} }
                onPress={ () => dispatch(signIn({email: email, password: password})) }
            >
                { 'Login' }
            </Button>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        paddingTop: heightPercent(1),
    },
    labelStyle: {
        color: Colors.white,
    },
    inputStyle: {
        borderBottomWidth: 2,
        borderStyle: 'solid',
        color: Colors.white,
    },
    registerText: {
        color: Colors.white,
        fontFamily: 'Poppins regular',
        fontSize: fontScale(20),
    },
    registerLink: {
        color: Colors.white,
        fontFamily: 'Poppins semiBold',
        fontSize: fontScale(20),
        textDecorationColor: Colors.white,
        textDecorationLine: 'underline',
        textDecorationStyle: 'solid',
        marginLeft: widthPx(5)
    },
    loginButton: {
        backgroundColor: Colors.white,
        borderRadius: 0,
        marginTop: heightPx(20),
        marginBottom: heightPercent(2.5),
        paddingVertical: heightPx(5),
        width: widthPercent(75),
    },
    signUpTexts: {
        flexDirection: 'row',
        width: widthPercent(75),
        alignItems: 'center'
    }
});
