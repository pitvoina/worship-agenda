import * as React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {SignInLogo} from '../../svg/SignInLogo';
import {BackButton} from '../buttons/BackButton';
import {fontScale, heightPercent, heightPx} from '../../helpers/Dimensions';

export const LoginHeader = () => {
    return (
        <View style={ styles.container }>
            <BackButton/>
            <SignInLogo/>
            <Text style={ styles.logoText }>{ 'Sign in' }</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        height: heightPercent(50),
        justifyContent: 'center',
    },
    logoText: {
        fontSize: fontScale(45),
        fontFamily: 'Poppins semiBold',
        marginTop: heightPx(20),
    },
})
