import * as React from 'react';
import {Colors} from '../../theme/Colors';
import {ProfileChatButtonGroup} from '../buttons/ProfileChatButtonGroup';
import {StyleSheet, View} from 'react-native';
import {Text} from '../text/Text';

export const CalendarHeader = () => {
    return (
        <View style={ styles.container }>
            <Text content={ 'Calendar' } color={ Colors.black } size={ 30 } fontFamily={ 'Poppins bold' } style={ styles.text }/>
            <ProfileChatButtonGroup/>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 15,
        paddingRight: 5,
        paddingVertical: 15,
    },
    text: {
        marginRight: 'auto',
    }
});
