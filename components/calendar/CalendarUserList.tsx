import * as React from 'react';
import Constants from 'expo-constants';
import {CalendarSlice} from '../../store/slices/calendar/Calendar.slice';
import {Colors} from '../../theme/Colors';
import {RootState} from '../../store/store';
import {Image, ScrollView, StyleSheet, TouchableWithoutFeedback, View} from 'react-native';
import {Text} from '../text/Text';
import {fontScale, heightPercent, heightPx, widthPercent, widthPx} from '../../helpers/Dimensions';
import {useDispatch, useSelector} from 'react-redux';
import {useState} from 'react';
import {FontAwesome5} from '@expo/vector-icons';

export const CalendarUserList = () => {
    const dispatch = useDispatch();
    const {setShowUserList, setNewAppointment} = CalendarSlice;
    const {newAppointment} = useSelector((state: RootState) => state.calendar);
    const {userList} = useSelector((state: RootState) => state.user);
    const [selectedMembers, setSelectedMembers] = useState<string[]>(newAppointment.users);

    const handleSelectMember = (userId: string) => {
        if (selectedMembers.includes(userId)) {
            setSelectedMembers(selectedMembers.filter((ids) => ids !== userId));
        } else {
            setSelectedMembers([...selectedMembers, userId]);
        }
    }

    const handleSetSelectedMembers = () => {
        if (selectedMembers.length > 0) {
            dispatch(setNewAppointment({...newAppointment, users: selectedMembers}));
            dispatch(setShowUserList(false));
        }
    }

    return (
        <View style={ styles.backdrop }>
            <View style={ styles.container }>
                <ScrollView style={ styles.scrollView }>
                    { userList && userList.map(user =>
                        <TouchableWithoutFeedback
                            onPress={ () => handleSelectMember(user.userId) }
                            key={ user.userId }
                        >
                            <View key={ user.userId } style={ [(selectedMembers.includes(user.userId) && styles.selected), styles.userContainer] }>
                                { user.photo ?
                                    <Image source={ {uri: user.photo} } style={ styles.image }/>
                                    :
                                    <FontAwesome5
                                        solid
                                        color={ Colors.white }
                                        name={ 'user-circle' }
                                        size={ fontScale(52) }
                                        style={ styles.image }
                                    />
                                }
                                <View>
                                    <Text content={ user.name } size={ 22 } fontFamily={ 'Poppins semiBold' }/>
                                    <Text content={ user.role } size={ 16 } fontFamily={ 'Poppins light' }/>
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    ) }
                </ScrollView>
                <View style={ styles.buttons }>
                    <TouchableWithoutFeedback onPress={ () => dispatch(setShowUserList(false)) }>
                        <View style={ styles.cancelButton }>
                            <Text content={ 'Cancel' } fontFamily={ 'Poppins semiBold' } size={ 22 }/>
                        </View>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback
                        onPress={ () => handleSetSelectedMembers() }
                    >
                        <View style={ [styles.saveButton, selectedMembers.length > 0 && {backgroundColor: Colors.green}] }>
                            <Text content={ 'Ok' } fontFamily={ 'Poppins semiBold' } size={ 22 }/>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    backdrop: {
        elevation: 999,
        zIndex: 999,
        position: 'absolute',
        backgroundColor: Colors.backdrop,
        top: Constants.statusBarHeight,
        width: widthPercent(100),
        height: heightPercent(100),
        left: 0,
        bottom: 0,
        paddingHorizontal: widthPx(30),
        paddingVertical: widthPx(40),
    },
    container: {
        backgroundColor: Colors.ob_dark,
        borderColor: Colors.light_teal,
        borderRadius: 2,
        borderWidth: .2,
        flex: 1,
        flexGrow: 1,
        padding: widthPx(20)
    },
    scrollView: {
        flexGrow: 1,
    },
    buttons: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: heightPx(10),
    },
    cancelButton: {
        alignItems: 'center',
        backgroundColor: Colors.red,
        borderRadius: 4,
        paddingVertical: heightPx(10),
        width: widthPercent(35),
    },
    saveButton: {
        alignItems: 'center',
        backgroundColor: Colors.gray,
        borderRadius: 4,
        paddingVertical: heightPx(10),
        width: widthPercent(35),
    },
    selected: {
        backgroundColor: Colors.dark_teal,
    },
    image: {
        borderRadius: 50,
        height: widthPx(50),
        marginHorizontal: widthPx(15),
        width: widthPx(50),
    },
    userContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        marginBottom: heightPx(5),
        minHeight: heightPx(75),
        paddingVertical: heightPx(5),
    }
})
