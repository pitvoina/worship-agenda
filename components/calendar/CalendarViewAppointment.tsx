import * as React from 'react';
import {ScrollView, StyleSheet, TouchableWithoutFeedback, View} from 'react-native';
import Animated, {SlideInDown, SlideOutDown} from 'react-native-reanimated';
import {Ionicons} from '@expo/vector-icons';
import {Colors} from '../../theme/Colors';
import {heightPercent, heightPx, widthPercent, widthPx} from '../../helpers/Dimensions';
import {Calendar} from './Calendar';
import {Text} from '../text/Text';
import {CalendarForm} from './CalendarForm';
import {AppointmentType} from '../../store/slices/calendar/Calendar.slice';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../../store/store';
import {setArchive} from '../../store/slices/calendar/Calendar.thunk';

interface Props {
    activityData: AppointmentType,
    close: () => void,
}

export const CalendarViewAppointment = (props: Props) => {
    const dispatch = useDispatch();
    const {userId} = useSelector((state: RootState) => state.user);
    const {close, activityData} = props;

    const handleSetArchive = () => {
        dispatch(setArchive({userId: String(userId), appointmentId: activityData.appointmentId}));
        close();
    }

    return (
        <Animated.View
            style={ [styles.container] }
            entering={ SlideInDown }
            exiting={ SlideOutDown }
        >
            <ScrollView
                contentContainerStyle={ styles.scrollContainer }
            >
                <>
                    <Ionicons style={ styles.close } name="close" size={ 30 } color={ Colors.white } onPress={ close }/>
                    <Calendar
                        isCreate={ false }
                        date={ activityData.date }
                    />
                    <View style={ styles.hourContainer }>
                        <Text content={ 'Hour' } size={ 30 } fontFamily={ 'Poppins semiBold' }/>
                        <View style={ styles.pickedHour }>
                            <Text
                                content={ activityData.hour }
                                color={ Colors.white }
                                size={ 50 }
                            />
                        </View>
                    </View>
                    <CalendarForm
                        isForm={ false }
                        titleData={ activityData.title }
                        notesData={ activityData.info }
                        membersData={ activityData.users }
                    />
                    { activityData.createdBy !== userId ?
                        <TouchableWithoutFeedback onPress={ () => console.log(1234) }>
                            <View style={ styles.archiveButton }>
                                <Text content={ 'Delete' } size={ 20 }/>
                            </View>
                        </TouchableWithoutFeedback>
                        :
                        <TouchableWithoutFeedback onPress={ () => handleSetArchive() }>
                            <View style={ styles.archiveButton }>
                                <Text content={ 'Move to archive' } size={ 20 }/>
                            </View>
                        </TouchableWithoutFeedback>
                    }
                </>
            </ScrollView>
        </Animated.View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        bottom: 0,
        elevation: 5,
        flex: 1,
        height: heightPercent(90),
        position: 'absolute',
        width: widthPercent(100),
        zIndex: 5,
    },
    scrollContainer: {
        backgroundColor: Colors.o3_dark,
        borderTopRightRadius: 70,
        flexGrow: 1,
        paddingBottom: heightPx(40),
        paddingHorizontal: widthPx(15),
        paddingTop: heightPx(15),
    },
    close: {
        marginLeft: 'auto',
        marginRight: widthPx(20),
    },
    pickedHour: {
        marginLeft: 'auto'
    },
    hourContainer: {
        borderBottomWidth: 1,
        borderColor: 'rgba(255, 255, 255, .6)',
        borderTopWidth: 1,
        flexDirection: 'row',
        paddingHorizontal: widthPx(15),
        paddingVertical: heightPx(25),
        alignItems: 'center'
    },
    saveButton: {
        alignSelf: 'center',
        backgroundColor: Colors.white,
        borderRadius: 50,
        paddingHorizontal: widthPx(45),
        paddingVertical: heightPx(10),
    },
    archiveButton: {
        alignSelf: 'center',
        backgroundColor: Colors.light_red,
        borderRadius: 20,
        paddingHorizontal: widthPx(25),
        paddingVertical: heightPx(10),
    }
});
