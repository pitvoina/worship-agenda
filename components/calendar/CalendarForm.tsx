import * as React from 'react';
import {StyleSheet, TouchableWithoutFeedback, View} from 'react-native';
import {AppInput} from '../form/AppInput';
import {Colors} from '../../theme/Colors';
import {fontScale, heightPx, widthPercent, widthPx} from '../../helpers/Dimensions';
import {Text} from '../text/Text';
import {Ionicons} from '@expo/vector-icons';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../../store/store';
import {CalendarSlice} from '../../store/slices/calendar/Calendar.slice';

interface Props {
    isForm: boolean,
    titleData?: string,
    notesData?: string,
    membersData?: string[],
}

export const CalendarForm = (props: Props) => {
    const {isForm, titleData, notesData, membersData} = props;
    const dispatch = useDispatch()
    const {setNewAppointmentTitle, setNewAppointmentInfo, setShowUserList, setNewAppointment} = CalendarSlice;
    const {newAppointment} = useSelector((state: RootState) => state.calendar);
    const {userList} = useSelector((state: RootState) => state.user);

    const removeBandMember = (id: string) => {
        const removed = newAppointment.users.filter(userId => userId !== id);
        dispatch(setNewAppointment({...newAppointment, users: removed}));
    }

    return (
        <View style={ styles.container }>
            <AppInput
                autoCorrect={ false }
                editable={ isForm }
                inputContainerStyle={ styles.inputContainerStyle }
                inputLabel={ 'Title' }
                inputPlaceholder={ 'Add title' }
                inputStyle={ styles.inputStyle }
                labelStyle={ styles.labelStyle }
                placeholderColor={ 'rgba(255, 255, 255,.5)' }
                setValueFn={ (value: string) => dispatch(setNewAppointmentTitle(value)) }
                value={ isForm ? newAppointment.title : String(titleData) }
            />
            <View>
                <Text content={ 'Additional information' } color={ Colors.white } size={ 20 } style={ styles.header }/>
                <AppInput
                    autoCorrect={ false }
                    editable={ isForm }
                    inputContainerStyle={ styles.inputContainerStyle }
                    inputPlaceholder={ 'Add note' }
                    inputStyle={ styles.inputStyle }
                    labelStyle={ styles.labelStyle }
                    placeholderColor={ 'rgba(255, 255, 255,.5)' }
                    preIcon={ {
                        name: 'sticky-note-o',
                        color: 'rgba(255, 255, 255, .7)',
                        size: 25
                    } }
                    setValueFn={ (value: string) => dispatch(setNewAppointmentInfo(value)) }
                    value={ isForm ? newAppointment.info : String(notesData) }
                />

                {
                    (isForm && newAppointment.users.length > 0) &&
                    <View style={ [styles.selectedMembers, {top: heightPx(0), paddingBottom: heightPx(15), justifyContent: 'center'}] }>
                        {
                            newAppointment.users.map(id =>
                                userList.map(user =>
                                    (user.userId === id) &&
                                    (
                                        <TouchableWithoutFeedback key={ user.userId } onPress={ () => removeBandMember(id) }>
                                            <View style={ [styles.pill, {paddingRight: widthPx(5)}] }>
                                                <Text content={ `${ user.name }` }/>
                                                { !!user.role && <Text content={ `(${ user.role })` } size={ 12 }/> }
                                                <Ionicons name={ 'md-close' } size={ 22 } color={ Colors.white }/>
                                            </View>
                                        </TouchableWithoutFeedback>
                                    )
                                )
                            )
                        }
                    </View>
                }

                { isForm &&
                <TouchableWithoutFeedback
                    onPress={ () => dispatch(setShowUserList(true)) }
                >
                    <View style={ styles.showMembers }>
                        <Text content={ 'Select band members' } color={Colors.ob_dark} fontFamily={ 'Poppins semiBold' } size={ 18 }/>
                    </View>
                </TouchableWithoutFeedback>
                }

                { !isForm && <View style={ styles.selectedMembers }>
                    { membersData &&
                    membersData.map((id, _index: number) =>
                        userList.map(user =>
                            (user.userId === id) &&
                            (
                                <View style={ styles.pill } key={ user.userId }>
                                    <Text content={ `${ user.name }` }/>
                                    { !!user.role && <Text content={ `(${ user.role })` } size={ 12 }/> }
                                </View>
                            )
                        )
                    ) }
                </View> }
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        paddingVertical: heightPx(20),
    },
    labelStyle: {
        color: Colors.white,
        paddingHorizontal: widthPx(5),
    },
    header: {
        paddingHorizontal: widthPx(5),
    },
    pill: {
        alignItems: 'center',
        backgroundColor: Colors.teal,
        borderRadius: 20,
        flexDirection: 'row',
        marginBottom: heightPx(5),
        marginRight: widthPx(5),
        paddingHorizontal: widthPx(10),
    },
    selectedMembers: {
        top: heightPx(-25),
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingHorizontal: widthPx(0),
    },
    showMembers: {
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor: Colors.light_teal,
        marginBottom: heightPx(25),
        padding: heightPx(10),
        paddingHorizontal: widthPx(20),
        paddingVertical: heightPx(10),
        borderRadius: 4,
    },
    inputStyle: {
        height: heightPx(40),
        color: 'rgba(255, 255, 255,.8)',
        fontSize: fontScale(18)
    },
    inputContainerStyle: {
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderColor: 'rgba(255 ,255, 255, .5)',
        paddingHorizontal: widthPx(5),
        width: widthPercent(100) - 30,
    }
});
