import * as React from 'react';
import {ScrollView, StyleSheet, TouchableWithoutFeedback, View} from 'react-native';
import {Text} from '../text/Text';
import {Colors} from '../../theme/Colors';
import {heightPx, widthPx} from '../../helpers/Dimensions';
import {ActivityListItem} from '../home/ActivityListItem';
import {Ionicons} from '@expo/vector-icons';
import Animated, {Easing, useAnimatedStyle, withTiming} from 'react-native-reanimated';
import {useEffect, useState} from 'react';
import {Menu} from '../menu/Menu';
import {CalendarCreateAppointment} from './CalendarCreateAppointment';
import {CalendarViewAppointment} from './CalendarViewAppointment';
import {useDispatch, useSelector} from 'react-redux';
import {getAppointments} from '../../store/slices/calendar/Calendar.thunk';
import {RootState} from '../../store/store';
import {AppointmentType} from '../../store/slices/calendar/Calendar.slice';

export const CalendarBody = () => {
    const dispatch = useDispatch();
    const {appointments} = useSelector((state: RootState) => state.calendar);
    const {userId} = useSelector((state: RootState) => state.user);
    const [showNewApp, setShowNewApp] = useState(true);
    const [createApp, setCreateApp] = useState(false);
    const [activityData, setActivityData] = useState<any>(null);


    useEffect(() => {
        dispatch(getAppointments({userId: String(userId)}));
    }, []);


    const onScrollStart = () => {
        setShowNewApp(false);
    }

    const onScrollEnd = () => {
        setTimeout(() => setShowNewApp(true), 1500);
    }

    const config = {duration: 500, easing: Easing.bezier(0.5, 0.01, 0, 1)};
    const animated = useAnimatedStyle(() => {
        return {
            opacity: withTiming(showNewApp ? 1 : 0, config),
            right: withTiming(showNewApp ? 10 : -200, config)
        };
    });

    const getActivityData = (appointment: AppointmentType) => {
        setActivityData(appointment);
    }

    return (
        <View style={ styles.container }>
            <Text
                size={ 20 }
                content={ 'Your appointments' }
                color={ Colors.od_dark }
                fontFamily={ 'Poppins semiBold' }
                style={ styles.header }
            />
            <ScrollView
                onScrollBeginDrag={ () => onScrollStart() }
                onScrollEndDrag={ () => onScrollEnd() }

                style={ {flex: 1} }
            >
                { appointments.length !== 0 && appointments.map(
                    (appointment: AppointmentType) =>
                        !appointment.archive.includes(String(userId)) &&
                        <ActivityListItem
                            viewActivity={ () => getActivityData(appointment) }
                            appointment={ appointment }
                            key={ appointment.appointmentId }
                            theme={ 'light' }
                        />
                ) }
            </ScrollView>
            <TouchableWithoutFeedback onPress={ () => setCreateApp(true) }>
                <Animated.View style={ [styles.newAppointment, animated] }>
                    <Text content={ 'New appointment' } color={ Colors.light_red } style={ styles.buttonText }/>
                    <Ionicons name="add-circle" size={ 50 } color={ Colors.light_red }/>
                </Animated.View>
            </TouchableWithoutFeedback>
            <Menu/>
            { createApp && <CalendarCreateAppointment close={ () => setCreateApp(false) }/> }
            { activityData && <CalendarViewAppointment
                activityData={ activityData }
                close={ () => setActivityData(null) }
            /> }
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    header: {
        borderBottomWidth: 1,
        borderColor: 'rgba(0 , 0, 0, .3)',
        paddingHorizontal: widthPx(15),
        paddingBottom: heightPx(5),
    },
    newAppointment: {
        backgroundColor: Colors.ob_dark,
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'flex-end',
        position: 'absolute',
        bottom: heightPx(80),
        paddingRight: widthPx(2),
        paddingLeft: widthPx(15),
        borderRadius: 60,
    },
    buttonText: {
        marginRight: widthPx(10)
    }
});
