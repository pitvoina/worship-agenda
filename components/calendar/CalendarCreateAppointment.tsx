import * as React from 'react';
import {ScrollView, StyleSheet, TouchableWithoutFeedback, View} from 'react-native';
import Animated, {SlideInDown, SlideOutDown} from 'react-native-reanimated';
import {Ionicons} from '@expo/vector-icons';
import {Colors} from '../../theme/Colors';
import {heightPercent, heightPx, widthPercent, widthPx} from '../../helpers/Dimensions';
import {Calendar} from './Calendar';
import {Text} from '../text/Text';
import {useEffect, useState} from 'react';
import DateTimePicker from '@react-native-community/datetimepicker';
import {CalendarForm} from './CalendarForm';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../../store/store';
import {CalendarSlice} from '../../store/slices/calendar/Calendar.slice';
import {createNewAppointment} from '../../store/slices/calendar/Calendar.thunk';

interface Props {
    close: () => void
}

export const CalendarCreateAppointment = (props: Props) => {
    const {close} = props;
    const dispatch = useDispatch();
    const {newAppointment} = useSelector((state: RootState) => state.calendar);
    const {userId} = useSelector((state: RootState) => state.user);
    const {setNewAppointment, clearNewAppointment} = CalendarSlice;
    const [showTimePicker, setShowTimePicker] = useState(false);
    const [canSave, setCanSave] = useState(false);

    useEffect(() => {
        dispatch(setNewAppointment({
            ...newAppointment,
            hour: `${ ('0' + newAppointment.date.getHours()).slice(-2) } : ${ ('0' + newAppointment.date.getMinutes()).slice(-2) }`
        }));
    }, []);

    useEffect(() => {
        switch (true) {
            case newAppointment.hour === '':
                setCanSave(false);
                break;
            case newAppointment.title === '':
                setCanSave(false);
                break;
            case newAppointment.info === '':
                setCanSave(false);
                break;
            default:
                setCanSave(true);
        }
    }, [newAppointment.title, newAppointment.hour, newAppointment.info]);


    const onChange = (e: any, selectedDate: any) => {
        setShowTimePicker(false);
        dispatch(setNewAppointment({
            ...newAppointment,
            hour: `${ ('0' + selectedDate.getUTCHours()).slice(-2) } : ${ ('0' + selectedDate.getUTCMinutes()).slice(-2) }`
        }));
    };

    const closeCreateAppointment = () => {
        close();
        dispatch(clearNewAppointment());
    }

    const saveAppointment = () => {
        closeCreateAppointment();

        if (canSave) {
            dispatch(createNewAppointment({userId: String(userId), appointment: newAppointment}));
        }
    }

    return (
        <Animated.View
            style={ [styles.container] }
            entering={ SlideInDown }
            exiting={ SlideOutDown }
        >
            <ScrollView
                contentContainerStyle={ styles.scrollContainer }
            >
                <>
                    <Ionicons style={ styles.close } name="close" size={ 30 } color={ Colors.white } onPress={ () => closeCreateAppointment() }/>
                    <Calendar isCreate={ true }/>
                    <View style={ styles.hourContainer }>
                        <Text content={ 'Pick hour' } size={ 30 } fontFamily={ 'Poppins semiBold' }/>
                        <TouchableWithoutFeedback onPress={ () => setShowTimePicker(true) }>
                            <View style={ styles.pickedHour }>
                                <Text
                                    content={ newAppointment.hour }
                                    color={ Colors.white }
                                    size={ 50 }
                                />
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                    <CalendarForm isForm={ true }/>

                    {/* Do  save !! */ }
                    <TouchableWithoutFeedback onPress={ () => saveAppointment() }>
                        <View style={ [styles.saveButton, !canSave && {backgroundColor: Colors.gray}] }>
                            <Text content={ 'Schedule' } size={ 24 } fontFamily={ 'Poppins semiBold' } color={ Colors.black }/>
                        </View>
                    </TouchableWithoutFeedback>
                </>
            </ScrollView>
            { showTimePicker && (
                <DateTimePicker
                    timeZoneOffsetInMinutes={ 0 }
                    testID="dateTimePicker"
                    value={ newAppointment.date ? newAppointment.date : new Date() }
                    mode={ 'time' }
                    is24Hour={ true }
                    onChange={ onChange }
                    display={ 'spinner' }
                    themeVariant={ 'dark' }
                />
            ) }
        </Animated.View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        bottom: 0,
        flex: 1,
        height: heightPercent(90),
        position: 'absolute',
        width: widthPercent(100),
    },
    scrollContainer: {
        backgroundColor: Colors.o3_dark,
        borderTopRightRadius: 70,
        flexGrow: 1,
        paddingBottom: heightPx(40),
        paddingHorizontal: widthPx(15),
        paddingTop: heightPx(15),
    },
    close: {
        marginLeft: 'auto',
        marginRight: widthPx(20),
    },
    pickedHour: {
        marginLeft: 'auto'
    },
    hourContainer: {
        borderBottomWidth: 1,
        borderColor: 'rgba(255, 255, 255, .6)',
        borderTopWidth: 1,
        flexDirection: 'row',
        paddingHorizontal: widthPx(15),
        paddingVertical: heightPx(25),
        alignItems: 'center'
    },
    saveButton: {
        alignSelf: 'center',
        backgroundColor: Colors.white,
        borderRadius: 50,
        paddingHorizontal: widthPx(45),
        paddingVertical: heightPx(10),
    }
});
