import * as React from 'react';
import {useEffect, useState} from 'react';
import {StyleSheet, TouchableWithoutFeedback, View} from 'react-native';
import {Text} from '../text/Text';
import {Colors} from '../../theme/Colors';
import {heightPx, widthPercent, widthPx} from '../../helpers/Dimensions';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from '../../store/store';
import {CalendarSlice} from '../../store/slices/calendar/Calendar.slice';

interface Props {
    isCreate: boolean,
    date?: string | Date,
}

export const Calendar = (props: Props) => {
    const {isCreate, date} = props;
    const dispatch = useDispatch();
    const {newAppointment} = useSelector((state: RootState) => state.calendar);
    const {setNewAppointment} = CalendarSlice;
    const [activeDate, setActiveDate] = useState(date ? new Date(date) : newAppointment.date);
    const [selectedDay, setSelectedDay] = useState(date ? new Date(date).getDate() : newAppointment.date.getDate());
    const [matrix, setMatrix] = useState<Array<any>>([]);
    const [currentDate, setCurrentDate] = useState(new Date());

    const months = ['January', 'February', 'March', 'April',
        'May', 'June', 'July', 'August', 'September', 'October',
        'November', 'December'];
    const weekDays = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
    const nDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    const setNewDate = (alterMonth: number) => {
        setActiveDate(new Date(activeDate.getFullYear(), activeDate.getMonth() + alterMonth, 1));
    }

    const handleSetDate = (date: number) => {
        setSelectedDay(date);
        dispatch(setNewAppointment({...newAppointment, date: new Date(`${ activeDate.getFullYear() }/${ activeDate.getMonth() + 1 }/${ date === selectedDay ? date : date }`)}));
    }

    useEffect(() => {
        let mtx: Array<any> = [];
        mtx[0] = weekDays;
        let year = activeDate.getFullYear();
        let month = activeDate.getMonth();
        let firstDay: number = new Date(year, month, 1).getDay();
        let maxDays = nDays[month];
        if (month === 1) {
            if ((year % 4 === 0 && year % 100 != 0) || year % 400 === 0) {
                maxDays += 1;
            }
        }

        let counter = 1;
        for (let row = 1; row < 7; row++) {
            mtx[row] = [];
            for (let col = 0; col < 7; col++) {
                mtx[row][col] = -1;
                if (row === 1 && col >= firstDay) {
                    mtx[row][col] = counter++;
                } else if (row > 1 && counter <= maxDays) {
                    mtx[row][col] = counter++;
                }
            }
        }
        setMatrix(mtx);
    }, [activeDate]);

    return (
        <View>
            <View style={ styles.months }>
                <TouchableWithoutFeedback onPress={ () => isCreate && setNewDate(-1) }>
                    <View style={ styles.month }>
                        <Text content={ months[(activeDate.getMonth() - 1 < 0) ? 11 : activeDate.getMonth() - 1] } size={ 18 }/>
                    </View>
                </TouchableWithoutFeedback>
                <View style={ styles.selectedMonth }>
                    <Text content={ months[activeDate.getMonth()] } size={ 30 } color={ Colors.teal } fontFamily={ 'Poppins bold' }/>
                    <Text content={ `${ activeDate.getFullYear() }` } size={ 30 } color={ Colors.teal } fontFamily={ 'Poppins bold' }/>
                </View>
                <TouchableWithoutFeedback onPress={ () => isCreate && setNewDate(1) }>
                    <View style={ styles.month }>
                        <Text content={ months[(activeDate.getMonth() + 1 > 11) ? 0 : activeDate.getMonth() + 1] } size={ 18 }/>
                    </View>
                </TouchableWithoutFeedback>
            </View>
            {
                matrix.map((row: number[], index: number) =>
                    <View key={ `${ index }r` } style={ [styles.row, index === 0 && styles.border] }>
                        { row.map((col: number, cIndex: number) => {
                                return (col &&
                                    <TouchableWithoutFeedback
                                        key={ `${ index }${ cIndex }c` }
                                        onPress={ () => isCreate && handleSetDate(!isNaN(col) && col >= 0 ? col : selectedDay) }
                                    >
                                        <View
                                            style={ [styles.col, selectedDay === col && styles.selected] }
                                        >
                                            <Text
                                                size={ 20 }
                                                content={ `${ col !== -1 ? col : '' }` }
                                                fontFamily={ index === 0 ? 'Poppins semiBold' : 'Poppins light' }
                                                style={ [
                                                    (
                                                        currentDate.getDate() === col &&
                                                        activeDate.getMonth() === currentDate.getMonth() &&
                                                        activeDate.getFullYear() === currentDate.getFullYear()
                                                    ) && styles.active
                                                ] }
                                            />
                                        </View>
                                    </TouchableWithoutFeedback>
                                )
                            }
                        ) }
                    </View>
                )
            }
        </View>
    );
}

const styles = StyleSheet.create({
    row: {
        flexDirection: 'row',
    },
    border: {
        borderBottomWidth: 1,
        borderColor: 'rgba(255, 255, 255, .6)'
    },
    col: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 10,
        width: (widthPercent(100) - 170) / 7,
        height: (widthPercent(100) - 170) / 7,
    },
    selectedMonth: {
        alignItems: 'center',
        flex: 4
    },
    active: {
        color: Colors.red
    },
    selected: {
        borderRadius: 50,
        backgroundColor: Colors.light_teal,
    },
    month: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    months: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: heightPx(10),
        paddingRight: widthPx(10)
    }
})
