import * as React from "react"
import Svg, {
    G,
    Path,
    Defs,
    LinearGradient,
    Stop,
    ClipPath,
} from "react-native-svg"
import {StyleSheet} from 'react-native';
import {screenDimensions} from '../helpers/Dimensions';

export const SignInBackground = () => {
    return (
        <Svg
            style={ styles.svgImage }
            width={ screenDimensions.width + 1 }
            height={ screenDimensions.height }
        >
            <G clipPath="url(#a)">
                <Path fill="#fff" d={ `M0 0h${ screenDimensions.width }v${ screenDimensions.height + 50 }H0z` }/>
                <Path
                    d={ `M.376 ${ screenDimensions.height / 4.5 }c107 28 68 160 ${screenDimensions.width / 1.91} 157 18.604 0 38.574-1.965 58.552-5.083 65.967-11.858 153.448 51.231 143.448 ${ screenDimensions.width / 3.3 }v${ screenDimensions.height / 2 }h-890s-232.597-646.482 0-605Z` }
                    fill="url(#b)"
                />
            </G>
            <Defs>
                <LinearGradient
                    id="b"
                    x1={ screenDimensions.width }
                    y1={ 347.914 }
                    x2={ screenDimensions.width }
                    y2={ screenDimensions.height }
                    gradientUnits="userSpaceOnUse"
                >
                    <Stop offset={ 0.036 } stopColor="#274B5B"/>
                    <Stop offset={ 0.539 }/>
                </LinearGradient>
                <ClipPath id="a">
                    <Path fill="#ff0" d={ `M0 0h${ screenDimensions.width }v${ screenDimensions.height }H0z` }/>
                </ClipPath>
            </Defs>
        </Svg>
    );
}

const styles = StyleSheet.create({
    svgImage: {
        position: 'absolute',
    }
});
