import * as React from "react"
import Svg, {Path, Defs, LinearGradient, Stop} from "react-native-svg"
import {scale} from '../helpers/Dimensions';

export const WelcomeLogo = () => {
    const width = scale(58.8);
    const height = scale(53.6);

    return (
        <Svg
            width={ width }
            height={ height }
            viewBox="0 0 147 134"
            fill="none"
        >
            <Path
                d="M68.493 71.467a10.6 10.6 0 0 1-10.6 10.6H20.384a10.6 10.6 0 0 1-10.6-10.6V33.959a10.6 10.6 0 0 1 10.6-10.6h37.508a10.6 10.6 0 0 1 10.6 10.6v37.508ZM55.446 38.85a2.446 2.446 0 0 0-2.113-2.423L53 36.405H25.277l-.333.023a2.446 2.446 0 0 0 0 4.847l.333.022H53l.333-.022a2.446 2.446 0 0 0 2.113-2.424Zm0 27.723a2.446 2.446 0 0 0-2.113-2.423L53 64.128H25.277l-.333.023a2.446 2.446 0 0 0 0 4.847l.333.023H53l.333-.023a2.446 2.446 0 0 0 2.113-2.424Zm0-13.861a2.446 2.446 0 0 0-2.113-2.424L53 50.267H25.277l-.333.022a2.446 2.446 0 0 0 0 4.847l.333.023H53l.333-.023a2.446 2.446 0 0 0 2.113-2.423Z"
                fill="url(#a)"
            />
            <Path
                d="M88.316 108.875c0 9.254-9.38 16.75-20.938 16.75-11.557 0-20.937-7.496-20.937-16.75s9.38-16.75 20.937-16.75c11.558 0 20.938 7.496 20.938 16.75Z"
                fill="url(#b)"
            />
            <Path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M88.316 25.125v83.75H79.94v-83.75h8.375Z"
                fill="url(#c)"
            />
            <Path
                d="M79.94 23.618a8.375 8.375 0 0 1 6.734-8.208l25.125-5.025a8.377 8.377 0 0 1 10.017 8.208V33.5l-41.875 8.375V23.618Z"
                fill="url(#d)"
            />
            <Defs>
                <LinearGradient
                    id="a"
                    x1={ 39.139 }
                    y1={ 23.359 }
                    x2={ 39.139 }
                    y2={ 82.067 }
                    gradientUnits="userSpaceOnUse"
                >
                    <Stop stopColor="#274B5B"/>
                    <Stop offset={ 1 } stopColor="#57A8B2"/>
                </LinearGradient>
                <LinearGradient
                    id="b"
                    x1={ 67.378 }
                    y1={ 92.125 }
                    x2={ 67.378 }
                    y2={ 125.625 }
                    gradientUnits="userSpaceOnUse"
                >
                    <Stop stopColor="#274B5B"/>
                    <Stop offset={ 1 } stopColor="#57A8B2"/>
                </LinearGradient>
                <LinearGradient
                    id="c"
                    x1={ 84.128 }
                    y1={ 25.125 }
                    x2={ 84.128 }
                    y2={ 108.875 }
                    gradientUnits="userSpaceOnUse"
                >
                    <Stop stopColor="#274B5B"/>
                    <Stop offset={ 1 } stopColor="#57A8B2"/>
                </LinearGradient>
                <LinearGradient
                    id="d"
                    x1={ 100.878 }
                    y1={ 10.223 }
                    x2={ 100.878 }
                    y2={ 41.875 }
                    gradientUnits="userSpaceOnUse"
                >
                    <Stop stopColor="#274B5B"/>
                    <Stop offset={ 1 } stopColor="#57A8B2"/>
                </LinearGradient>
            </Defs>
        </Svg>
    )
};
