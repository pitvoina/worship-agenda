import * as React from "react";
import Svg, {
    Path,
    Defs,
    LinearGradient,
    Stop,
} from 'react-native-svg';
import {StyleSheet} from 'react-native';
import {screenDimensions} from '../helpers/Dimensions';

export const WelcomeBackground = () => {
    return (
        <Svg
            style={ styles.svgImage }
            width={ screenDimensions.width + 2 }
            height={ screenDimensions.height + 2 }
            fill="none"
        >
            <Path fill="url(#a)" d={ `M0 0h${ screenDimensions.width }v${ screenDimensions.height }H0z` }/>
            <Path
                d={`M0 ${screenDimensions.height / 2.3}c208.089 100.838 122.089-103.162 ${screenDimensions.width}-23v${screenDimensions.height / 1.6}H0V394Z`}
                fill="#fff"
            />
            <Defs>
                <LinearGradient
                    id="a"
                    x1={ screenDimensions.width }
                    y1={ 0 }
                    x2={ screenDimensions.width }
                    y2={ screenDimensions.height }
                    gradientUnits="userSpaceOnUse"
                >
                    <Stop stopColor="#0D0D0D"/>
                    <Stop offset={ 1 } stopColor="#0B1E26"/>
                </LinearGradient>
            </Defs>
        </Svg>
    );
}

const styles = StyleSheet.create({
    svgImage: {
        display: 'flex',
        position: 'absolute',
        width: '100%',
    }
});
