import * as React from 'react';
import {useSelector} from 'react-redux';
import {RootState} from '../store/store';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';

import {CalendarScreen} from '../screens/CalendarScreen';
import {ChatScreen} from '../screens/ChatScreen';
import {ForumScreen} from '../screens/ForumScreen';
import {HomeScreen} from '../screens/HomeScreen';
import {LoginScreen} from '../screens/LoginScreen';
import {ProfileScreen} from '../screens/ProfileScreen';
import {RegisterScreen} from '../screens/RegisterScreen';
import {RootStackParamList} from '../types';
import {WelcomeScreen} from '../screens/WelcomeScreen';
import {NewThreadScreen} from '../screens/NewThreadScreen';
import {ForumThreadScreen} from '../screens/ForumThreadScreen';

const Stack = createStackNavigator<RootStackParamList>();

export const RootNavigator = () => {
    const {authUser} = useSelector((state: RootState) => state.user);

    return (
        <>
            {
                authUser ? (
                    <Stack.Navigator
                        initialRouteName={ 'Forum' }
                        screenOptions={ {
                            cardStyle: {backgroundColor: '#fff'},
                            headerShown: false,
                        } }>
                        <Stack.Screen name={ 'Home' } component={ HomeScreen }/>
                        <Stack.Screen name={ 'Calendar' } component={ CalendarScreen }/>
                        <Stack.Screen name={ 'Chat' } component={ ChatScreen }/>
                        <Stack.Screen name={ 'Forum' } component={ ForumScreen }/>
                        <Stack.Screen name={ 'Profile' } component={ ProfileScreen }/>
                        <Stack.Screen name={ 'NewThread' } component={ NewThreadScreen }/>
                        <Stack.Screen name={ 'Thread' } component={ ForumThreadScreen }/>
                    </Stack.Navigator>
                ) : (
                    <Stack.Navigator
                        initialRouteName={ 'Welcome' }
                        screenOptions={ {
                            animationTypeForReplace: 'push',
                            gestureEnabled: false,
                            animationEnabled: true,
                            gestureResponseDistance: 150,
                            cardStyle: {backgroundColor: '#fff'}
                        } }>
                        <Stack.Screen
                            name={ 'Login' }
                            component={ LoginScreen }
                            options={ {
                                headerShown: false,
                                ...TransitionPresets.SlideFromRightIOS
                            } }
                        />
                        <Stack.Screen
                            name={ 'Register' }
                            component={ RegisterScreen }
                            options={ {
                                headerShown: false,
                                ...TransitionPresets.SlideFromRightIOS
                            } }
                        />
                        <Stack.Screen
                            name={ 'Welcome' }
                            component={ WelcomeScreen }
                            options={ {headerShown: false} }
                        />
                    </Stack.Navigator>
                )
            }
        </>

    )
}
