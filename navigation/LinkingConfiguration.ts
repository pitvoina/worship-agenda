import * as Linking from 'expo-linking';
import {LinkingOptions} from '@react-navigation/native';
import {RootStackParamList} from '../types';

const LinkingConfiguration: LinkingOptions<RootStackParamList> = {
    prefixes: [Linking.createURL('/')],
    config: {
        screens: {
            Calendar: {
                screens: {
                    CalendarScreen: 'calendar'
                }
            },
            Chat: {
                screens: {
                    ChatScreen: 'chat'
                },
            },
            Forum: {
                screens: {
                    ForumScreen: 'forum'
                }
            },
            Home: {
                screens: {
                    HomeScreen: 'home'
                }
            },
            Login: {
                screens: {
                    LoginScreen: 'login'
                }
            },
            NewThread: {
                screens: {
                    NewThreadScreen: 'newThread',
                },
            },
            Thread: {
                screens: {
                    ForumThreadScreen: 'thread',
                },
                parse: {
                    threadId: (threadId: string) => threadId
                }
            },
            Profile: {
                screens: {
                    ProfileScreens: 'profile'
                }
            },
            Register: {
                screens: {
                    RegisterScreen: 'register'
                }
            },
            Welcome: {
                screens: {
                    WelcomeScreen: 'welcome'
                }
            }
        }
    }
}

export default LinkingConfiguration;
