import {initializeApp} from 'firebase/app';
import {getFirestore} from 'firebase/firestore';
import {getAuth, initializeAuth} from 'firebase/auth';
import {getStorage} from 'firebase/storage';

const firebaseConfig = {
    apiKey: "AIzaSyDyWBf_mG9XSW8XvFAWsmmYGkK7K7IgRZk",
    authDomain: "worship-agenda-d671b.firebaseapp.com",
    projectId: "worship-agenda-d671b",
    storageBucket: "worship-agenda-d671b.appspot.com",
    messagingSenderId: "156301382610",
    appId: "1:156301382610:web:1b73bd97f745f0e612ae0b",
    measurementId: "G-0HEBBQR3NC"
};

const app = initializeApp(firebaseConfig);
const auth = getAuth();
const storage = getStorage();
const db = getFirestore(app);

export const androidId = '211143380493-fl3a4h3brop6st23hof4ui3npp1gd3mp.apps.googleusercontent.com';
export const facebookAppId = '3040187982915367';

export {db, auth, storage};
