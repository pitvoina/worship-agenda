const readline = require("readline");
const {writeFile, createDir} = require("./file-utils");

const setLowerCase = (name) => {
    return name[0].toLowerCase() + name.slice(1);
};

const setUpperCase = (name) => {
    return name[0].toUpperCase() + name.slice(1);
};


const getBaseComponentTemplateTsx = (name) => `import * as React from 'react';
import {StyleSheet} from 'react-native';

interface Props {
}

export const ${ setUpperCase(name) } = (props: Props) => {
  const {  } = props;

  return (
    <>
      ${ name }
    </>
  );
};

const styles = StyleSheet.create({});
`;

const getBaseComponentSliceTs = (name) => `import {createSlice} from '@reduxjs/toolkit';

interface ${ setUpperCase(name) }State {

}

const initialState: ${ setUpperCase(name) }State = {

}

const ${ setLowerCase(name) }Slice = createSlice({
    name: '${ setUpperCase(name) }Slice',
    initialState,
    reducers: {

    }
});

export const ${ setUpperCase(name) }Slice = ${ setLowerCase(name) }Slice.actions;
export const ${ setUpperCase(name) }Reducer = ${ setLowerCase(name) }Slice.reducer;`;

const getBaseComponentTemplateScss = (name) => `.${ name }-container{
}
`;


function askQuestion(query) {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
    });

    return new Promise((resolve) =>
        rl.question(query, (ans) => {
            rl.close();
            resolve(ans);
        }),
    );
}

const getFileNameAndPath = (name) => {
    const splitName = name.split("/");
    const fileName = splitName[splitName.length - 1];
    splitName.pop();
    const filePath = splitName.join("/");
    if (!fileName) {
        throw new Error("name is needed to create component");
    }

    return {fileName, filePath};
};

const createComponent = async () => {
    const name = process.argv[2];
    let fileName,
        filePath = null;
    let isCorrectNameAndPath = false;
    ({fileName, filePath} = getFileNameAndPath(name));

    isCorrectNameAndPath = await askQuestion(`Write file to ${ filePath } and name ${ fileName } ? y/n: `);

    if (isCorrectNameAndPath !== "y" && isCorrectNameAndPath !== "Y") {
        console.log('Stopped execution')
        return false;
    }

    console.log(filePath);
    console.log(fileName);
    createDir(`${ filePath }/${ setUpperCase(fileName) }`);
    writeFile(`${ filePath }/${ setUpperCase(fileName) }/${ setUpperCase(fileName) }.tsx`, getBaseComponentTemplateTsx(fileName));
    writeFile(`${ filePath }/${ setUpperCase(fileName) }/${ setUpperCase(fileName) }.slice.ts`, getBaseComponentSliceTs(fileName));
    // writeFile(`${filePath}/${fileName}/${fileName}.scss`,  getBaseComponentTemplateScss(fileName));

};

createComponent();
