const fs = require('fs');

const writeFile = (path, data) => {
    fs.writeFile(path, data, function (err) {
        if (err) return console.log(err);
        console.log("Generated::", path);
    });
};

const createDir = (path) => {

    if (!fs.existsSync(path)){
        fs.mkdirSync(path, { recursive: true });
    }
}


const readFile = (name, callback) => {
    fs.readFile(name, 'utf8' , (err, data) => {
        if (err) {
            console.error(err)
            return
        }
        if(callback && typeof callback === 'function'){
            callback( data );
        }
    })
}


module.exports = {
    writeFile,
    readFile,
    createDir,
}
