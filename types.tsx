import {NativeStackScreenProps} from '@react-navigation/native-stack';

declare global {
    namespace ReactNavigation {
        interface RootParamList extends RootStackParamList {
        }
    }
}

export type RootStackParamList = {
    Calendar: undefined,
    Chat: undefined,
    Forum: undefined,
    Home: undefined,
    Login: undefined,
    NewThread: undefined,
    Thread: any,
    Profile: undefined,
    Register: undefined,
    Welcome: undefined,
}

export type RootStackScreenProps<Screen extends keyof RootStackParamList> = NativeStackScreenProps<RootStackParamList, Screen>;
