import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {Provider} from 'react-redux';
import {StatusBar} from 'expo-status-bar';
import AppLoading from 'expo-app-loading';
import 'react-native-get-random-values';

import {LogBox} from 'react-native';
import {store} from './store/store';
import {RootNavigator} from './navigation/RootNavigator';
import LinkingConfiguration from './navigation/LinkingConfiguration';
import {useFonts} from './hooks/useFonts';
import {useState} from 'react';

LogBox.ignoreLogs(['Setting a timer for a long period of time']);

export default function App() {
    const [isReady, setIsReady] = useState(false);

    const LoadFonts = async () => {
        await useFonts();
    }
    if (!isReady) {
        return (<AppLoading
            startAsync={ LoadFonts }
            onFinish={ () => setIsReady(true) }
            onError={ () => {
            } }
        />);
    } else {
        return (
            <Provider store={ store }>
                <NavigationContainer linking={ LinkingConfiguration }>
                    <RootNavigator/>
                    <StatusBar style="light"/>
                </NavigationContainer>
            </Provider>
        );
    }
}

