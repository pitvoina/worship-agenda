import {InputType} from '../components/form/AppInput';

const INVALID_EMAIL = 'auth/invalid-email';
const UNKNOWN_USER = 'auth/user-not-found';
const WRONG_PASS = 'auth/wrong-password';
const INTERNAL_ERROR = 'auth/internal-error';

interface Props {
    value?: string,
    type?: InputType,
    code?: string,
    isDirty: boolean,
}

export const Validation = (props: Props) => {
    const {value, type, code} = props;
    let {isDirty} = props;
    if (!isDirty) return;

    let message: string | undefined;
    let hasError: boolean | undefined;

    const validateEmail = () => {
        if (value?.length === 0) return 'E-mail field cannot be empty!';
        switch (code) {
            case INVALID_EMAIL:
                return 'E-mail address is invalid!';
            case UNKNOWN_USER:
                return 'E-mail address is not associated with any of the registered users!';
        }
    }

    const validatePassword = () => {
        if (value?.length === 0) return 'Password field cannot be empty!';

        switch (code) {
            case WRONG_PASS:
                return 'Wrong password!';
            case INTERNAL_ERROR:
                return 'Something went wrong! Please contact admin.';
        }
    }

    const validateRegisterPassword = () => {
        const regex = /(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}/g;
        if (!value || !value?.match(regex) || value?.length === 0) return 'Password should have at least 6 characters and contain at least one number and one letter!';
    }

    const validateName = () => {
        if (!value || value?.length < 3) return 'Name should have at least 3 letters.';
    }

    const validatePhone = () => {
        const regex = /^(\+4|)?(07[0-8]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$/g;
        if (!value || !value?.match(regex) || value?.length === 0) return 'Invalid phone number!';
    }

    switch (type) {
        case 'email':
            message = validateEmail();
            break;
        case 'password':
            message = validatePassword();
            break;
        case 'name':
            message = validateName();
            break;
        case 'registerPassword':
            message = validateRegisterPassword();
            break;
        case 'phone':
            message = validatePhone();
            break;
        default:
            console.log('Invalid type - cannot validate!');
            break;
    }

    hasError = !!message?.length;

    return {message, hasError};
}
