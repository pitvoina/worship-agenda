import {Dimensions, PixelRatio} from 'react-native';

type ResizeBy = 'fontScale' | 'windowHeight' | 'screenHeight';

const dimensions = Dimensions.get('window');
const screenDimensions = Dimensions.get('screen');

const relatedHeight = 844;
const relatedWidth = 390;
const relatedScreenHeight = 941;
const pixelRatio = PixelRatio.get();
const relatedScreenWidth = 390;

const pxRatio = (px: number) => {
    return (px * 2.3) / pixelRatio;
}

const widthPercent = (widthPercent: number) => {
    return dimensions.width * (widthPercent / 100);
}

const heightPercent = (heightPercent: number) => {
    return dimensions.height * (heightPercent / 100);
}

const widthPx = (widthPx: number) => {
    // return  widthPx;
    let timesWindow = relatedWidth / widthPx;
    return pxRatio(dimensions.width / timesWindow);
}

const heightPx = (heightPx: number) => {
    // return  heightPx;
    let timesWindow = relatedHeight / heightPx;
    return pxRatio(dimensions.height / timesWindow);
}

const fontScale = (fontSize: number, type: ResizeBy = 'windowHeight') => {
    let endFontSize: number = fontSize;
    let timesWindow = relatedHeight / fontSize;
    let timesScreen = relatedScreenHeight / fontSize;

    switch (type) {
        case 'fontScale':
            endFontSize = dimensions.fontScale * fontSize;
            break;
        case 'screenHeight':
            endFontSize = screenDimensions.height / timesScreen;
            break;
        case 'windowHeight':
            endFontSize = dimensions.height / timesWindow;
            break;
        default:
            console.log('Error');
            break;
    }

    return pxRatio(endFontSize);
}

const scale = (size: number) => {
    return dimensions.scale * size;
}

export {widthPercent, heightPercent, fontScale, scale, widthPx, heightPx, dimensions, screenDimensions};
